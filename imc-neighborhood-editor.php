<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $wpdb;
$sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_neighorhoods';
$neighborhoods = $wpdb->get_results($sql);
?>
<div class="wrap">
    <h2>Add New Neighborhood</h2>
    <form method="post" action="" id="imc-add-neighborhood"> <!-- action="admin-post.php" -->
    <label for="neighborhood">
        <input type="text" id="neighborhood" name="neighborhood" placeholder="type neighborhoods here" />
        <input type="hidden" name="neighborhood_hidden" value="Y" />
        <input type="submit" class="button-primary" id="imc-submit-neighborhood" name="submit_ajax">
        <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" class="waiting" id="imc-loading" style="display:none;" />
        <p class="infoText">Place a comma between each neighborhood if adding multiple neighborhoods.</p>

    </label>
        <div><span id="feedback"></span></div>
    </form>
    
    <h2>Current Neighborhoods</h2>

    <ul class="typeList" id="imc-neighborhood-results">
        <?php
        foreach($neighborhoods as $neighborhood) {
            echo '<li id="'.$neighborhood->id.'">'.$neighborhood->name.'</li>';
        } ?>
    </ul>
</div>