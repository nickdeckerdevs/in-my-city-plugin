<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_states() {
    $stateDropDown = '<select id="state" name="state"><option value="ZZ">Other</option><option value="AL">Alabama</option><option value="AK">Alaska</option>'
            . '<option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option>'
            . '<option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option>'
            . '<option value="DC">District of Columbia</option><option value="FL" selected="selected">Florida</option><option value="GA">Georgia</option>'
            . '<option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option>'
            . '<option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option>'
            . '<option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option>'
            . '<option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option>'
            . '<option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option>'
            . '<option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option>'
            . '<option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option>'
            . '<option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option>'
            . '<option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option>'
            . '<option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option>'
            . '<option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option>'
            . '<option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option>'
            . '<option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option>'
            . '</select>';
    return $stateDropDown;
}

function select_dropdown($option, $value) {
    if ($option == $value) {
        return ' selected = "selected" ';
    }
    return;
}

function get_checkbox_array($string, $value) {
    if (strlen($string) > 1) {
        $array = explode(',', $string);
        foreach ($array as $key) {
            if ($key == $value) {
                return ' checked="checked" ';
            }
        }
    } else {
        if ($string == $value) {
            return ' checked ="checked" ';
        }
    }
    return;
}

function get_text($value) {
    return ' value = "' . $value . '" ';
}

function check_checkbox($option, $value) {
    if ($option == $value) {
        return ' checked="checked" ';
    }
    return;
}

function display_hh_edit($id, $name) {
    echo '<div id="happy-hour-edit-container"><form id="happy-hour-edit">';
    echo '<input type="hidden" name="venue_id" value="' . $id . '" />';
    echo '<input type="hidden" name="action" value="imc_happy_hour_editor" />';
    echo '<input type="submit" id="edit-hh" class="button-primary" value="Edit Happy Hours for ' . $name . '" />';
    echo '</form>';
}


function venue_editor_page() {
    global $wpdb;
    $edit = false;
    $premium = false;
    if (isset($_GET) && isset($_GET['id'])) {
        $check_sql = 'SELECT * from ' . $wpdb->prefix . 'imc_venue_paid where venue_id = ' . $_GET['id'] . ' LIMIT 1';
        $checked = $wpdb->get_row($check_sql);
        if ($checked != null) {
            $sql = 'SELECT venue.*, paid.venue_id, paid.website, paid.photos, paid.twitter, paid.yelp, paid.googleplus, paid.foursquare, paid.facebook, paid.urbanspoon, paid.instagram, paid.details, paid.menu, paid.beer_on_taps, paid.craft_beers FROM ' . $wpdb->prefix . 'imc_venue as venue inner join ' . $wpdb->prefix . 'imc_venue_paid as paid on venue.id = paid.venue_id WHERE venue.id = ' . $_GET['id'] . ' LIMIT 1';
            $premium = true;
        } else {
            $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue  WHERE id = ' . $_GET['id'] . ' LIMIT 1';
        }
        $venue = $wpdb->get_row($sql);
        $edit = true;
    }
    $alpha = ' ORDER BY name ASC';
    $save_type = ($edit) ? 'save_venue' : 'new_venue';
    $title = ($edit) ? 'Edit Venue' : 'Add New Venue';
    ?>
    <style>
        .notYet { display: none!important; }
        #venueEdit label { vertical-align: top; padding: 10px 0; display: inline-block; width: 20%; margin-right: 2%; line-height: 175%; }
        #venueEdit input[type="text"], #venueEdit select { display: block; margin-left: 33px; }
        #venueEdit input[type="radio"], #venueEdit input[type="checkbox"] { margin-left: 15px; }
        .labelOptions { width: 20%; margin-right: 2%; display: inline-block; vertical-align: top; }
        #venueEdit .labelOptions label { margin-right: 0; width: 100%;}
        .labelOptions input { float: initial; } //remove
        .answerOptions { width: 100%; float: left; margin-left: 20px; }
        .imc-button { cursor: pointer; }
        .saveVenue { background: #0074a2; padding: 20px 0; text-align: center; margin: 30px 20px 0 0; }
        .imc-save-button { padding: 15px; margin-left: 20px; cursor: pointer; }
        .labelWrapper > div > label { padding: 0!important; }
        .logo { display: inline-block; }
        #imc_image_preview { max-width: 200px; float: right; margin-top: 30px; }
        #premium { display: block;}
        #premium input[type="text"], #premium select { display: inline-block; }
        #premium .no-main { display: none; }
        .logo-preview, .image-preview { max-width: 250px; }
    </style>
    <?php echo ($edit) ? display_hh_edit($venue->id, $venue->name) : ''; ?>
    
    
    
    <div id="imc-venue-results"></div>
    <div class="venue-edit-container">
        <h1><?php echo $title; ?></h1>
        <div id="formClear">
            <form method="POST" action="" id="imc-add-venue">

                <input type="hidden" name="action" value="imc_save_venue" />

                <input type="hidden" name="save_type" value="<?php echo $save_type; ?>" />
                <?php wp_nonce_field('imc-venue-nonce'); ?>
                <div id="venueEdit">
                    <label for="name">
                        Venue Name: <input type="text" id="name" name="name" <?php echo ($edit) ? get_text($venue->name) : ''; ?> />
                    </label>
                    <label for="address">
                        Street Address: <input type="text" id="address" name="address" <?php echo ($edit) ? get_text($venue->address) : ''; ?> />
                    </label>
                    <label for="address2">
                        Street Address 2: <input type="text" id="address2" name="address2" <?php echo ($edit) ? get_text($venue->address2) : ''; ?> />
                    </label>
                    <label for="city">
                        City: <input type="text" id="city" name="city" <?php echo ($edit) ? get_text($venue->city) : 'value="Naples" '; ?>  />
                    </label>
                    <label for="state">
                        State: <?php echo get_states(); ?>
                    </label>
                    <label for="zip">
                        Zip: <input type="text" id="zip" name="zip" <?php echo ($edit) ? get_text($venue->zip) : ''; ?> />
                    </label>
                    <label for="phone">
                        Phone Number: <input type="text" id="phone" name="phone" <?php echo ($edit) ? get_text($venue->phone) : ''; ?> />
                    </label>
                    <!--<label for="contact_id" class="notYet">
                        Contact Id: <input type="text" id="contact_id" name="contact_id" />
                    </label>
                    <label for="contact_name">
                        Venue Contact: <input type="text" id="contact_name" name="contact_name" />
                    </label>
                    <label for="contact_phone">
                        Venue Phone: <input type="text" id="contact_phone" name="contact_phone" />
                    </label> -->
                    <label for="cuisine1">
                        Cuisine 1:
                        <select id=cuisine1" name="cuisine1">
                            <option value="">Select A Cuisine Type</option>
                            <?php
                            $table = $wpdb->prefix . 'imc_venue_cuisine_list';
                            $cuisines = $wpdb->get_results('SELECT * FROM ' . $table . $alpha);
                            foreach ($cuisines as $cuisine) {
                                if ($cuisine->name !== '') {
                                    $selected = ($edit) ? select_dropdown($cuisine->id, $venue->cuisine1) : '';
                                    echo '<option value="' . $cuisine->id . '" ' . $selected . '>' . $cuisine->name . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                    <label for="cuisine2">
                        Cuisine 2:
                        <select id=cuisine2" name="cuisine2">
                            <option value="">Select A Cuisine Type</option>
                            <?php
                            $table = $wpdb->prefix . 'imc_venue_cuisine_list';
                            $cuisines = $wpdb->get_results('SELECT * FROM ' . $table . $alpha);
                            foreach ($cuisines as $cuisine) {
                                if ($cuisine->name !== '') {
                                    $selected = ($edit) ? select_dropdown($cuisine->id, $venue->cuisine2) : '';
                                    echo '<option ' . $selected . ' value="' . $cuisine->id . '">' . $cuisine->name . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                    <label for="cuisine3">
                        Cuisine 3:
                        <select id=cuisine3" name="cuisine3">
                            <option value="">Select A Cuisine Type</option>
                            <?php
                            $table = $wpdb->prefix . 'imc_venue_cuisine_list';
                            $cuisines = $wpdb->get_results('SELECT * FROM ' . $table . $alpha);
                            foreach ($cuisines as $cuisine) {
                                if ($cuisine->name !== '') {
                                    $selected = ($edit) ? select_dropdown($cuisine->id, $venue->cuisine3) : '';
                                    echo '<option ' . $selected . ' value="' . $cuisine->id . '">' . $cuisine->name . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                    <label for="neighborhood">
                        Neighborhood:
                        <select id="neighborhood" name="neighborhood">
                            <option value="">Select A Neighborhood</option>
                            <?php
                            $table = $wpdb->prefix . 'imc_venue_neighborhoods';
                            $neighborhoods = $wpdb->get_results('SELECT * FROM ' . $table . $alpha);
                            foreach ($neighborhoods as $neighborhood) {
                                $selected = ($edit) ? select_dropdown($neighborhood->id, $venue->neighborhood) : '';
                                echo '<option ' . $selected . ' value="' . $neighborhood->id . '">' . $neighborhood->name . '</option>';
                            }
                            ?>
                        </select>
                    </label>
                    <label for="price_range">
                        Price Range:
                        <select id="price_range" name="price_range">
                            <option value="">Select A Price Range</option>
                            <?php
                            for ($x = 1; $x <= 5; $x++) {
                                switch ($x) {
                                    case 5:
                                        $price_range = 'Unsure';
                                        break;
                                    case 4:
                                        $price_range = '$$$$';
                                        break;
                                    case 3:
                                        $price_range = '$$$';
                                        break;
                                    case 2:
                                        $price_range = '$$';
                                        break;
                                    case 1:
                                        $price_range = '$';
                                        break;
                                    default:
                                        $price_range = '';
                                }
                                $selected = ($edit) ? select_dropdown($x, $venue->price_range) : '';
                                echo '<option  ' . $selected . ' value="' . $x . '">' . $price_range . '</option>';
                            }
                            ?>
                        </select>
                    </label>

                    <div class="labelOptions">
                        <div class="labelWrapper">
                            Outdoor Seating: 
                            <div class="answerOptions">
                                <label>
                                    <input type="radio" name="outdoor" value="1" <?php echo ($edit) ? check_checkbox('1', $venue->outdoor) : ''; ?> />Yes<br>
                                </label>
                                <label>
                                    <input type="radio" name="outdoor" value="0" <?php echo ($edit) ? check_checkbox('0', $venue->outdoor) : ''; ?> />No<br>
                                </label>
                                <label>
                                    <input type="radio" name="outdoor" value="3" <?php echo ($edit) ? check_checkbox('3', $venue->outdoor) : ''; ?> />Unsure<br>
                                </label>
                                <input type="hidden" id="outdoor_hidden" name="outdoor_hidden" value ="3" />
                            </div>
                        </div>
                    </div>
                    <div class="labelOptions" style="display: none;">
                        <div class="labelWrapper">
                            Claimed:
                            <div class="answerOptions">
                                <label>
                                    <input type="radio" name="is_claimed" value="1" />Yes<br>
                                </label>
                                <label>
                                    <input type="radio" name="is_claimed" value="0" />No<br>
                                </label>
                                <label>
                                    <input type="radio" name="is_claimed" value="3" />Unsure<br>
                                </label>
                                <input type="hidden" id="is_claimed_hidden" name="is_claimed_hidden" value ="2" />
                            </div>
                        </div>
                    </div>
                    <div class="labelOptions">
                        <div class="labelWrapper">
                            Open For:
                            <div class="answerOptions">
                                <label>
                                    <input type="checkbox" name="meal_types" value="1" <?php echo ($edit) ? get_checkbox_array($venue->meal_types, 1) : ''; ?> />Breakfast<br>
                                </label>
                                <label>
                                    <input type="checkbox" name="meal_types" value="2" <?php echo ($edit) ? get_checkbox_array($venue->meal_types, 2) : ''; ?> />Brunch<br>
                                </label>
                                <label>
                                    <input type="checkbox" name="meal_types" value="3" <?php echo ($edit) ? get_checkbox_array($venue->meal_types, 3) : ''; ?> />Lunch<br>
                                </label>
                                <label>
                                    <input type="checkbox" name="meal_types" value="4" <?php echo ($edit) ? get_checkbox_array($venue->meal_types, 4) : ''; ?> />Dinner<br>
                                </label>
                                <label>
                                    <input type="checkbox" name="meal_types" value="5" <?php echo ($edit) ? get_checkbox_array($venue->meal_types, 5) : ''; ?> />Unsure<br>
                                </label>
                                <input type="hidden" id="meal_types_hidden" name="meal_types_hidden" value ="5" />
                            </div>
                        </div>
                    </div>
                    <div class="labelOptions">
                        <div class="labelWrapper">
                            Venue Options
                            <div class="answerOptions">
                                <?php
                                $table = $wpdb->prefix . 'imc_venue_flag_list';
                                $flags = $wpdb->get_results('SELECT * FROM ' . $table);
                                foreach ($flags as $flag) {
                                    $ischecked = ($edit) ? get_checkbox_array($venue->venue_flags, $flag->id) : '';
                                    echo '<label><input type="checkbox"  name="venue_flags" value="' . $flag->id . '" ' . $ischecked . ' />' . $flag->name . '</label><br>';
                                }
                                ?>
                            </div>
                            <input type="hidden" id="venue_flags_hidden" name="venue_flags_hidden" value ="11" />
                        </div>
                    </div>
                    <div class="labelOptions" style="display:none;">
                        <div class="labelWrapper">
                            Waterfront:
                            <div class="answerOptions">
                                <label>
                                    <input type="radio" name="waterfront" value="0" <?php echo ($edit) ? check_checkbox('0', $venue->waterfront) : ''; ?> />None<br>
                                </label>
                                <label>
                                    <input type="radio" name="waterfront" value="1" <?php echo ($edit) ? check_checkbox('1', $venue->waterfront) : ''; ?> />Beach Front<br>
                                </label>
                                <label>
                                    <input type="radio" name="waterfront" value="2" <?php echo ($edit) ? check_checkbox('2', $venue->waterfront) : ''; ?> />River Front<br>
                                </label>
                                <label>
                                    <input type="radio" name="waterfront" value="3" <?php echo ($edit) ? check_checkbox('3', $venue->waterfront) : ''; ?> />Bay Front<br>
                                </label>
                                <label>
                                    <input type="radio" name="waterfront" value="4" <?php echo ($edit) ? check_checkbox('4', $venue->waterfront) : ''; ?> />Unsure<br>
                                </label>
                                <input type="hidden" id="waterfont_hidden" name="waterfront_hidden" value ="4" />
                            </div>
                        </div>
                    </div>
                    <div class="labelOptions">
                        <label for="website">
                            Website: <input type="text" id="website" name="website"  <?php echo ($edit) ? get_text($venue->website) : 'value="" '; ?> />
                            <br><i>Please type http:// before the website address</i><br>
                        </label>
                    </div>
                    <div class="labelOptions">
                        <div class="labelWrapper">
                            Is Active Listing:
                            <div class="answerOptions">
                                <label>
                                    <input type="radio" name="is_active" value="1" <?php echo ($edit) ? check_checkbox('1', $venue->is_active) : ' checked="checked" '; ?> />Yes<br>
                                </label>
                                <label>
                                    <input type="radio" name="is_active" value="0" <?php echo ($edit) ? check_checkbox('0', $venue->is_active) : ''; ?> />No<br>
                                </label>
                                <label>
                                    <input type="radio" name="is_active" value="1" />Unsure<br>
                                </label>
                                <input type="hidden" id="is_active_hidden" name="is_active_hidden" value ="1" />
                            </div>
                        </div> 
                    </div>

                    <div id="premium">
                        <h2>Premium fields</h2>
                        <?php
                        $logo = '';
                        if(isset($venue)) {
                            $logo = ($venue->logo != 'http://' && $venue->logo != '') ? $venue->logo : 'http://';
                        }
                        ?>
                        <table>
                        
                        <tr><th><label><b>Logo Image Upload</b></label></th>
                        <td><input name='logo_image' id='logo_image' type='text' class='imc_logo_image' value='<?php echo $logo; ?>' size='50' />
                        <input id='imc_logo_button' class='imc_logo_button imc_image_url_button button button-primary' type='button' value='Select Logo' />
                        <br><img id='imc_logo_preview' class='logo-preview' /></td></tr>
                        <?php 
                        if(isset($venue)) {
                            if(property_exists($venue, 'photos')) {
                                $image = explode(',', $venue->photos);
                            } else {
                                $image = ['','','','',''];
                            }
                        } else {
                            $image = ['','','','',''];
                        }
                        
                        for($i = 1; $i <= 5; $i++) {
                            ?>
                        <tr><th><label><b>Venue Image Upload <?php echo $i; ?></b></label></th>
                        <td><input id='imc_image_url<?php echo  $i; ?>' type='text' name='adimage<?php echo $i; ?> ' class='imc_image_venue_profile' value='<?php 
                        if(array_key_exists($i, $image)) {
                            echo ($image[$i] != 'http://' || $image[$i] != '') ? $image[$i] : '';
                        } 
                        
                         ?>' size='50' />
                        <input id='imc_image_url_button<?php echo  $i; ?>' class='imc_image_url_button button button-primary' type='button' value='Select Image' />
                        <br>Images should be 1130px in width.
                            <br><img id='imc_image_preview<?php echo  $i; ?>' class='image-preview' /></td></tr>
                        <?php                            
                        }
                        ?>
                        </table>
                        <label for="facebook">
                            Facebook URL: <br><input type="text" id="facebook" name="facebook" <?php echo ($premium) ? get_text($venue->facebook) : ''; ?> />
                        </label>
                        <label for="twitter">
                            Twitter URL: <br><input type="text" id="twitter" name="twitter" <?php echo ($premium) ? get_text($venue->twitter) : ''; ?> />
                        </label>
                        <label for="yelp">
                            Yelp: <br><input type="text" id="yelp" name="yelp" <?php echo ($premium) ? get_text($venue->yelp) : ''; ?> />
                        </label>
                        <label for="googleplus">
                            Google Plus URL: <br><input type="text" id="googleplus" name="googleplus" <?php echo ($premium) ? get_text($venue->googleplus) : ''; ?>  />
                        </label>
                        <label for="foursquare">
                            Foursquare: <br><input type="text" id="foursquare" name="foursquare" <?php echo ($premium) ? get_text($venue->foursquare) : ''; ?>  />
                        </label>
                        <label for="urbanspoon">
                            Urbanspoon: <br><input type="text" id="urbanspoon" name="urbanspoon" <?php echo ($premium) ? get_text($venue->urbanspoon) : ''; ?> />
                        </label>
                        <label for="instagram">
                            Instagram: <br><input type="text" id="instagram" name="instagram" <?php echo ($premium) ? get_text($venue->instagram) : ''; ?> />
                        </label>
                        <label for="menu">
                            Link to Menu: <br><input type="text" id="menu" name="menu" <?php echo ($premium) ? get_text($venue->menu) : ''; ?> />
                        </label>
                        <label for="beersOnTap">
                            Beers on Tap: <br><input type="text" id="beersOnTap" name="beersOnTap" <?php echo ($premium) ? get_text($venue->beer_on_taps) : ''; ?> />
                        </label>
                        <label for="craftBeers">
                            Additional Information: <br><input type="text" id="craftBeers" name="craftBeers" <?php echo ($premium) ? get_text($venue->craft_beers) : ''; ?> />
                        </label>
                        <!--<label for="details">
                            Open Details: <input type="text" id="details" name="details" <?php /* echo ($premium) ? get_text($venue->details) : ''; */ ?> />
                        </label>
-->                        
                    </div>

                </div>
                <div class="saveVenue">
                    <input type="hidden" name="images" id="images" <?php echo ($premium) ? get_text($venue->photos) : ''; ?> />
                    <input type="hidden" name="saveid" id="save-id" <?php echo ($edit) ? get_text($venue->id) : ''; ?> />
                    <input id="imc-reset-venue" type="submit" class="button-cancel" value="Reset Venue" />
                    <input id="imc-submit-venue" type="submit" class="button-primary" value="Save Venue" />
                    <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" class="waiting" id="imc-loading" style="display:none;" />
                </div>
            </form>
        </div>
        <script>
            function populate_logo() {
                var image = jQuery('#logo_image').val();
                jQuery('#logo_image').val(image);
                jQuery('#imc_logo_preview').attr('src', image);
            }


            function populate_images() {
                var images = jQuery('#images').val().split(',');
                for (var i = 0; i <= images.length; i++) {
                    var image = images[i];
                    jQuery('.imc_image_venue_profile').eq(i).val(image);
                    jQuery('.image-preview').eq(i).attr('src', image);
                }
            }
            jQuery(document).ready(function ($) {
                populate_logo();
                populate_images();
            });

        </script>
    </div>
    <?php
}
