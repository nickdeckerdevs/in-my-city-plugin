<?php
/**
 * Created by PhpStorm.
 * User: nickdecker
 * Date: 10/29/14
 * Time: 8:25 AM
 */

function calculate_totals() {
    $cuisines = get_cuisines();
    foreach( $cuisines as $cuisine ) {
        $cuisine_id = $cuisine->id;
        $total = get_total_venues( $cuisine_id );
        $updated = update_venue_total( $cuisine_id, $total );
        //var_dump($updated);
    }
}

function get_cuisines() {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_cuisine_list';
    $results = $wpdb->get_results( $sql );
    return $results;
}

function get_total_venues( $id ) {
    global $wpdb;
    $sql = "SELECT COUNT(*) FROM " . $wpdb->prefix . "imc_venue WHERE cuisine1 = $id OR cuisine2 = $id OR cuisine3 = $id";
    $results = $wpdb->get_var( $sql );
    echo $results;
    return $results;
}

function update_venue_total( $id, $total ) {
    global $wpdb;
    $venue_update = $wpdb->update(
        $wpdb->prefix . 'imc_venue_cuisine_list',
        array( 'total' => $total ),
        array( 'id' => $id )
    );
    return $venue_update;
}