<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function imc_get_hh_venues_table( $happy_hour_results, $sort ) {
    $hh_type_array = imc_get_happy_hour_table($sort);
    ?>
<table id="best-hh-data" cellpadding="15">
    <?php if(count($happy_hour_results) != 0) {
            foreach( $happy_hour_results as $result ) {

            global $wpdb;
            $sql = 'SELECT wp_guid FROM ' . $wpdb->prefix . 'imc_venue WHERE id = ' . $result->venue_id;
            $url = $wpdb->get_var( $sql );
            if( $url == '' || $url == null ) {
                $name = $result->name;
            } else {
                $name = '<a href="' . $url . '" target="_blank">' . $result->name . '</a>';
            }
              if( time() < strtotime( $result->time_end ) || $result->all_day == 1 ) {
                  //$price_discount = $result->reg_price - $result->price;
                  echo '<tr>';
                  echo '<td colspan = "6" class="venue-name"><b>' . $name . '</b></td>';
                  echo '</tr><tr>';
                  echo '<td>' . imc_check_all_day( $result->all_day, 'time_start', $result ) . '</td>';
                  echo '<td>' . imc_check_all_day( $result->all_day, 'time_end', $result ) . '</td>';
                  echo '<td>' . $hh_type_array[ $result->happy_hour_type - 1 ][ 'happy_hour_type' ] . '</td>';
                  echo '<td colspan="5"><i>' . $result->summary . '</i></td>';
                  echo '<td><span class="old-price">$'.number_format( $result->reg_price, 2, '.', '' ).'</span><br><b>$' . number_format( $result->price, 2, '.', '' ) . '</b></td>';
                  echo '</tr>';
              }
            }
        } else {
            echo '<tr>';
            echo '<td colspan = "6" class="venue-name"><b>No Upcoming happy hours for today.</b></td>';
            echo '</tr>';
        }
    echo '</table>';
    
}

function imc_check_all_day( $all_day, $alpha_or_omega, $result ) {
    if( $all_day == 1 ) {
        if( $alpha_or_omega == 'time_end' ) {
            $ret_value = '';
        } else {
            $ret_value = 'All Day';
        }
    } else {
        $ret_value = date( "g:i a", strtotime( $result->$alpha_or_omega ) );
    }
    return $ret_value;
}

function imc_get_hh_type_text_db( $type ) {
    if( $type != 0 ) {
        global $wpdb;
        $type_sql = 'SELECT happy_hour_type FROM ' . $wpdb->prefix . 'imc_happy_hour_type WHERE id = ' . $type;
        $type_result = $wpdb->get_row( $type_sql );
        $ret_val = $type_result->happy_hour_type;
    } else {
        $ret_val = '';
    }
    return  $ret_val;
}


$uri = strpos($_SERVER['REQUEST_URI'], 'ajax') !== false ? parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) : 'happy-hour/';
$active_sort = isset($_POST['sort']) ? trim(htmlspecialchars($_POST['sort'])) : 'time';
?>



<div class="best-hh">
    <h2>Upcoming Happy Hours - <?php echo imc_get_hh_type_text( $type ); ?></h2>
    
    <div id="hh-data">
        <div>
            <p style="text-align:right; color: #000; padding: 20px; cursor:pointer;" class='sort-type'>Sort By: 
            <span id="time">Time</span> | 
            <span id="price">Price Per Oz</span> | 
            <span id="cheapest">Cheapest</span> |
            <span id="name">Name</span>
            </p>
        </div>
        <?php imc_get_hh_venues_table( $happy_hour_results, $sort ); ?>
    
        
    </div>
</div>