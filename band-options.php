<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include( 'imc-get-venue-options.php' );
function display_band_sidebar() {
?>
<div class="widget">
    <form action="" id="imc-filter-venues" name="filters">
    <h2>Advanced Search</h2>

    <div class="search">
        Check any of the boxes below to refine your search
    </div>
    <input name="searchname" id="venue-name-search" type="text" class="hidden" />
    <input name="bandsearch" id="venue-search-hidden" type="hidden" value="band" />
    <input name="record" id="pagination-record" type="text" class="hidden" />
    <input name="letter" id="pagination-letter" type="text" class="hidden" />
    <div class="filter">
        <h2 class="expand">Genre</h2>
        
        <div class="filter-options" style="display:block;">
            <?php echo get_options( 'genres' ); ?>
        </div>
    </div>
    
    <input type="hidden" name="action" value="imc_show_bands" />
    </form>
</div>
<?php
}