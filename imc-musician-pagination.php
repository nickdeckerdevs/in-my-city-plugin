<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_alpha_pagination() {
    global $wpdb;
    $alpha_query = 'SELECT band_name FROM ' . $wpdb->prefix . 'imc_bands ORDER BY band_name ASC';
    $alpha_results = $wpdb->get_results( $alpha_query );
    if( isset( $_POST[ 'letter' ] ) ) {
        $active = $_POST[ 'letter' ];
    } else {
        $active = '';
    }

    $alpha_array = array();
    foreach( $alpha_results as $result ) {
        if( !in_array( ucfirst( substr( $result->band_name, 0, 1) ), $alpha_array ) ) {
            $alpha_array[] = substr( $result->band_name, 0, 1);
        }
    }
    echo '<div id="pagination">';
        echo '<ul class="pagination">';
            foreach( $alpha_array as $page ) {
                $pagination_class = ( $active == $page ) ? ' current_pagination' : '';
                echo "<li><span data-pagination='$page' class='pagination-letter pagination_button$pagination_class'>$page</a></li>";
            }
        echo '</ul>';
    echo '</div>';
}

function get_numerical_pagination( $limit, $record, $venue_sql ) {
    global $wpdb;
    $new_query = explode( '*', $venue_sql );
    $new_query = explode( 'LIMIT', $new_query[1] );
    $new_query = 'SELECT COUNT(*) ' . $new_query[0];
    //var_dump($new_query);
    $rows = $wpdb->get_var( $new_query );
    $total = ceil( $rows / $limit );
    if( $total > 1 ) {
        echo '<div id="pagination" class="hidden">';
        //var_dump($total);
        //var_dump($rows);
            echo '<ul class="pagination">';
                for( $i = 1; $i <= $total; $i++ ) {
                    if( $i == $record ) {
                        echo '<li><span class="current_pagination">' . $i . '</span></li>';
                    } else { 
                        echo '<li><span class="pagination-button" data-pagination="' . $i . '">' . $i . '</span></li>';
                    }
                }
            echo '</ul>';
        echo '</div>';
    }
}

function imc_display_pagination( $limit, $record, $venue_sql, $type ) {
    echo '<div class="venue-head">';
    if( $type == 'alpha' || $type == 'both' ) {
        get_alpha_pagination();
    } 
    if( $type == 'numerical' || $type == 'both' ) {
        get_numerical_pagination( $limit, $record, $venue_sql );
    } 
    
    echo '<div class="center namesearch">';
    echo '<i>Search by Musican/Band Name</i> ';
    echo ' <input id="venue-search-name" name="searchname" type="text" /><input type="button" id="name-search" value="Search By Name" /> ';
    echo ' <input type="button" class="clear-all" value="X Clear All Filters and Searches">';
    echo '</div>';
    echo '</div>';
}