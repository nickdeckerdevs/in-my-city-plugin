<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function display_import( $type ) {

    global $wpdb;
    $alpha = ' ORDER BY name ASC';
    $stateDropDown = '<select id="state" name="state"><option value="ZZ">Other</option><option value="AL">Alabama</option><option value="AK">Alaska</option>'
        .'<option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option>'
        .'<option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option>'
        .'<option value="DC">District of Columbia</option><option value="FL">Florida</option><option value="GA">Georgia</option>'
        .'<option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option>'
        .'<option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option>'
        .'<option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option>'
        .'<option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option>'
        .'<option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option>'
        .'<option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option>'
        .'<option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option>'
        .'<option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option>'
        .'<option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option>'
        .'<option value="PA">Pennsylvania</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option>'
        .'<option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option>'
        .'<option value="VT">Vermont</option><option value="VA">Virginia</option><option value="WA">Washington</option>'
        .'<option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option>'
        .'</select>';


    ?>
    <style>
        .notYet { display: none!important; }
        #venueEdit label { vertical-align: top; padding: 10px 0; display: inline-block; width: 20%; margin-right: 2%; line-height: 175%; }
        #venueEdit input[type="text"], #venueEdit select { display: block; margin-left: 33px; }
        #venueEdit input[type="radio"], #venueEdit input[type="checkbox"] { margin-left: 15px; }
        .labelOptions { width: 20%; margin-right: 2%; display: inline-block; vertical-align: top; }
        #venueEdit .labelOptions label { margin-right: 0; width: 100%;}
        .labelOptions input { float: initial; } //remove
        .answerOptions { width: 100%; float: left; margin-left: 20px; }
        .imc-button { cursor: pointer; }
        .saveVenue { background: #0074a2; padding: 20px 0; text-align: center; margin: 30px 20px 0 0; }
        .imc-save-button { padding: 15px; margin-left: 20px; cursor: pointer; }

        .labelWrapper > div > label { padding: 0!important; }
        .logo { display: inline-block; }
        #imc_image_preview { max-width: 327px; float: right; }


    </style>

    <h2><?php echo $type; ?> Importer</h2>
    <form method="POST" action="" id="imc-process-textarea">
    <input type="hidden" name="action" value="imc_process_textarea" />
    <input type="hidden" name="importtype" value="<?php echo $type; ?>" />
    <?php wp_nonce_field( 'imc-csv-nonce' ); ?>
    <?php wp_nonce_field('create_none_for_wti','wti_like_post_meta_box_nonce'); ?>
    <textarea style="width:1469px; height: 287px;"  name="csv-data" id="csv-data"></textarea>

    <div id="imc-venue-results"></div>

    <div class="saveVenue">

        <input id="imc-process-import" type="submit" class="button-primary" value="Process textarea" />
        <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" class="waiting" id="imc-loading" style="display:none;" />
    </div>
    </form>

<?php
}