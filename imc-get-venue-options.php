<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function display_options( $var ) {
    $table_name = get_db_table( $var );
    global $wpdb;
    $sql = 'SELECT * FROM ' . $table_name . ' ORDER BY name';
    $results = $wpdb->get_results( $sql );
    return $results;   
}

function get_db_table( $option ) {
    global $wpdb;
    $suffix ='';
    switch ( $option ) {
        case 'genres':
            $suffix = 'imc_music_genres';
            break;
        case 'cuisine':
            $suffix = 'imc_venue_cuisine_list';
            break;
        case 'neighborhood':
            $suffix = 'imc_venue_neighborhoods';
            break;
        case 'filters':
            $suffix = 'imc_venue_flag_list';
            break;
    }
    $wpdb->imc = $wpdb->prefix . $suffix;
    return $wpdb->imc;
}
function get_options( $option ) {
    
    $results = display_options( $option );
    $html = '';
    
    foreach ( $results as $result ) {
        $total = '';
        $count = 1;
        if( $option == 'cuisine' ) {
            if( strlen( $result->total ) > 0 ) {
               $total = " ($result->total)";
               $count = $result->total;
            }
        }
        
        $name = str_replace( ' ', '_', $result->name );
        if ( $name != 'Unsure' && $count != 0 ) {
            $html .= '<label for="' . $name . '"> <input type="checkbox" name="' . $option . '[]" id="' . $name . '" value="' . $result->id . '" /> ' . str_replace( '_', ' ', $name ) . $total . '</label>';
        }
    }
    return $html;
}

function get_display_total_options() {
    $total_html = '<select id="display_total" name="display_total">';
    $display = array( 5, 10, 15, 20, 'All' );
    foreach ( $display as $display_total ) {
        if ( $display_total == 15) { 
            $selected = 'selected="selected"'; 
        } else {
            $selected = '';
        }
        $total_html .= '<option ' . $selected . ' value="' . $display_total . '">' . $display_total . '</option>';
    }
    $total_html .= '</select>';
    return $total_html;
}

function get_sort_options() {
    $display_total_options = get_display_total_options(); 
    return $display_total_options;
}

function imc_display_sidebar() {

?>

<div class="widget">
    <form action="" id="imc-filter-venues" name="filters">
        
        <ul style="list-style-type: none;">
            <li>$ - Up to $10 Plates</li>
            <li>$$ - $10+ Plates</li>
            <li>$$$ - $20+ Plates</li>
            <li>$$$$ - $30+ Plates</li>
        </ul>
    <input name="searchname" id="venue-name-search" type="text" class="hidden" />
    <input name="record" id="pagination-record" type="text" class="hidden" />
    <input name="letter" id="pagination-letter" type="text" class="hidden" />
    <div class="search">
        Check any of the boxes below to refine your search
    </div>
    
    
    <div class="filter">
        <h2 class="normal">Neighborhood</h2>
        
        <div class="filter-options">
            <?php echo get_options( 'neighborhood' ); ?>
        </div>
    </div>
    <div class="filter">
        
        <h2 class="normal">Filters</h2>
        
        <div class="filter-options">
            <?php echo get_options( 'filters' ); ?>
            <label for="Breakfast"> <input type="checkbox" name="meal[]" id="Breakfast" value="1"> Breakfast</label>
            <label for="Brunch"> <input type="checkbox" name="meal[]" id="Brunch" value="2"> Brunch</label>
            <label for="Lunch"> <input type="checkbox" name="meal[]" id="Lunch" value="3"> Lunch</label>
            <label for="Dinner"> <input type="checkbox" name="meal[]" id="Dinner" value="4"> Dinner</label>
        </div>
    </div>
    <div class="filter">
        <h2 class="expand">Cuisine</h2>
        
        <div class="filter-options" style="display:block;">
            <?php echo get_options( 'cuisine' ); ?>
        </div>
    </div>
    
    <!--<div class="filter">
        <h2 class="normal">Display Options</h2>
        
        <div class="filter-options">
            Display:
            <?php //echo get_sort_options(); ?>
            per page
        </div>
        
    </div> -->
    <input type="hidden" name="action" value="imc_filter_venues" />
    
</div>
    
<?php
}