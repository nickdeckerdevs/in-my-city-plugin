<?php
    
?>
<h2>Add A Happy Hour</h2>
<form id="imc-add-happy-hour" action="" method="POST">
    <table>
        <tr>
            <td> Day Of Week </td>
            <td> <select name="day" id="day">
                    <?php echo imc_get_days_of_week(); ?>
                </select>
            </td>
        </tr>
        <tr>
            <td> Start Time </td>
            <td>
                <select name="time_start" id="start_time">
                    <?php echo imc_get_hours_of_day( ' Start' ); ?>
                </select>
            </td>
        </tr>
        <tr>
            <td> End Time </td>
            <td>
                <select name="time_end" id="end_time">
                    <?php echo imc_get_hours_of_day( 'n End' ); ?>
                </select>
            </td>
        </tr>
        <tr>
            <td> Type </td>
            <td>
                <select name="type" id="type">
                    <?php echo imc_get_happy_hour_types(); ?>
                </select>
            </td>
        </tr>
        <tr>
            <td> HH Price: </td>
            <td> 
                <input class="small_input" type="text" id="price" name="price" value="" /> <i>No dollar symbol needed</i>
            </td>
        </tr>
        <tr>
            <td> Regular Price </td>
            <td> 
                <input class="small_input" type="text" id="reg_price" name="reg_price" value="" /> <i>No dollar symbol needed</i>
            </td>
        </tr>
        <tr>
            <td> Size </td>
            <td>
                <input class="small_input" type="text" id="ounces" name="ounces" value="" /> <i>ounces</i>
            </td>
        </tr>
        <tr>
            <td> Summary of Individual Happy Hour </td>
            <td>
                <textarea id="summary" name="summary" value="" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="hh_response"></div>
            </td>
        </tr>
    </table>
    <label>
        <input type="hidden" name="venue_id" value="<?php echo $venue_id; ?>" />
        <input type="hidden" name="updated_by" value="<?php echo get_current_user_id(); ?>" />
        <input type="hidden" name="action" value="imc_update_happy_hour" />
    </label>
    <label>
        <input class="button-primary" type="submit" name="imc-submit-happy-hour" id="imc-submit-happy-hour" />
        <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" class="waiting" id="imc-loading" style="display:none;" />
    </label>
    <div id="ajaxResponse">
    </div>
</form>