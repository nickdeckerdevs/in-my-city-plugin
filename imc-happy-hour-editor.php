<?php


function imc_display_happy_hours( $venue_id, $venue_name ) {
    echo '<h1 id="active-venue">' . $venue_name . '</h1>';
    echo '<input type="hidden" name="search-name" id="search-name" value="" />';
    if( imc_get_happy_hour( $venue_id ) ) {
        echo 'Current Happy Hours<br>';
        echo imc_get_happy_hour( $venue_id );
        echo '<p><i>Remove any of the above happy hour items by clicking the corresponding Delete Row Button</i>';
        
    } else { 
        echo '<i>There are no current happy hours</i>, please add one below';
    }
    
}

function imc_add_happy_hours( $venue_id, $venue_name ) {
    include( 'imc-add-happy-hour.php' );
    die();
    
}

function imc_edit_happy_hour( $venue_id, $hh_id ) {
    
}

function imc_test_hh_value( $column ) {
    switch( $column ) {
        case 'id':
        case 'venue_id':
        case 'reg_price':
        case 'updated_by':
        case 'updated_date':
            $column_returns = false;
            break;
        default:
            $column_returns = true;
    }
    return $column_returns;
}

function imc_get_day( $day_value ) {
    $days_of_week = array( 
        'Sunday' => 0, 
        'Monday' => 1, 
        'Tuesday' => 2, 
        'Wednesday' => 3, 
        'Thursday' => 4, 
        'Friday' => 5, 
        'Saturday' => 6
    );
    $new_day = array_search( $day_value, $days_of_week );   
    return $new_day;
}

function imc_convert_hh_column( $column ) {
    
    if( $column == 'day' ) {
        $fixed_column = imc_get_day( $column );
        return $fixed_column;
    }
    $fixed_column = ucfirst( str_replace( '_', ' ', $column ) );
    return $fixed_column;
}
function imc_display_index( $column, $index ) {
    switch( $column ) {
        case 'price':
        case 'ounces':
            $value = $index;
            break;
        default:
            $value = '';
            
    }
    return $value;
}

function getHappyHourName($id) {
    global $wpdb;
    $sql = 'SELECT happy_hour_type FROM ' . $wpdb->prefix . 'imc_happy_hour_type where id = ' . $id;
    $name = $wpdb->get_row($sql);
    return $name->happy_hour_type;
}

function imc_display_happy_hour_array( $value_array ) {
    $string = '<form id="delete_happy_hours_form" action="" method="POST">';
    $string .= '<table id="delete_happy_hours" cellpadding="8">';
    if( $value_array ) {
        foreach( $value_array as $happy_hour ) {
            $string .= '<tr id="' . $happy_hour->id . '"><td>';
            $string .= imc_get_day( $happy_hour->day ) . '</td>'; 
            $string .= '<td> from ' . $happy_hour->time_start . ' to ' . $happy_hour->time_end . '</td>';
            $string .= '<td>$' . $happy_hour->price . ' per ' . $happy_hour->ounces . 'oz - ' . $happy_hour->summary . '</td>';
            $string .= '<td>' . getHappyHourName($happy_hour->happy_hour_type) . '</td>';
            $string .= '<td class="cursor"> Delete Row </td>';   
        }
    }
    $string .= '</table>';
    $string .= '<input type="hidden" name="action" value="imc_delete_happy_hour" />';
    $string .= '<input type="hidden" id="delete_hh" name="happy_hour" value="" />';
    $string .= '</form>';
    return $string;
}

function imc_get_happy_hour( $venue_id ) {
    global $wpdb;
    $hh_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_happy_hour WHERE venue_id = ' . $venue_id . ' ORDER BY `day` ASC';
    $happy_hours = $wpdb->get_results( $hh_sql );
    return imc_display_happy_hour_array( $happy_hours );
}

function imc_fix_pm_time( $hour ) {
    $hour = $hour + 12;
    if( $hour == 24 ) {
        $hour = 0;
    }
    return $hour;
}

function imc_get_hours_of_day( $when ) {
    $time_array = array(
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
    );
    $string = '<option value="">Select A'.$when.' Time</option>';
    $pm_string = '';
    foreach( $time_array as $value ) {
        $string .= '<option value="' . $value . ':00:00">' . $value . ':00 AM</option>';
        $string .= '<option value="' . $value . ':30:00">' .  $value . ':30 AM</option>';
        $pm_string .= '<option value="' . imc_fix_pm_time( $value ) . ':00:00">' . $value . ':00 PM</option>';
        $pm_string .= '<option value="' . imc_fix_pm_time( $value ) . ':30:00">' . $value . ':30 PM</option>';
    }
    return $string . $pm_string;
}

function imc_get_days_of_week() {
    $days_array = array( 
        'Sunday' => '0', 
        'Monday' => '1', 
        'Tuesday' => '2', 
        'Wednesday' => '3', 
        'Thursday' => '4', 
        'Friday' => '5', 
        'Saturday' => '6',
        'Everyday' => '0,1,2,3,4,5,6',
        'Monday - Thursday' => '1,2,3,4',
        'Monday - Friday' => '1,2,3,4,5',
        'Saturday - Sunday' => '6,0',
        'Friday - Sunday' => '5,6,0'
    );
    
    $string = '';
    foreach( $days_array as $day => $value ) {
        $string .= '<option value="' . $value . '">' . $day . '</option>';
    }
    return $string;
}

function imc_get_happy_hour_types() {
    global $wpdb;
    $wpdb->happy_hour_type = $wpdb->prefix . 'imc_happy_hour_type';
    $hh_type_sql = 'SELECT * FROM ' . $wpdb->happy_hour_type;
    $results = $wpdb->get_results( $hh_type_sql );
    $string = '<option value="">Select A Happy Hour Type</option>';
    
    if( $results ) {
        foreach( $results as $result ) {
            $string .= '<option value="' . $result->id . '">' . $result->happy_hour_type . '</option>';
        }
    }
    return $string;
       
}



