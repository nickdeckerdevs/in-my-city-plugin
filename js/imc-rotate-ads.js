
//likes prevention if not logged in
function is_user_logged_in() {
    var data = { action: 'is_user_logged_in' };
    jQuery.post(ajaxurl, data, function(response) {
       if(response == 'yes') {
           return true;
       } else {
           alert('Please log in or create a free account to use the Like Feature.');
           return false;
       }
    });
}
var loaded = 0;
jQuery(document).ready(function($) {
    $('.more-options').on('click', function(e) {
        return false;
    });
    $('a.more-options').on('click', function(e) {
        var number = Math.floor(Math.random() * 100) + 1;
        window.location.href = $('#'+number).text();     
    }); 
     
     
    $('.action-like').on('click', '.jlk', function(e) {
       
       if(is_user_logged_in() === true) {
           //console.log('loggedin');
       } else {
           //console.log('notloggedin');
           e.preventDefault();
       } 
    });
    var counter = 0;
    $('.image-campaign').each(function() {
        counter++;
        current_campaign = $(this).attr('id');
        total_slides = $('#' + current_campaign + '>li').length;
        if(total_slides > 1 && ($(this).attr('id') === 'campaign-4')) {
            $(this).find('li').addClass('rotate');
            $(this).find('li:eq(0)').addClass('active-ad');
            if(loaded == 0) {
                var ad_height = jQuery('#imc-campaign-4 .active-ad img').css('height');
                
                $('#imc-campaign-4 ul').css('height', ad_height).css('min-height', '600px');
                loaded++;
            }

        } else {
            var random_slide = Math.floor((Math.random() * total_slides));
            $(this).find('li:eq(' + random_slide + ')').addClass('current_slide');
        }
    });
});

function switch_images() {
    var active = jQuery('.active-ad');
    jQuery('.active-ad').removeClass('active-ad');
    if(active.next().length === 0) {
        active.parent().find('li:eq(0)').addClass('active-ad');
    } else {
        active.next().addClass('active-ad');
    }
}
jQuery(function() {
    setInterval('switch_images()', 4000);
}); 