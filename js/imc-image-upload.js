/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function imc_set_custom_uploader() {
    var imc_custom_uploader;

    jQuery('#imc_image_url_button').click(function(e) {
        e.preventDefault();
        if(imc_custom_uploader) {
            imc_custom_uploader.open();
            return;
        }
        imc_custom_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            }
        });
        imc_custom_uploader.on('select', function() {
            attachment = imc_custom_uploader.state().get('selection').first().toJSON();
            $('#imc_image_url').val(attachment.url);
            $('#imc_image_preview').attr('src', attachment.url);
        });

        imc_custom_uploader.open();
    });
}

jQuery(document).ready(function($) {
    if($('#imc_image_url_button').length != 0) {
     //   imc_set_custom_uploader();
    }


});
