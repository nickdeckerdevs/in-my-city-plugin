function check_fields() {
    var missing_items = '';
    jQuery('#imc-add-happy-hour option').each(function() {
        if(jQuery(this).val() === '' || jQuery(this).val() === null || jQuery(this).val() === undefined) {
            switch(jQuery(this).attr('id')) {
                case 'day':
                    missing_items += 'Please Select a day of the week<br>';
                    break;
                case 'type':
                    missing_items += 'Please Select a happy hour type<br>';
                    break;
                case 'default':
                    missing_items += 'Please Select all options<br>';
            }
        }
    });
    jQuery('#imc-add-happy-hour input[type=text]').each(function() {
        if(jQuery(this).val() === '' || jQuery(this).val() === null || jQuery(this).val() === undefined) {
            switch(jQuery(this).attr('name')) {
                case 'price':
                    missing_items += 'Please Enter a happy hour price<br>';
                    break;
                case 'reg_price':
                    missing_items += 'Please Enter a regular price<br>';
                    break;
                case 'ounces':
                    missing_items += 'Please Enter a size in ounces<br>';
                    break;
                default:
                    missing_items += 'Please fill out all text fields<br>';
            }
        }
    });
    if(missing_items.length < 10) {
        missing_items = true;
    }
    return missing_items;
        
}

function display_loading( message ) {
    var $height = jQuery('body').height();
    var $width = jQuery('body').width();
    jQuery('body').append('<div id="modal-loading" style="position: fixed; top: 0; left: 0; width: ' + $width + 'px; height: ' + $height + 'px; background: rgba(0,0,0,0.8);"><div id="temp-alert" style="position: fixed; top: 50%; left: 50%; width: 300px; margin-left: -150px;padding: 20px; border-radius: 10px; background: #fff; color: #6fa032;"><p style="text-align: center;margin-bottom: 0;">' + message + '</div></div>');
}
function remove_loading() {
    jQuery('#modal-loading').remove();
}
jQuery(document).ready(function($) {
    $('body').on('click', '#imc-submit-happy-hour', function() {
        $('#imc-add-happy-hour').submit(function(event) {
            event.stopImmediatePropagation();
            var response = check_fields();
            if(response !== true) {
                $('#hh_response').html(response);
                return false;
            } else {
                $('#imc-submit-happy-hour').attr('disabled', true);
                display_loading( 'Saving Happy Hour Details' );
                data = $(this).serializeArray();
                $.post(ajaxurl, data, function (response) {
                    $('#ajaxResponse').html(response);
                    previous_data = $('#imc-venue-results').serializeArray();
                    $.post(ajaxurl, previous_data, function(response) {
                        $('#venue-results').html(response);
                        remove_loading();
                    });
                    
                });
                
                
                return false;
            }
            
       }); 
       
   });
});