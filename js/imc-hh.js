/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



jQuery(document).ready(function($) {
    $('#favorite-hh-form').on('click', 'label', function() { 
        $('.active').removeClass('active');
        current_id = $(this).find('input').attr('id');
        $('#' + current_id).prop('checked', true);
        $(this).addClass('active');
    });
    /* gets the sorting stuff */
    $('body').on('click', '.sort-type span', function() {
        sortish = $(this).attr('id');
        $('#sort').val(sortish); 
        
        getHH(sortish);
    });
    
    function getHH(sortish) {
        $('#imc-loading').show();
        active = jQuery('form input:checked').val();
        data = $('form').serializeArray();
        $.post(ajaxurl, data, function (response) {
            $('.best-hh').html(response.substring(0, response.length -1));
            jQuery('form input[name=favorite][value="' + active + '"]').prop('checked', true);
            $('#imc-loading').hide();
            $('.sort-type span').css('background', 'none').css('color', '#000');
//            console.log(sortish);
            $('#'+sortish).css('background', '#009DD7').css('color', '#fff').css('padding', '8px 12px');
            
        });
        return false;
    }
    
    $('#favorite-hh-form').on('click', function() { 
        getHH('time');    
    });
    /* Removing the select your favorite icon
    var label_counter = 0;
    $('label').on('hover', function() {
        if(label_counter > 3) {
            $('head').append('<style>#favorite-hh-form:before {content:" "!important; background: none!important; }</style>'); 
            $('#select-your-favorite').fadeOut('fast');
        } else {
            label_counter++;
        }
    });
    */
});
