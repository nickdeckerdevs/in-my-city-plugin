/**
 * Created by nickdecker on 10/23/14.
 */



jQuery(document).ready(function($) {
   $('#profile-extended').on('click', '.imc_image_url_button', function(e) {

       
       var $element = $(this);
       
       e.preventDefault();

       frame = wp.media.frames.customHeader = wp.media({
           title: $element.data('choose'),
               library: {
               type: 'image'

           },
           button: {
               text: $element.data('update'),
               multiple: true,
               close: true
           }
       });

       frame.on('select', function() {
          var attachment = frame.state().get('selection').first(),
              link = $element.data('updateLink');
           $element.prev('input').val(attachment.attributes.url);
           $element.parent().find('img').attr('src', attachment.attributes.url);
           copy_urls_to_profile();
       });
       frame.open();
   });
   
   /**
 * Created by nickdecker on 10/23/14.
 */



   $('#premium').on('click', '.imc_image_url_button', function(e) {

       
       var $element = $(this);
       e.preventDefault();

       frame = wp.media.frames.customHeader = wp.media({
           title: $element.data('choose'),
               library: {
               type: 'image'

           },
           button: {
               text: $element.data('update'),
               multiple: true,
               close: true
           }
       });

       frame.on('select', function() {
          var attachment = frame.state().get('selection').first(),
              link = $element.data('updateLink');
           $element.prev('input').val(attachment.attributes.url);
           $element.parent().find('img').attr('src', attachment.attributes.url);
           copy_urls_to_profile();
       });
       frame.open();
   });

});