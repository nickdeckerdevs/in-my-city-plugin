/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




jQuery(document).ready(function($) {
    function rotate_hh_specials(value) {
        var happy_hours = [];
        var target_id = '#' + value;
        var target_class = '.' + value;
        $(target_class).each(function() {
            happy_hours.push($(this).html());
        });
        var hh_counter_max = happy_hours.length;
        var hh_counter = 1; 
        $(target_id).html(happy_hours[0]);
        setInterval(function() {
            if(hh_counter >= hh_counter_max) {
                hh_counter = 0;
            }
            $(target_id).html(happy_hours[hh_counter]);
            hh_counter++;
        }, 3900);
    }
    $('#venue-main').on('click', '.venue-thumbnail', function() {
        var filename = $(this).data('large-image');
        $('#venue-image img').attr('src', filename);
        
    });
    rotate_hh_specials('hh-live');
    
});
   