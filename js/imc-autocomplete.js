var delay = (function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();


jQuery(document).ready(function($) {
    
    $('body').on('click', '.venue-name', function() {
        $('#venue_id').val($(this).attr('id'));
        data = $('#imc-venue-results').serializeArray();
        var loading = '<img src="http:/napleslivenow.com/wp-admin/images/wpspin_light.gif" class="waiting" id="imc-loading" />';
        $('#venue-results').html('Please wait while your happy hours are loaded. ' + loading );
        $.post(ajaxurl, data, function(response) {
            //$('#venue_search').remove();
            $('#venue-results').html(response);
        });
        $('#venue_name').val('');
        return true;
        
    });
    $('#venue_name').keyup(function(e) {
        var input = this.value;
        delay(function() {
            if (input.length > 3) {
                e.preventDefault();
                if (e.keyCode == 38 || e.keyCode == 40) {
                }
                $('.imc-autocomplete').slideDown('fast');
                $('.imc-autocomplete').html('<i>Loading results - please wait just a moment.</i>');
                data = $('#imc-venue-autocomplete').serializeArray();
                $.post(ajaxurl, data, function (response) {
                    $('.imc-autocomplete').html(response);

                });
                return false;
            }
        }, 500);
    });
    $('body').on('click', '.imc-autocomplete li', function(e) {
        $('.imc-autocomplete').slideUp();
    });
    $('body').on('click', '.cursor', function(event) {
           var delete_id = $(this).parent().attr('id');
           $('#delete_hh').val(delete_id);
           data = $('#delete_happy_hours_form').serializeArray();
           $.post(ajaxurl, data, function(response) {
               if(response) {
                   $('#' + delete_id).remove();
                   alert('You Deleted Happy Hour Successfully');
               } else {
                   alert('Your Delete was unsuccessful, please contact support');
               }
           });
           return false;
       });
});
