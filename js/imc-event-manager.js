
jQuery(document).ready(function($) {
    $("body").on("click", ".delete-event", function() {
        $("#delete_post").val($(this).attr("data-postid"));
        $("#delete_event").val($(this).attr("data-eventid"));
        $('#delete_events_form').submit();
    });
    
    $('#delete_events_form').submit(function() {
        data = $(this).serializeArray();
        $.post(ajaxurl, data, function (response) {
//            console.log('in here');
//            console.log(response);
            if(response > 0) {
                $('#'+response).remove();
                alert('Event Was Removed From the System');
            } else {
                alert('There was an error');
            }
            
            
        });
        return false;
    });
});
