/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function($) {
    if($('.body.events-cal')) {
        $('body.events-cal .eventtable').eq(1).hide();
        $('body.events-cal .eventtable').eq(2).hide();
        $('body.events-cal .eventtable').eq(3).hide();
        $('body.events-cal .eventtable').eq(4).hide();
        $('body.events-cal #tribe_events_catdiv').hide();
        $('body.events-cal #tribe_events_event_options').hide();
        $('body.events-cal #postexcerpt').hide();
        $('body.events-cal #postcustom').hide();
        $('body.events-cal #commentstatusdiv').hide();
        $('body.events-cal #commentsdiv').hide();
    }
    if($('li.menu-icon-tribe_events')) {
        $('li.menu-icon-tribe_events li').hide();
    }
    if($('.venues-edit')) {
    }
    
    
    //Ajax start
    $('#happy-hour-edit').submit(function() {
        data = $(this).serializeArray();
        
        $.post(ajaxurl, data, function (response) {
            $('#imc-venue-results').html(response);
            $('.venue-edit-container').hide();
        });
        return false;
    });
    
    

    
   
    
    $('#imc-add-record').submit(function() {
        $('#imc-submit-record').attr('disabled', true);
        $('#imc-loading').show();
        data = $(this).serializeArray();
        
        $.post(ajaxurl, data, function (response) {
            $('#imc-record-results').html(response);
            $('#imc-loading').hide();
            $('#imc-submit-record').attr('disabled', false);
        });
        return false;
    });
    $venueForm = $('#formClear').html();
    $('#imc-add-venue').submit(function() {
        check_fields();
        $('#imc-submit-venue').attr('disabled', true);
        $('#imc-loading').show();
        data = $(this).serializeArray();
        $.post(ajaxurl, data, function (response) {
            $('#imc-venue-results').html(response);
            $('#imc-loading').hide();
            $('#imc-submit-venue').attr('disabled', false);
            $('#formClear').html($venueForm);
        });
        return false;
        
        
    });

    
    $('body').on('click', '.edit-band', function() {
        
        var band_id = $(this).attr('id');
        $('#band-id').val(band_id);
        data = $('#edit-band-form').serializeArray();
        $.post(ajaxurl, data, function (response) {
            $('.edit-band-admin-details').html(response);
            window.scrollTo(0,0);
            $('#imc-loading').hide();
        });
    });
    
    $('body').on('click', '#imc-remove-band', function() {
       $('#imc-save-band-form').append('<input id="deleteband" type="hidden" name="deleteband" value="1" />');
//       console.log('added detel area');
    });
    
    $('body').on('submit', '#imc-save-band-form', function() {
//        console.log($(this));
        data = $(this).serializeArray();
        $.post(ajaxurl, data, function (response) {
            alert(response);
            window.location.reload();
        });
        return false;
    });
    
    $('body').on('click', '.approve-items .delete-item', function() {
        $('form').append('<input type="hidden" name="delete" value="1" id="delete-field" />');
        var id = $(this).parent().attr('id');
        $('#approve-id').val(id);
        data = $('form').serializeArray();
        $.post(ajaxurl, data, function(response) {
            if(response != '0') {
                $('tr#'+id).remove();
                $('#delete-field').remove();
                alert(id + ' was deleted');
            } else {
                alert(id + ' couldn\'t be deleted');
                $('#delete-field').remove();
            }
        });
       
    });
    
    $('body').on('click', '.approve-items .approve-item', function() {
        var id = $(this).parent().attr('id');
        $('#approve-id').val(id);
        data = $('form').serializeArray();
        $.post(ajaxurl, data, function(response) {
            if(response != '0') {
                $('tr#'+id).remove();
                alert(id + ' was approved');
            } else {
                alert(id + ' couldn\'t be approved');
            }
        });
       
    });
    
    $('body').on('click', '.approve-items #approve-all', function() {
        var counter = 0;
        var ids = '';
        $('tr').each(function() {
            var id = $(this).attr('id');
            if(id != undefined) {
                if(counter > 0) {
                    ids += ',';
                }
                ids += id;
                counter++;
            }
        });
        $('#approve-id').val(ids);
        data = $('form').serializeArray();
        $.post(ajaxurl, data, function(response) {
             
            if(response != '0') {
                $('tr').remove();
                alert('All Approved');
            } else {
                alert('There was an error, refreshing the page -- please try again');
                window.location.reload();
            }
        });
    });
    
    
    //import ajax
    $('#imc-process-textarea').submit(function() {
        
        $('#imc-loading').show();
        
        data = $(this).serializeArray();
        
        $.post(ajaxurl, data, function (response) {
            $('#imc-venue-results').html(response);
            $('#imc-loading').hide();

            
        });
        return false;
        
    });
    function check_fields() {
        $('form select').each(function() {
            if($(this).val() === '' || $(this).val() === null || $(this).val() === undefined) {
                switch($(this).attr('name')) {
                    case 'state':
                        $(this).val('zz');
                        break;
                    case 'cuisine1':
                        $(this).val('29');
                        break;
                    case 'neighborhood':
                        $(this).val('9');
                        break;
                    case 'price_range':
                        $(this).val('5');
                        break;
                    case 'default':
                        $(this).val('0');
                }
            }
        });
        $('form input[type=text]').each(function() {
            if($(this).val() === '' || $(this).val() === null || $(this).val() === undefined) {
                $(this).val(' ');
            }
        });
        $('form input[type="radio"]').each(function() {
            if($(this).is(':checked')) {
                var is_checked = $(this).attr('name') + '_hidden';
                $('#' + is_checked).val($(this).val());
            }
        });
        var meal_type_string, venue_type_string;
        var meal_type_counter = 0;
        var venue_type_counter = 0;
        $('form input[type="checkbox"]').each(function() {
            if($(this).is(':checked')) {
                switch($(this).attr('name')) {
                    case 'meal_types':
                        if(meal_type_counter === 0) {
                            meal_type_string = $(this).val();
                            meal_type_counter++;
                        } else {
                            meal_type_string += ', ' + $(this).val();
                        }
                        break;
                    case 'venue_flags':
                        if(venue_type_counter === 0) {
                            venue_type_string = $(this).val();
                            venue_type_counter++;
                        } else {
                            venue_type_string += ', ' + $(this).val();
                        }
                }

            }
            
        });
        if(meal_type_string === undefined) {
            $('#meal_types_hidden').val('5');
        } else {
            $('#meal_types_hidden').val(meal_type_string);
        }
        if(venue_type_string === undefined) {
            $('#venue_flags_hidden').val('11');
        } else {
            $('#venue_flags_hidden').val(venue_type_string);
        }
    }
});