var delay = (function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();
var autocomplete = 0;
var paidOrFree = 'Free';
function removeHelper() {
    if($('.remove-after-input').length > 0) {
        $('.remove-after-input').remove();
    }
}

jQuery(document).ready(function($) {
    $('.timepicker').on('click', 'input', function() {
        removeHelper();
    });
    $('.timepicker').on('click', 'select', function() {
        removeHelper();
    });
    autocomplete = 0;
    $('.event-category').on('click', 'input', function() {
        var selected_category = $(this).val();
        switch(parseInt(selected_category)) {
            case 1:
                $('#band-input').fadeIn();
                break;
            case 0:
            case 3:
            case 4:
            case 5:
            case 6:            
            default:
                $('#band-input').fadeOut();
        }
        
        
    });
    $("input:radio[name=event-cost]").click(function() {
        paidOrFree = $(this).val();
    });

    $('.location-autocomplete').on('click', 'input', function() {
        
        var location_type = $(this).val();
        if(location_type == 1) {
            autocomplete = 1;
            $('#venue-name').attr('autocomplete', 'off');
        } else {
            autocomplete = 0;
            $('#venue-name').attr('autocomplete', 'on');
        }
        
    });
    
    $('ul.uk-navbar-nav.uk-hidden-small>li:eq(1)').addClass('uk-active');     
    function check_inputs() {
        var message = '';
        $('.e-required').each(function() {
            if($(this).val() === null || $(this).val() === '') {
                message += 'Please enter your ' + $(this).attr('id').replace('-', ' ') + '<br>';
            }
        });
        return message;
    }
    
    $('#imc-create-event').submit(function() {
        $('#imc-process-event').attr('disabled', true);
        var inputs_checked = check_inputs();
        if(inputs_checked.length > 1) {
            $('#imc-event-result').html(check_inputs);
            $('#imc-process-event').attr('disabled', false);
        } else {
            display_loading( 'Creating your event, please wait just a moment' );
            if(paidOrFree == 'Free') {
                var temp = $('#description').val();
                if(temp.indexOf('This is a Free Event') > -1) {
                    
                } else {
                    $('#description').val(temp + ' - This is a Free Event');
                }
            }
            //$('#description').val()
            data = $(this).serializeArray();
            $.post(ajaxurl, data, function(response) {
                $('#imc-loading').hide();
                $('#imc-process-event').attr('disabled', false);
                alert('Your event has been created.');
                if(response == 'clear') {
                    window.location.reload(true);
                } else {
                    $('.logo').after('<div class="remove-after-input" style="background:#009DD7;padding:15px;border-radius: 4px;">Please change the dates on your duplicate</div>');
                    $('#imc-event-result').html('<div class="remove-after-input">Please change your information then create your duplicate event.</div>');
                    window.scrollTo(0,0);
                }
                
                
                //window.location.href = response;
                remove_loading();
                $('#imc-process-event').attr('disabled', false);
            });
           return false;
        }
        return false;
    });
    
    function display_loading( message ) {
        var $height = jQuery('body').height();
        var $width = jQuery('body').width();
        jQuery('body').append('<div id="modal-loading" style="position: fixed; top: 0; left: 0; width: ' + $width + 'px; height: ' + $height + 'px; background: rgba(0,0,0,0.8);"><div id="temp-alert" style="position: fixed; top: 50%; left: 50%; width: 300px; margin-left: -150px;padding: 20px; border-radius: 10px; background: #fff; color: #6fa032;"><p style="text-align: center;margin-bottom: 0;">' + message + '</div></div>');
    }
    function remove_loading() {
        jQuery('#modal-loading').remove();
    }
    function imc_fill_in_venue( beautiful_object ) {
        $('.imc-autocomplete').slideUp('fast');
        display_loading('Getting the Venue\'s Details...');
        $('#imc-process-event').attr('disabled', false);
        $.post(ajaxurl, beautiful_object, function(response) {
            $('.imc-autocomplete').html(response);
            $('#venue-address').html(response);
            $('#venue-name').val(beautiful_object.name);
            $('#website').focus();
            remove_loading();
        });
        
    }
    if($('#start-date').length > 0) {
        $('#start-date').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('#end-date').datepicker({
            dateFormat: 'yy-mm-dd'
        });
    }
    var list_counter = -1;
    $('#venue-name').keyup(function(e) {
        if(autocomplete == 1) {
            var input = this.value;
            delay(function () {
                if (input.length >= 3) {
                    $('#imc-process-event').attr('disabled', true);
                    if (e.keyCode === 13) {
                        var selected_element = {
                            id: $('.auto-focus').attr('id'),
                            name: $('.auto-focus').data('name'),
                            action: 'imc_get_venue_details'
                        };
                        imc_fill_in_venue(selected_element);
                    }
                    if (e.keyCode === 38 || e.keyCode === 40) {
                        var max_items = (jQuery('.imc-autocomplete li').length - 1);
                        if (e.keyCode === 40) {
                            list_counter++;
                        } else {
                            list_counter--;
                        }
                        $('.imc-autocomplete li').removeClass('auto-focus');
                        $('.imc-autocomplete li:eq(' + list_counter + ')').addClass('auto-focus');
                        if (e.keyCode === 40) {
                            if (list_counter >= max_items) {
                                list_counter = -1;
                            }
                        } else {
                            if (list_counter < 0) {
                                list_counter = max_items;
                            }
                        }
                        return true;
                    }
                    $('.imc-autocomplete').slideDown();
                    var loading = '<img src="http://napleslivenow.com/wp-admin/images/wpspin_light.gif" class="waiting" id="imc-loading" />';
                    $('.imc-autocomplete').html('Please wait while similarly named restaurants are loaded are loaded. If this isn\t a restaurant in the system, please ignore this and finish typing out the Location Name.  ' + loading);
                    var data = $('#venue-name').serializeArray();
                    data[1] = {name: 'action', value: 'imc_venue_autocomplete'};
                    
                    $.post(ajaxurl, data, function (response) {
                        if (response.length < 2) {
                            $('.imc-autocomplete').html('No Restaurants Found that match the string "' + data[0]['value'] + '". Please type in the event details below along with the Full Name of the Location the event is happening <div style="color: red;" class="float-right" id="closeThis">Close X</span>');
                        } else {
                            $('.imc-autocomplete').html(response);
                        }
                    });
                    return false;
                }
            }, 500);
        }
    });


     $('body').on('click', '.imc-autocomplete li', function(e) {
        var selected_element = { id: $(this).attr('id'), name: $(this).data('name'), action: 'imc_get_venue_details' };
        imc_fill_in_venue( selected_element );
        
     });
     
     $('body').on('click', '#closeThis', function() {
         $('.imc-autocomplete').hide();
         $('#venue-name').val('');
     });
     
    
    jQuery(document).ready(function($) {
        $('.event-category').on('click', 'input', function() {
//            console.log($(this));
            if($(this) == 'Other Event Not Listed') {
                var category = '';
            } else {
                var category = '[ ' + $(this).parent().text().trim() + ' ]';
            }
            
            var description = $('#description').val();
            if(description.indexOf('[') != -1 && description.indexOf(']') != -1) {
                description = description.replace(/\[[^\]]*?\]/, category);

            } else {
//                console.log('includes NOTHING');
                description = description + '  ' + category;
            }
//            console.log('THE sCRIPT: ' + description);
            $('#description').val(description);
        });
        
        $('#imc-create-event').on('click', '#imc_image_url_button', function(e) {

           
           var $element = $(this);
           
           e.preventDefault();

           frame = wp.media.frames.customHeader = wp.media({
               title: $element.data('choose'),
                   library: {
                   type: 'image'

               },
               button: {
                   text: $element.data('update'),
                   multiple: true,
                   close: true
               }
           });

           frame.on('select', function() {
              var attachment = frame.state().get('selection').first(),
                  link = $element.data('updateLink');
               $element.prev('input').val(attachment.attributes.url);
               $element.parent().find('img').attr('src', attachment.attributes.url);
               copy_urls_to_profile();
           });
           frame.open();
       });
    });
});