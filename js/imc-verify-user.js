/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function approve_user(id) {
    
    jQuery('#user-id').val(id);
    var data = jQuery('#imc-verify-user').serializeArray();
    jQuery.post(ajaxurl, data, function(response) {
        jQuery('#feedback').html(response);
    });
}


jQuery(document).ready(function($) {
   $('body').on('click', '.verify-user li', function() {
       $id = parseInt($(this).find('span').text());
       approve_user($id);
   });
});