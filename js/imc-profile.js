/**
 * Created by nickdecker on 10/23/14.
 */
function disable_profile_fields(ids_array) {
    for(var i = 0; i <= ids_array.length; i++) {
        jQuery(ids_array[i]).attr('disabled', 'disabled');
        jQuery(ids_array[i]).parent().parent().css('display', 'none');
    }
}


jQuery(document).ready(function($) {
    populate_extended_profile();
    hide_more_stuff();
    fields = [ "#url", "#waterfront", "#cuisine1", "#cuisine2", "#cuisine3", "#neighborhood", "#price_range", "#outdoor", "#live_entertainment", "#meal_types", "#venue_flags", "#images", "#is_active", "#subscription_level", "#venue_id", "#logo" ];
    disable_profile_fields(fields);


    /* Triggers to copy over selections to actual fields that will submit to the profile */

    $('#profile-extended [id^="cuisine"]').on('change', function() {
        copy_cuisine_to_profile();
    });
    $('#profile-extended #price_range_select').on('change', function() {
        copy_select_to_profile($(this));
    });
    $('#profile-extended #outdoor_select').on('change', function() {
        copy_select_to_profile($(this));
    });
    $('#profile-extended #waterfront_select').on('change', function() {
        copy_select_to_profile($(this));
    });
    $('#profile-extended #is_active_select').on('change', function() {
        copy_select_to_profile($(this));
    });
    $('#profile-extended .venue_flags_box').on('click', function() {
        copy_boxes_to_profile('venue_flags_box');
    });
    $('#profile-extended .meal_types_box').on('click', function() {
        copy_boxes_to_profile('meal_types_box');
    });
});

function remove_select(id) {
    return id.replace('_select', '');
}

function populate_select_without_zero(element) {
    if(element.val() != '0') {
        jQuery('#' + element.attr('id') + '_select').val(element.val());
    }
}

function populate_select_with_zero(element) {
    jQuery('#' + element.attr('id') + '_select').val(element.val());
}
function populate_check_boxes(element) {
    var values = element.val().split(',');
    for(var i = 0; i <= values.length; i++) {
        jQuery('.' + element.attr('id') + '_box[value="' + values[i] + '"]').prop('checked', true);
    }
}

function populate_logo() {
    var image = jQuery('#logo').val();
    jQuery('#logo_image').val(image);
    jQuery('#imc_logo_preview').attr('src', image);
}


function populate_images() {
    var images = jQuery('#images').val().split(',');
    for(var i = 0; i <= images.length; i++) {
        var image = images[i];
        jQuery('.imc_image_venue_profile').eq(i).val(image);
        jQuery('.image-preview').eq(i).attr('src', image);
    }
}

function hide_more_stuff() {
    var elements = [ "#description", "h3", "#admin_bar_front" ];
    for(var i=0; i <= elements.length - 1; i++) {
        var element = elements[i];
        if(element.indexOf('#') != 0) {
            jQuery(element).hide();
        } else {
            jQuery(element).parent().parent().hide();
        }

    }

}

function populate_extended_profile() {
    if(jQuery('#is_active').val() == '') {
        jQuery('#is_active').val(1);
    }
    jQuery('input[id^="cuisine"]').each(function() {
        populate_select_without_zero(jQuery(this));
    });
    /*jQuery('#images').val('http://hdcomputerwallpaper.com/wp-content/uploads/2013/12/Puppy-images.jpg,http://graemethomasonline.com/wp-content/uploads/2011/04/body-composition-changes.jpg,http://4.bp.blogspot.com/-BrLngyC1Bto/UpO-IY1kcNI/AAAAAAAALqA/1bSQRy3G_gU/s1600/Image-b%C3%A9b%C3%A9-facebook-8.jpg,http://www.online-image-editor.com/styles/2013/images/example_image.png,http://www.online-image-editor.com/styles/2013/images/example_image.png');
    jQuery('#logo').val('https://wiki.creativecommons.org/images/2/2c/Casey_image_cc0.jpg'); */
    populate_select_without_zero(jQuery('#price_range'));
    populate_select_with_zero(jQuery('#outdoor'));
    populate_select_with_zero(jQuery('#waterfront'));
    populate_select_with_zero(jQuery('#is_active'));
    populate_check_boxes(jQuery('#venue_flags'));
    populate_check_boxes(jQuery('#meal_types'));
    populate_logo();
    populate_images();

}

function copy_urls_to_profile() {
    var links = '';
    jQuery('.imc_image_venue_profile').each(function() {
        var $url = jQuery(this).val().trim();
        if($url != 'http://' || $url != '') {
            if(links.length > 1) {
                links += ',' + $url;
            } else {
                links += $url;
            }

        }
    });
    var logo_import = jQuery('#logo_image');
    if(logo_import.val() != 'http://' || logo_import.val() != '') {
        update_profile_item('logo', logo_import.val());
    }
    update_profile_item('images', links);
}
function copy_cuisine_to_profile() {
    jQuery('#profile-extended [id^="cuisine"]').each(function() {
        var $cuisine = jQuery(this).val();
        var $id = remove_select(jQuery(this).attr('id'));
        if($cuisine != 'Select A Cuisine Type') {
            jQuery('#' + $id).val($cuisine);
        }
    });
}

function copy_select_to_profile(element) {
    var id = remove_select(element.attr('id'));
    jQuery('#' + id).val(element.val());
}
function copy_boxes_to_profile(target) {
    var value ='';
    jQuery('.' + target).each(function() {
        var checked = jQuery(this).attr('checked');
        if(checked != undefined) {
            if(value.length > 0) {
                value += ',' + jQuery(this).val();
            } else {
                value = jQuery(this).val();
            }
        }
    });
    var id = target.replace('_box', '');
    jQuery('#' + id).val(value);
}



function update_profile_item(id, data) {
    jQuery('#' + id).removeAttr('disabled');
    jQuery('#' + id).val(data);
    jQuery('#' + id).attr('disabled', 'disabled');
}