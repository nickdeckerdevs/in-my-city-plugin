/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


jQuery(document).ready(function($) {
    $('a').each(function() {
        $(this).attr('href', '#');
    });
    $('body').on( 'keyup', 'input', function(e) {
        var val = $(this).val();
        var target = '#' + $(this).attr('id').replace('replace-', '');
        $(target + ' h3').html(val);
    });
    $('body').on('submit', '#edit-slider', function() {
        $('#imc-edit-venue-button').attr('disabled', true);
        $('#imc-loading').show();
        $('input').each(function() { 
            $(this).prop('disabled', false); 
        });
        data = $(this).serializeArray();
        $.post(ajaxurl, data, function (response) {
            $('#imc-loading').hide();
            $('#imc-edit-venue-button').attr('disabled', false);
            $('input').each(function() { 
                $(this).prop('disabled', true); 
            });
            $('#slider-preview').append(response);
        });
        return false;
    });
    
    
    
});