/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function display_modal_alert( $message ) {
    var $height = jQuery('body').height();
    var $width = jQuery('body').width();
    jQuery('body').append('<div id="modal-background" style="position: fixed; top: 0; left: 0; width: ' + $width + 'px; height: ' + $height + 'px; background: rgba(0,0,0,0.8);"><div id="temp-alert" style="position: fixed; top: 50%; left: 50%; width: 300px; margin-left: -150px;padding: 20px; border-radius: 10px; background: #fff; color: #6fa032;"><p style="text-align: center;margin-bottom: 0;">' + $message + '<br><br>Click Anywhere to return to results</div></div>');
    setTimeout(function() {
        remove_filter_alert();
    }, 3000);
}

function remove_filter_alert() {
    jQuery('#modal-background').remove();
}

jQuery(document).ready(function($) {
    $('body').on('click', '#modal-background', function() {
       remove_filter_alert();
    });
    $('body').on('click', '#temp-alert', function() {
       remove_filter_alert();
    });
    
    function get_new_results(e) {
        e.stopPropagation();
        data = $('#imc-filter-band').serialize();
        $.post(ajaxurl, data, function (response) {
            if(response.substr(response.length - 1) === '0') {
                response = response.slice(0, -1);
                var $previous_total = $('#result-total').text();
                $('.tm-content').html(response);
                var total_results = $('.band').length;
                if(parseInt($previous_total) === total_results ) {
                    display_modal_alert('Your Selection did not change the results');
                }
                if(total_results > 1) {
                    var plural = 's';
                } else {
                    var plural = '';
                }
                response = response + 'Showing <span id="result-total">' + total_results + '</span> result' + plural + '. Change your filter options to see more results >>';
            }
            $('.tm-content').html(response);
        });
    }
    $('#modal').hide();
    $('body').append($('#modal-temp').html());
    $('#modal-temp').remove();
    $('#add-musician').on('click', 'span', function() {
        $('#modal').fadeIn();
        $('#add-musician-input').fadeIn();
    });
    
    $('#modal').on('click', '.close-modal', function() {
        close_modal();
    });
    
    function close_modal() {
        $('#modal').hide();
    }
    window.document.onkeyup = function(e) {
        if(!e) {
            e = event;
        }
        if(e.keyCode === 27) {
            close_modal();
        }
    };
    $('#imc-add-band-form').submit(function() {
        $('#imc-add-band').attr('disabled', true);
        $('#imc-loading').show();
        data = $(this).serializeArray();
        $.post(ajaxurl, data, function (response) {
            if(response.substr(response.length - 2) === '/0') {
                url = response.substr(0, response.length - 2);
            }
            window.location = url;
            $('#imc-loading').hide();
            $('#imc-add-band').attr('disabled', false);
        });
        return false;
    });
    $('body').on('click', '.filter-options input', function(e) {
        get_new_results(e);
    });
    
    
});