<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function imc_record_edit( $table, $category, $category_plural ) {

    global $wpdb;
    $sql = 'SELECT * FROM ' . $table . ' ORDER BY name ASC';
    $records = $wpdb->get_results($sql);
    ?>

    <div class="wrap">
        <h2>Add New <?php echo $category; ?></h2>
        <form method="post" action="" id="imc-add-record"> <!-- action="admin-post.php" -->
        <label for="<?php echo $category; ?>">
            <input type="hidden" name="action" value="imc_insert_records" />
            <?php wp_nonce_field( 'imc-record-nonce' ); ?>
            <input type="text" id="records" name="records" placeholder="type <?php echo $category_plural; ?> here" />
            <input type="hidden" name="<?php echo $category; ?>_hidden" value="Y" />
            <input type="hidden" name="table" id="desk" value="<?php echo $table; ?>" />
            <input type="submit" class="button-primary" id="imc-submit-record" name="submit_ajax">
            <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" class="waiting" id="imc-loading" style="display:none;" />
            <p class="infoText">Place a comma between each <?php echo $category; ?> if adding multiple neighborhoods.</p>

        </label>
            <div><span id="feedback"></span></div>
        </form>

        <h2>Current <?php echo $category_plural; ?></h2>

        <ul class="typeList" id="imc-record-results">
            <?php
            foreach($records as $record) {
                echo '<li id="'.$record->id.'">'.$record->name.'</li>';
            } ?>
        </ul>


    </div>
<script>
    $(function() { 
        $('.custom-menu-primary').addClass('js-enabled');
        $('.custom-menu-primary .hs-menu-flow-horizontal').before('<a class="mobile-trigger"><span></span></a>');
        $('a.mobile-trigger').click(function(){ 
            $(this).next('.custom-menu-primary .hs-menu-flow-horizontal').slideToggle(250);
            $('body').toggleClass("mobile-open");
            return false;
        });
    });0
</script>
    <?php
}

