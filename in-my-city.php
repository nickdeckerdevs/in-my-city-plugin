<?php
/**
 * In My City
 *
 * An example WordPress plugin used to show how to include templates with your
 * plugins and programmatically add them to the active theme
 *
 * @package   
 * @author    Nick Decker <nick@chooseimpulse.com>
 * @license   GPL-2.0+
 * @link      
 * @copyright 2014 Impulse Creative
 *
 * @wordpress-plugin
 * Plugin Name:       In My City
 * Plugin URI:        
 * Description:       Keep your city updated about what is going on
 * Version:           0.0.1
 * Author:            Nick Decker
 * Author URI:        http://meepfacedecker.com
 * Text Domain:       pte-locale
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 * GitHub Plugin URI: 
 */

//require_once('kint/Kint.class.php');

date_default_timezone_set('America/New_York');
function imc_activation() {
    global $wpdb;
    $table_name = $wpdb->prefix.'imc_venue';
    
    $sql = 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        wp_post_id int(255) DEFAULT NULL,
        name varchar(255) DEFAULT NULL,
        address varchar(255) DEFAULT NULL,
        address2 varchar(255) DEFAULT NULL,
        city varchar(255) DEFAULT NULL,
        state text(10) DEFAULT NULL,
        zip int(15) DEFAULT NULL,
        phone varchar(255) DEFAULT NULL,
        claimed_id int(11) DEFAULT NULL,
        contact_name varchar(255) DEFAULT NULL,
        contact_phone varchar(255) DEFAULT NULL,
        cuisine1 int(1) DEFAULT NULL,
        cuisine2 int(1) DEFAULT NULL,
        cuisine3 int(1) DEFAULT NULL,
        website varchar(255) DEFAULT NULL,
        neighborhood int(11) DEFAULT NULL,
        price_range int(5) DEFAULT NULL,
        logo varchar(255) DEFAULT NULL,
        image varchar(255) DEFAULT NULL,
        waterfront varchar(175) DEFAULT NULL,
        outdoor int(1) DEFAULT NULL,
        is_claimed int(1) DEFAULT NULL,
        meal_types varchar(25) DEFAULT NULL,
        venue_flags varchar(255) DEFAULT NULL,
        created_by int(11) DEFAULT NULL,
        created_date DATETIME DEFAULT NULL, 
        updated_by int(11) DEFAULT NULL,
        updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        is_active int(1) DEFAULT NULL,
        wp_guid text NOT NULL,
        is_paid int(1) DEFAULT NULL
        PRIMARY KEY (id)
        KEY (name)
    );';
        
    $table_name = $wpdb->prefix.'imc_venue_owner';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        wordpress_id int(11) DEFAULT NULL,
        name text(255) DEFAULT NULL,
        email varchar(255) DEFAULT NULL,
        phone varchar(255) DEFAULT NULL,
        is_paid int(1) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_venue_hours';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        day text(25) DEFAULT NULL,
        time_start TIME DEFAULT NULL,
        time_end TIME DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_meal_types';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        brunch int(1) DEFAULT NULL,
        breakfast int(1) DEFAULT NULL,
        lunch int(1) DEFAULT NULL,
        dinner int(1) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_venue_flags';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        gluten_free int(1) DEFAULT NULL,
        pet_friendly int(1) DEFAULT NULL,
        reservations int(1) DEFAULT NULL,
        beer int(1) DEFAULT NULL,
        wine int(1) DEFAULT NULL,
        alcohol int(1) DEFAULT NULL,
        late_night_hh int(1) DEFAULT NULL,
        coupons int(1) DEFAULT NULL,
        live_entertainment int(1) DEFAULT NULL,
        craft_beer int(1) DEFAULT NULL,
        sports_bar int(1) DEFAULT NULL,
        televisions int(20) DEFAULT NULL,
        hometown varchar(255) DEFAULT NULL,
        sports_types varchar(150) DEFAULT NULL,
        indoor_seating int(1) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_venue_flag_list';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        name varchar(255) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_venue_paid';
    
    /* Need to change photo/menu tables to ids inside a table.  Also in class */
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        website varchar(255) DEFAULT NULL,
        photos text(65000) DEFAULT NULL, 
        twitter varchar(255) DEFAULT NULL,
        yelp varchar(255) DEFAULT NULL,
        googleplus varchar(255) DEFAULT NULL,
        foursquare varchar(255) DEFAULT NULL,
        facebook varchar(255) DEFAULT NULL,
        urbanspoon varchar(255) DEFAULT NULL,
        instagram varchar(255) DEFAULT NULL,
        details varchar(255) DEFAULT NULL,
        menu varchar(255) DEFAULT NULL,
        beer_on_taps varchar(255) DEFAULT NULL,
        craft_beers varchar(255) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
        
    $table_name = $wpdb->prefix.'imc_venue_neighborhoods';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        name varchar(255) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_venue_cuisine';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        cuisine_type varchar(255) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_venue_cuisine_list';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        name varchar(255) DEFAULT NULL,
        total int(100) NOT NULL DEFAULT "0",
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_sports_market';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        city varchar(150) DEFAULT NULL,
        team varchar(150) DEFAULT NULL,
        sport varchar(25) DEFAULT NULL,
        PRIMARY KEY (id)
        );';
    
    $table_name = $wpdb->prefix.'imc_venue_slider';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        cuisine varchar(75) DEFAULT NULL,
        special varchar(255) DEFAULT NULL,
        hh_hours varchar(50) DEFAULT NULL,
        link varchar(255) DEFAULT NULL,
        image varchar(255) DEFAULT NULL,
        is_active int(1) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_happy_hour';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        venue_id int(11) DEFAULT NULL,
        day int(10) DEFAULT NULL,
        time_start TIME DEFAULT NULL,
        time_end TIME DEFAULT NULL,
        time_end_fix TIME DEFAULT NULL,
        after_midnight int(1) DEFAULT NULL,
        all_day int(1) DEFAULT NULL,
        happy_hour_type int(10) DEFAULT NULL,
        price decimal(13,4) DEFAULT NULL,
        reg_price decimal(13,4) DEFAULT NULL,
        ounces int(10) DEFAULT NULL,
        price_per_oz int(10) DEFAULT NULL,
        summary varchar(255) DEFAULT NULL,
        points decimal(10) DEFAULT NULL,
        updated_by int(11) DEFAULT NULL,
        updated_date TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix.'imc_happy_hour_type';
    
    $sql .= 'CREATE TABLE '.$table_name.' (
        id int(11) NOT NULL AUTO_INCREMENT,
        happy_hour_type varchar(255) DEFAULT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix . 'imc_events';
    
    $sql .= 'CREATE TABLE ' . $table_name . ' (
        id int(100) NOT NULL AUTO_INCREMENT,
        title varchar(255) NOT NULL,
        image text NOT NULL,
        event_category int(11) NOT NULL,
        venue_id int(11) NOT NULL,
        event_id int(111) NOT NULL,
        start_date datetime NOT NULL,
        end_date datetime NOT NULL,
        event_band int(111) NOT NULL,
        approved int(1) NOT NULL DEFAULT "0",
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix . 'imc_top_rated';
    
    $sql .= 'CREATE TABLE ' . $table_name . ' (
        id int(10) NOT NULL AUTO_INCREMENT,
        venue_id int(100) NOT NULL,
        name text NOT NULL,
        url text NOT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix . 'imc_music_genres';
    
    $sql .= 'CREATE TABLE ' . $table_name . ' (
        id int(10) NOT NULL AUTO_INCREMENT,
        name text NOT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix . 'imc_bands';
    
    $sql .= 'CREATE TABLE ' . $table_name . ' (
        id int(10) NOT NULL AUTO_INCREMENT,
        band_name text NOT NULL,
        website text NOT NULL,
        soundcloud text NOT NULL,
        facebook text NOT NULL,
        youtube text NOT NULL,
        twitter text NOT NULL,
        instagram text NOT NULL,
        image text NOT NULL,
        genre varchar(255) NOT NULL,
        bio text NOT NULL,
        post_id int(10) NOT NULL,
        url text NOT NULL,
        approved int(1) NOT NULL DEFAULT "0",
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix . 'imc_top_rated_key';
    
    $sql .= 'CREATE TABLE ' . $table_name . ' (
        id int(10) NOT NULL AUTO_INCREMENT,
        hh_type int(10) NOT NULL,
        steps varcahr(255) NOT NULL,
        PRIMARY KEY (id)
    );';
    
    $table_name = $wpdb->prefix . 'imc_rated_venues';
    
    $sql .= 'CREATE TABLE ' . $table_name . ' (
        venue_id int(100) NOT NULL UNIQUE,
        d_bottle float(10) NOT NULL,
        d_draft float(10) NOT NULL,
        i_bottle float(10) NOT NULL,
        i_draft float(10) NOT NULL,
        wine float(10) NOT NULL,
        well float(10) NOT NULL,
        ah int(10) NOT NULL,
        lnb int(255) NOT NULL,
        points int(10) NOT NULL,
        PRIMARY KEY (venue_id)
    );';
    
    require_once(ABSPATH.'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}

register_activation_hook(__FILE__, 'imc_activation');
function imc_deactivation() {
    
}
register_deactivation_hook(__FILE__, 'imc_deactivation');


function imc_styles_style() {
    wp_enqueue_style('imc-styles', plugins_url('imc-styles.css', __FILE__));
}
add_action('admin_enqueue_scripts', 'imc_styles_style');
add_action('login_enqueue_scripts', 'imc_styles_style');

register_activation_hook( __FILE__, 'prefix_activation' );

function prefix_activation() {
    wp_schedule_event( time(), 'daily', 'imc_housekeeping' );
}

add_action( 'imc_housekeeping', 'mad_math' );
function mad_math() {
    include( 'happy_hour_daily.php' );
    calculate_scores();
}

/* Form submission actions */
add_filter('upload_mimes', 'my_upload_mimes');

function my_upload_mimes ( $existing_mimes=array() ) {
    $existing_mimes['csv'] = 'text/csv';
    return $existing_mimes;
}
add_action( 'admin_post_add_cuisine', 'add_cuisine' );


function imc_admin_actions() {
    $branding = '/images/impulse_logo.png';
    global $imc_cuisine_edit, $imc_venue_slider, $imc_venue_edit;
    $imc_impulse_settings = add_menu_page( 'Impulse Creative', 'Impulse Creative', 'edit_users', 'imc-impulse-creative', 'imc_display_help', plugins_url($branding, __FILE__), '58' );
    $imc_venue_edit = add_submenu_page( 'imc-impulse-creative', 'Create Venue', 'Create Venue', 'edit_users', 'imc-venue-editor', 'imc_venue_editor_admin' );  
    $imc_venues_edit = add_submenu_page( 'imc-impulse-creative', 'Edit Venues', 'Edit Venues', 'edit_users', 'imc-venues-editor', 'imc_venue_editors_admin' );  
    $imc_venue_upload = add_submenu_page( 'imc-impulse-creative', 'Venue Importer', 'Venue Importer', 'edit_users', 'imc-venue-importer', 'imc_venue_importer_admin' );  
    $imc_venue_upload = add_submenu_page( 'imc-impulse-creative', 'Musician Importer', 'Musician Importer', 'edit_users', 'imc-music-importer', 'imc_music_importer_admin' );  
    //$imc_venue_slider = add_submenu_page( 'imc-impulse-creative', 'Venue Slider', 'Venue Slider', 'edit_users', 'imc-venue-slider', 'imc_venue_slider_admin' );
    $imc_cuisine_edit = add_submenu_page( 'imc-impulse-creative', 'Cuisine Editor', 'Cuisine Editor', 'edit_users', 'imc-cuisine-editor-admin', 'imc_cuisine_editor_admin' );
    $imc_neighborhood_edit = add_submenu_page( 'imc-impulse-creative', 'Neighborhood Editor', 'Neighborhood Editor', 'edit_users', 'imc-neighborhood-editor-admin', 'imc_neighborhood_editor_admin' );
    $imc_genre_edit = add_submenu_page( 'imc-impulse-creative', 'Genre Editor', 'Genre Editor', 'edit_users', 'imc-genre-editor-admin', 'imc_genre_editor_admin' );
    $imc_flag_edit = add_submenu_page( 'imc-impulse-creative', 'Venue Flag Editor', 'Venue Flag Editor', 'edit_users', 'imc-venue-flag-editor-admin', 'imc_venue_flag_editor_admin' );
    $imc_approve_events = add_submenu_page( 'imc-impulse-creative', 'Approve Events', 'Approve Events', 'edit_users', 'imc-approve-events', 'imc_approve_events' );
    $imc_approve_bands = add_submenu_page( 'imc-impulse-creative', 'Approve Bands', 'Approve Bands', 'edit_users', 'imc-approve-bands', 'imc_approve_bands' );
    //$imc_approve_owners = add_submenu_page( 'imc-impulse-creative', 'Approve Events', 'Approve Events', 'edit_users', 'imc-approve-events', 'imc_approve_events' );
    $imc_happyhour_edit = add_submenu_page( 'imc-impulse-creative', 'Happy Hour Editor', 'Happy Hour Editor', 'edit_users', 'imc-happy-hour-editor-admin', 'imc_happy_hour_editor_admin' );
    $imc_verify_user = add_submenu_page( 'imc-impulse-creative', 'Verify Owner', 'Verify Owner', 'edit_users', 'imc-verify-user', 'imc_verify_user_admin' );
    $imc_cuisine_edit = add_submenu_page( 'imc-impulse-creative', 'Event Manager', 'Event Manager', 'edit_users', 'imc-event-manager-admin', 'imc_event_manager_admin' );
    
    $imc_band_edit = add_submenu_page( 'imc-impulse-creative', 'Band Editor', 'Band Editor', 'edit_users', 'imc-band-editor-admin', 'imc_band_editor_admin' );
    //$imc_test_edit = add_submenu_page( 'imc-impulse-creative', 'Testing', 'Testing', 'edit_users', 'imc-testing-admin', 'imc_testing_admin' );
    add_action( 'admin_init', 'remove_unless_super_admin' );
}

function imc_display_help() {
    include( 'imc-help.php' );

}
function remove_unless_super_admin() {
    $userid = get_current_user_id();
    if( $userid != 1 ) {
        remove_menu_page('upload.php');                 //Media
        remove_menu_page('edit.php?post_type=page');    //Pages
        remove_menu_page('edit-comments.php');          //Comments
        //remove_menu_page('themes.php');                 //Appearance
        
        remove_menu_page('widgets.php');
        remove_submenu_page('themes.php', 'themes.php');                 //Appearance
        //remove_submenu_page('themes.php', 'nav-menus.php');    
        remove_submenu_page('themes.php', 'widgets.php');
        remove_submenu_page('themes.php', 'customize.php?return=%2Fnapleslivenow%2Fwp-admin%2F');
        remove_submenu_page('themes.php', 'theme-editor.php');
        remove_menu_page('theme-editor.php');
        remove_menu_page('');

        
        remove_menu_page('plugins.php');                //Plugins
//        remove_menu_page('users.php');                  //Users
        remove_menu_page('tools.php');
        remove_menu_page('options-general.php'); // settings
        remove_menu_page('warp'); //master warp theme page
        //remove_menu_page('edit.php?post_type=tribe_events'); //events
        remove_menu_page('ninja-forms'); //forms
        remove_menu_page('index.php'); //dashboard
        remove_submenu_page('edit.php?post_type=tribe_events', 'edit.php?post_type=tribe_venue');
    }
}

add_action('admin_init', 'wpse_136058_debug_admin_menu');
function wpse_136058_debug_admin_menu() {

    //echo '<pre>' . print_r($GLOBALS['menu'], true) . '</pre>';
}

function imc_styling() {
    echo '<style>
        li#toplevel_page_imc-impulse-creative img { width: 23px; margin-top: -2px; } 
        li#toplevel_page_imc-impulse-creative .wp-menu-name:hover, li#toplevel_page_imc-impulse-creative .wp-submenu a:hover { color: #f6861f; } 
        </style>';
}

global $wpdb;
add_action('admin_menu', 'imc_admin_actions');
add_action('admin_head', 'imc_styling');

function imc_venue_editor_admin() {
    include( 'imc-venue-editor.php' );
    venue_editor_page();
}
function imc_venue_editors_admin() {
    include( 'imc-venues-editor.php' );
    display_all_venues();
    
}

function imc_venue_importer_admin() {
    include( 'imc-venue-importer.php' );
    display_import( 'Venue' );
}

function imc_music_importer_admin() {
    include( 'imc-venue-importer.php' );
    display_import( 'Musician' );
}

function imc_cuisine_editor_admin() {
    global $wpdb;
    include ( 'imc-record-editor.php' );
    imc_record_edit( 
        $wpdb->prefix . 'imc_venue_cuisine_list', 
        'cuisine', 
        'cuisines' 
    );
}
function imc_genre_editor_admin() {
    global $wpdb;
    include ( 'imc-record-editor.php' );
    imc_record_edit( 
        $wpdb->prefix . 'imc_music_genres', 
        'genre', 
        'genres' 
    );
}
function imc_neighborhood_editor_admin() {
    global $wpdb;
    include ( 'imc-record-editor.php' );
    imc_record_edit( 
        $wpdb->prefix . 'imc_venue_neighborhoods', 
        'neighborhood', 
        'neighborhoods' 
    );
}
function imc_venue_flag_editor_admin() {
    global $wpdb;
    include ( 'imc-record-editor.php' );
    imc_record_edit( 
        $wpdb->prefix . 'imc_venue_flag_list', 
        'flag', 
        'flags' 
    );
}

function imc_approve_events() {
    include( 'imc-approve-admin.php' );
    $events = display_unapproved( 'events' );
    echo '<div class="wrap approve-items">';
    echo $events;
    echo '</div>';
}

function imc_approve_events_ajax() {
    global $wpdb;
    $approve = 'id IN ( ' . sanitize_text_field( $_POST[ 'approve' ] ) . ' )';
    if(isset($_POST['delete']) && $_POST['delete'] == '1') {
        $sql = 'DELETE FROM '.$wpdb->prefix . 'imc_events where id = ' . $_POST[ 'approve' ];
    } else {
        $sql = 'UPDATE ' . $wpdb->prefix . 'imc_events SET approved = 1 WHERE ' . $approve ;
    }
    $success = $wpdb->query( $sql );
    echo $success;   
    die();
}

function imc_approve_bands() {
    include( 'imc-approve-admin.php' );
    $events = display_unapproved( 'bands' );
    echo '<div class="wrap approve-items">';
    echo $events;
    echo '</div>';
}

function imc_approve_bands_ajax() {
    global $wpdb;
    if(isset($_POST['delete']) && $_POST['delete'] == '1') {
        $sql = 'DELETE FROM '.$wpdb->prefix . 'imc_bands where id = ' . $_POST[ 'approve' ];
    } else {
        $approve = 'id IN ( ' . sanitize_text_field( $_POST[ 'approve' ] ) . ' )';
        $sql = 'UPDATE ' . $wpdb->prefix . 'imc_bands SET approved = 1 WHERE ' . $approve ;
    }
    
    $success = $wpdb->query( $sql );
    echo $success;   
    die();
}

function imc_band_editor_admin() {
    include( 'imc-band-editor.php' );
    display_bands_admin();
}

function imc_happy_hour_editor_admin() {
    include( 'imc-happy-hour-editor.php' );
    include( 'imc-autocomplete.php' );
    imc_get_venue_names();
}

function imc_event_manager_admin() {
    include( 'imc-event-manager.php' );
    imc_get_event_manager();
}

function imc_verify_user_admin() {
    include( 'imc-verify-user.php' );
    imc_verify_user();
}
function imc_testing_admin() {
    include( 'imc-testing.php' );
    imc_run_test();
}

function imc_venue_slider_admin() {
    include( 'imc-venue-slider.php' );
    imc_display_slider_admin();
}

function imc_modify_contact_methods( $profile_fields ) {
    $profile_fields[ 'venue_id' ] = 'Listing ID';
    $profile_fields[ 'name' ] = 'Listing Name';
    $profile_fields[ 'address' ] = 'Address';
    $profile_fields[ 'address2' ] = 'Address 2';
    $profile_fields[ 'contact_name' ] = 'Venue Contact Name';
    $profile_fields[ 'contact_phone' ] = 'Venue Contact Phone Number';
    $profile_fields[ 'website' ] = 'Website Url';
    $profile_fields[ 'cuisine1' ] = 'Cuisine Type 1';
	$profile_fields[ 'cuisine2' ] = 'Cuisine Type 2';
	$profile_fields[ 'cuisine3' ] = 'Cuisine Type 3';
    $profile_fields[ 'neighborhood' ] = 'Neighborhood';
    $profile_fields[ 'price_range' ] = 'Price Range';
    $profile_fields[ 'logo' ] = 'Logo Url';
    $profile_fields[ 'waterfront' ] = 'Waterfront Type';
    $profile_fields[ 'outdoor' ] = 'Outdoor Seating';
    $profile_fields[ 'live_entertainment' ] = 'Live Entertainment';
    $profile_fields[ 'meal_types' ] = 'Meal Types';
    $profile_fields[ 'venue_flags' ] = 'Venue Flags';
    $profile_fields[ 'is_active' ] = 'In Business';
    $profile_fields[ 'subscription_level' ] = 'Membership';
    $profile_fields[ 'facebook' ] = 'Facebook';
    $profile_fields[ 'twitter' ] = 'Twitter Url';
    $profile_fields[ 'yelp' ] = 'Yelp Url';
    $profile_fields[ 'googleplus' ] = 'Google Plus Url';
    $profile_fields[ 'foursquare' ] = 'Foursquare Url';
    $profile_fields[ 'urbanspoon' ] = 'Urbanspoon Url';
    $profile_fields[ 'instagram' ] = 'Instagram Url';
    $profile_fields[ 'details' ] = 'Additional Information';
    $profile_fields[ 'menu' ] = 'Menu Url';
    $profile_fields[ 'beer_on_taps' ] = 'How Many Beers on Tap';
    $profile_fields[ 'craft_beers' ] = 'Would you like to mention any Craft Beer Names';
    $profile_fields[ 'images' ] = 'Images';
    
    unset( $profile_fields[ 'aim' ] );
    unset( $profile_fields[ 'url' ] );
    return $profile_fields;
    
}

add_filter( 'user_contactmethods', 'imc_modify_contact_methods' );

add_action( 'register_form', 'imc_modify_register_form' );

function imc_modify_register_form() {
    $venue_id = ( isset( $_GET[ 'id' ] ) ) ? $_GET[ 'id' ]: '';
    ?>
    <p class="hidden">
        <input type="text" id="venue_id" name="venue_id" value="<?php echo esc_attr(stripslashes( $venue_id ) ); ?>" />
    </p>
    <?php
}

function imc_update_meta( $id, $key, $value ) {
    $meta = get_user_meta( $id, $key, true );
    if( $meta == null ) {
        add_user_meta( $id, $key, $value );
    } else {
        update_user_meta( $id, $key, $value );
    }
}

function imc_update_venue_from_user_admin( $user_id ) {
    if ( current_user_can( 'edit_user', $user_id ) ) {
        $verified = get_user_meta( $user_id, 'verified', true );
        if( $verified == 1 ) {
            imc_update_venue( $user_id );
            imc_update_meta( $user_id, 'updated_listing', date( 'm-d-Y' ) );
        } else {
            imc_update_meta( $user_id, 'verified', 0 );
            $user = new WP_User( $user_id );
            $user_meta = get_user_meta( $user_id );
            $message = 'User: ' . $user->first_name . ' ' . $user->last_name . ' [' .$user->user_email . ']' . "\r\n";
            $message .= "\r\n";
            $message .= 'Please review the following information.' . "\r\n";
            foreach( $user_meta as $key => $value ) {
                $message .= $key . ': ' . $value[0] . "\r\n";
            }
            $message .= get_site_url() . '/wp-admin/admin.php?page=imc-verify-user';
            $mailSent = wp_mail( get_option( 'admin_email' ), sprintf(__('[%s] An owner has clamied a venue and has edited it'), get_option('blogname')), $message);
            imc_update_meta( $user_id, 'updated_listing', 'Awaiting Verification' );
        }
    }
}

add_action( 'profile_update', 'imc_update_venue_from_user_admin', 12 );

function imc_update_venue( $user_id ) {
    $user_meta = get_user_meta( $user_id );
    $venue_id = $user_meta[ 'venue_id' ][0];
    $venue = array( 
        'name' => $user_meta[ 'name' ][0],
        'address' => $user_meta[ 'address' ][0],
        'address2' => $user_meta[ 'address2' ][0],
        'city' => $user_meta[ 'city' ][0],
        'state' => $user_meta[ 'state' ][0],
        'zip' => $user_meta[ 'zip' ][0],
        'phone' => $user_meta[ 'phone' ][0],
        'website' => $user_meta[ 'website' ][0],
        'logo' => $user_meta[ 'logo' ][0]
    );
    $paid = array(
        'venue_id' => $venue_id,
        'website' => $user_meta[ 'website' ][0],
        'twitter' => $user_meta[ 'twitter' ][0],
        'yelp' => $user_meta[ 'yelp' ][0],
        'googleplus' => $user_meta[ 'googleplus' ][0],
        'foursquare' => $user_meta[ 'foursquare' ][0],
        'facebook' => $user_meta[ 'facebook' ][0],
        'urbanspoon' => $user_meta[ 'urbanspoon' ][0],
        'instagram' => $user_meta[ 'instagram' ][0],
        'details' => $user_meta[ 'details' ][0],
        'menu' => $user_meta[ 'menu' ][0],
        'beer_on_taps' => $user_meta[ 'beer_on_taps' ][0],
        'craft_beers' => $user_meta[ 'craft_beers' ][0]
    );
    global $wpdb;
    $venue_update = $wpdb->update(
        $wpdb->prefix . 'imc_venue',
        $venue,
        array( 'ID' => $venue_id )
    );
    $paid_table = $wpdb->prefix . 'imc_venue_paid';
    $check_query = "SELECT id FROM $paid_table WHERE venue_id = $venue_id";
    $paid_id = $wpdb->get_var( $check_query );
    if( $paid_id != null ) {
        $paid_update = $wpdb->update(
        $paid_table,
            $paid,
            array( 'id' => $paid_id )
        );
    } else {
        $paid_new = $wpdb->insert(
            $paid_table,
            $paid
        );
    }
}
add_filter( 'ajax_query_attachments_args', 'show_users_own_attachments', 1, 1 );
function show_users_own_attachments( $query ) {
	$id = get_current_user_id();
	if( !current_user_can('manage_options') )
		$query['author'] = $id;
	return $query;
}
add_action('show_user_profile', 'imc_profile_script');
add_action('edit_user_profile', 'imc_profile_script');
add_action('show_user_profile', 'imc_add_profile_pickers');
add_action('edit_user_profile', 'imc_add_profile_pickers');


function imc_add_profile_pickers() {
	include( 'imc-profile-pickers.php' );
        displayAll();
}
function imc_profile_script() {
    wp_enqueue_script( 'imc-profile', plugin_dir_url(__FILE__) . 'js/imc-profile.js', array( 'jquery' ) );
	wp_enqueue_style( 'imc-profile', plugin_dir_url(__FILE__) . 'css/imc-profile.css');
}

function imc_verify_user_by_id() {
    $user_id = $_POST[ 'user_id' ];
    imc_update_meta( $user_id, 'verified', 1 );
    $update = imc_update_venue( $user_id );
    imc_update_meta( $user_id, 'updated_listing', date( 'm-d-Y' ) );
    imc_update_venue( $user_id );
    die();
    
}

function imc_add_register_to_login() {
    if( isset( $_GET[ 'id' ] ) ) {
        $id = $_GET[ 'id' ];
        echo '<p class="custom-register"><a href="' . wp_login_url() . '?action=register&e=claim&id=' . $id . '">Don\'t Have an account?  Register Here!</a></p>';
    }
    
}
add_filter( 'login_message', 'imc_add_register_to_login' );

function imc_user_register( $user_id ) {
        $venue_id = $_POST[ 'venue_id' ];
        add_user_meta( $user_id, 'venue_id', $venue_id );
        global $wpdb;
        $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue WHERE id = ' . $venue_id;
        $venue = $wpdb->get_row( $sql );
        foreach( $venue as $key => $value ) {
            add_user_meta( $user_id, $key, $value );
        }
        add_user_meta( $user_id, 'verified', 0 );
}
   

add_filter( 'wp_login_errors', 'wpse_161709', 10, 2 );

function wpse_161709( $errors, $redirect_to ) {
    if( isset( $errors->errors['registered'] ) ) {
        // Use the magic __get method to retrieve the errors array:
        $tmp = $errors->errors;   

        // What text to modify:
        $old = 'Registration complete. Please check your e-mail.';
        $new = 'Registration complete. Please check your e-mail. Your password will be inside your email. Please check your spam box if you don\'t see the message in your inbox.';

        // Loop through the errors messages and modify the corresponding message:
        foreach( $tmp['registered'] as $index => $msg )
        {
          if( $msg === $old )
              $tmp['registered'][$index] = $new;        
        }
        // Use the magic __set method to override the errors property:
        $errors->errors = $tmp;

        // Cleanup:
        unset( $tmp );
    }  
    return $errors;
}



add_action( 'user_register', 'imc_user_register' );

if( !function_exists( 'wp_new_user_notification' ) ) {
    function wp_new_user_notification( $user_id, $plaintext_pass = '' ) {
        $user = new WP_User( $user_id );
        $user_login = stripslashes( $user->user_login );
        $user_email = stripslashes( $user->user_email );
        $url = wp_login_url();
        $message  = sprintf(__('New user registration on %s:'), get_option('blogname')) . "\r\n\r\n";
        $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
        $message .= sprintf(__('E-mail: %s'), $user_email) . "\r\n";
        $message .= sprintf(__('Password: %s'), $plaintext_pass) . "\r\n";
        $message .= sprintf(__('Login: %s'), $url) . "\r\n";
        $user_mail = wp_mail( $user_email, sprintf(__('[%s] New User Registration'), get_option('blogname')), $message);

    }
}

add_action( 'edit_user_profile_update', 'notify_admin_profile_changes' );

function notify_admin_profile_changes( $user_id ) {
        global $wpdb;
        
        
    /*
        $user = new WP_User( $user_id );
        $user_meta = get_user_meta( $user_id );
        $message = 'User: ' . $user->first_name . ' ' . $user->last_name . ' [' .$user->user_email . ']' . "\r\n";
        foreach( $user_meta as $key => $value ) {
            $message .= $key . ': ' . $value . "\r\n";
        }
        $user_mail = wp_mail( get_option( 'admin_email' ), sprintf(__('[%s] New User Registration'), get_option('blogname')), $message);
*/
}


function imc_save_venue() {
    check_admin_referer( 'imc-venue-nonce' );
    $saved_venue = imc_add_venue_to_database();
    global $wpdb;
    $sql = 'SELECT wp_post_id from '.$wpdb->prefix.'imc_venue where id = '.$saved_venue;
    $post_id = $wpdb->get_var($sql);
    if($post_id != 0) {
        imc_add_venue_meta_data( $post_id, $saved_venue );
        imc_add_premium_fields( $post_id, $saved_venue );
        ?><script>alert("<?php echo stripslashes( $_POST['name'] ); ?> was saved successfully!"); window.location.href = 'admin.php?page=imc-venues-editor';</script><?php
    } else {
        if ( $saved_venue > 0 ) {
            $name = $_POST[ 'name' ];
            $postid = imc_add_venue_page( $name, $saved_venue );
            imc_add_venue_meta_data( $postid, $saved_venue );
            imc_add_wp_url_to_venue( $postid );
            imc_add_premium_fields( $postid, $saved_venue );
            ?><script>alert("<?php echo stripslashes( $name ); ?> was saved successfully!"); window.location.href = 'admin.php?page=imc-venues-editor';</script><?php
        }
    }
    die();
}

function imc_add_premium_fields($postid, $venue_id) {
    global $wpdb;
    $photos = trim($_POST['adimage1_']).','.trim($_POST['adimage2_']).','.trim($_POST['adimage3_']).','.trim($_POST['adimage4_']).','.trim($_POST['adimage5_']);
    $venue_id = sanitize_text_field($_POST['saveid']);
    $website = sanitize_text_field($_POST['website']);
    $twitter = sanitize_text_field($_POST['twitter']);
    $yelp = sanitize_text_field($_POST['yelp']);
    $googleplus =sanitize_text_field($_POST['googleplus']);
    $foursquare = sanitize_text_field($_POST['foursquare']);
    $facebook = sanitize_text_field($_POST['facebook']);
    $urbanspoon = sanitize_text_field($_POST['urbanspoon']);
    $instagram = sanitize_text_field($_POST['instagram']);
    $menu = sanitize_text_field($_POST['menu']);
    $beersOnTap = sanitize_text_field($_POST['beersOnTap']);
    $craftBeers = sanitize_text_field($_POST['craftBeers']);
    $openDetails = sanitize_text_field($_POST['details']);
    $check_sql = 'SELECT * from ' . $wpdb->prefix . 'imc_venue_paid where venue_id = ' . $venue_id . ' LIMIT 1';
    $checked = $wpdb->get_row($check_sql);
    if ($checked == null) {
        $sql = "INSERT INTO {$wpdb->prefix}imc_venue_paid "
            . "(venue_id, website,photos,twitter,yelp,googleplus,foursquare,facebook,urbanspoon,instagram,menu,beer_on_taps,craft_beers,details)"
            . " VALUES "
            . "(%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)";
        $sql = $wpdb->prepare($sql,$venue_id, $website,$photos,$twitter,$yelp,$googleplus,$foursquare,$facebook,$urbanspoon,$instagram,$menu,$beersOnTap,$craftBeers,$openDetails);
    } else {
        $sql = "UPDATE {$wpdb->prefix}imc_venue_paid SET "
            . "venue_id = '%s', website = '%s',photos = '%s',twitter = '%s',yelp = '%s',googleplus = '%s',foursquare = '%s',facebook = '%s',urbanspoon = '%s',instagram = '%s',menu = '%s', beer_on_taps = '%s', craft_beers = '%s', details = '%s'"
            . " WHERE venue_id = " . $venue_id;
        $sql = $wpdb->prepare($sql,$venue_id, $website,$photos,$twitter,$yelp,$googleplus,$foursquare,$facebook,$urbanspoon,$instagram,$menu,$beersOnTap,$craftBeers,$openDetails,$venue_id);
    }
    $saved = $wpdb->query($sql);
}

function imc_add_wp_url_to_venue( $postid ) {
    global $wpdb;
    $url = get_permalink( $postid );
    $updated = $wpdb->update( $wpdb->prefix . 'imc_venue', array( 'wp_guid' => $url ), array( 'wp_post_id' => $postid ) );
}

function imc_add_venue_link_to_events() {
    $args = imc_get_venue_args();
    $postid = tribe_create_venue( $args );
    return $postid;
}

function imc_get_venue_args() {
    $args = array(
        'post_title'        => $venue[ 'name' ],
        'post_type'         => 'tribe_venue',
        'post_status'       => 'publish',
        'Venue'             => $venue[ 'name' ],
        'Address'           => $venue[ 'address' ],
        'City'              => $venue[ 'city' ],
        'Country'           => 'United States',
        'State'             => $venue[ 'state' ],
        'StateProvince'     => $venue[ 'state' ],
        'Zip'               => $venue[ 'Zip' ],
        'Phone'             => $venue[ 'phone' ],
        'Orgin'             => 'imc-plugin-admin',
        'ShowMap'           => false,
        'ShowMapLink'       => false
    );
    return $args;
}

function imc_check_wp_posts_for_duplicates( $name ) {
    global $wpdb;
    $sql = 'SELECT ID FROM ' . $wpdb->prefix . 'posts WHERE post_title LIKE "%' . $name . '%" AND post_type = "tribe_venue"';
    $result = $wpdb->get_var( $sql );
    if( $result == null ) {
        $result = false;
    } 
    return $result;
}



function imc_add_venue_to_database( $import = false, $import_format = false, $admin_update = false ) {
    global $wpdb;
    if( $import == false ) {
        //$wp_post_id = imc_check_wp_posts_for_duplicates( sanitize_text_field( $_POST[ 'name' ] ) );
        $venue = imc_get_clean_venue_post();
        $format = imc_get_clean_venue_format();

        if ( $_POST['save_type'] === 'new_venue' ) {
            $saved_venue = $wpdb->insert( 
                $wpdb->prefix . 'imc_venue', $venue, $format
            );
            $id = $wpdb->insert_id;
        } else {
            $saved_venue = $wpdb->update( 
                $wpdb->prefix . 'imc_venue', $venue, array( 'id' => $_POST[ 'saveid' ]), $format, array( '%d' )
            );
            $id = $_POST[ 'saveid' ];
        }
    } else {
        $imported_venue = $wpdb->insert( $wpdb->prefix . 'imc_venue', $import, $import_format );
        $id = $wpdb->insert_id;
    }
    return $id;
}

function imc_add_venue_page( $name, $venue_id, $content = ' ', $type = 'page', $status = 'publish', $author = 1, $category = array( 0 ), $parent = 10 ) {
    $name_replaced = str_replace( ' ', '-', $name );
    $name_replaced = str_replace( "'", "", $name_replaced );
    $url = '/' . $name_replaced;
    $new_venue = array();
    $new_venue[ 'post_title' ] = $name;
    $new_venue[ 'post_name' ] = $name_replaced; 
    $new_venue[ 'post_content' ] = $content;
    $new_venue[ 'post_type' ] = $type;
    $new_venue[ 'post_status' ] = $status;
    $new_venue[ 'author' ] = $author;
    $new_venue[ 'category' ] = $category;
    $new_venue[ 'post_parent' ] = $parent;
    $new_venue[ 'guid' ] = $url;
    $new_venue[ 'comment_status' ] = 'closed';
    $new_venue[ 'ping_status' ] = 'closed';
    $new_post_id = wp_insert_post( $new_venue );
    
    imc_add_postid_to_venue( $venue_id, $new_post_id );
    add_guid_to_imc_venue( $venue_id, $url );
    return $new_post_id;
}

function add_guid_to_imc_venue( $venue, $url ) {
    global $wpdb;
    $success = $wpdb->update(
            $wpdb->prefix . 'imc_venue', array( 'wp_guid' => $url ), array( 'id' => $venue )
        ); 
    return $success;
}
function imc_add_postid_to_venue( $venue_id, $wp_id ) {
    global $wpdb;
    $updated_venue = $wpdb->update(
            $wpdb->prefix . 'imc_venue', 
            array( 'wp_post_id' => $wp_id ), 
            array( 'id' => $venue_id ),
            array( '%d' )
    );
}

function imc_add_venue_meta_data( $id, $venue_id ) {
    add_post_meta( $id, 'venue', 1 );
    add_post_meta( $id, 'venue_id', $venue_id );
    add_post_meta( $id, '_edit_lock', '1406318205:1' );
}

function imc_get_venue_page_id( $venue_id ) {
    global $wpdb;
    $sql = 'SELECT post_id FROM ' . $wpdb->prefix . 'postmeta WHERE meta_key = "venue_id" and meta_value = ' . $venue_id;
    $result = $wpdb->get_row( $sql );
    
    if( $result == '' || $result == null ) {
        $ret_val = 54;
    } else {
        $ret_val = $result->post_id;
    }
    return $ret_val;
}

function imc_get_venue_guid( $pageid ) {
    global $wpdb;
    $sql = 'SELECT guid FROM ' . $wpdb->prefix . 'posts WHERE ID = ' . $pageid;
    $result = $wpdb->get_row( $sql );
    if($result) {
        if($result->guid != null) { 
            return ltrim( $result->guid, '/' );
        }
    }
}

function imc_get_meta() {
    $user_id = get_current_user_id();
    $timestamp = date("Y-m-d H:i:s");
    $user_meta = array();
    $user_meta[ 'id' ] = $user_id; 
    $user_meta[ 'timestamp' ] = $timestamp; 
    return $user_meta;
}

function imc_get_venue_admin( $id ) {
    global $wpdb;
    $venue = $wpdb->get_results( 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue WHERE id = ' .$id );
    return $venue;
}

function imc_get_venues_admin() {
    global $wpdb;
    $venues = $wpdb->get_results( 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue ORDER BY name' );
    return $venues;
}

function imc_get_clean_venue_post() {
    $meta = imc_get_meta();
    $POST = array_map( 'stripslashes_deep', $_POST );
    $venue = array(
        'name'          => $POST[ 'name' ],
        'address'       => $POST[ 'address' ],
        'address2'      => $POST[ 'address2' ],
        'city'          => $POST[ 'city' ],
        'state'         => $POST[ 'state' ],
        'zip'           => $POST[ 'zip' ],
        'phone'         => $POST[ 'phone' ],
        'cuisine1'      => $POST[ 'cuisine1' ],
        'cuisine2'      => $POST[ 'cuisine2' ],
        'cuisine3'      => $POST[ 'cuisine3' ],
        'neighborhood'  => $POST[ 'neighborhood' ],
        'price_range'   => $POST[ 'price_range' ],
        'logo'          => $POST[ 'logo_image' ],
        'waterfront'    => $POST[ 'waterfront_hidden' ],
        'outdoor'       => $POST[ 'outdoor_hidden' ],
        'is_claimed'    => $POST[ 'is_claimed_hidden' ],
        'meal_types'    => $POST[ 'meal_types_hidden' ],
        'venue_flags'   => $POST[ 'venue_flags_hidden' ],
        'created_by'    => $meta[ 'id' ],
        'created_date'  => $meta[ 'timestamp' ],
        'website'       => $POST[ 'website' ],
        'updated_by'    => $meta[ 'id' ],
        'updated_date'  => $meta[ 'timestamp' ],
        'paid_level'    => 3,
        'is_paid'       => 1,
        'is_active'     => $POST[ 'is_active_hidden']
    );
    return $venue;
    
}
function imc_get_clean_venue_format() {
    $format_array = array('%s', '%s', '%s', '%s', '%s', '%d', '%s', '%d', '%d', '%d', '%d',
        '%d', '%s', '%s', '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%d');
    return $format_array;
}

function imc_insert_records() {
    check_admin_referer( 'imc-record-nonce' );
    global $wpdb;
    $table = $_POST[ 'table' ];
    $records = $_POST[ 'records' ];
    if ( strpos ( $records, ',' ) !== FALSE ) {
        imc_add_multiple_records( $records, $table );
    } else {
        imc_add_single_record( $records, $table );
    }
    $results = '';
    $sql = 'SELECT * FROM ' . $table;
    $results_array = $wpdb->get_results($sql);
    foreach($results_array as $result) {
        $results .= '<li id="'.$result->id.'">'.$result->name.'</li>';
    } 
    echo $results;
    die();
    
}

function imc_add_multiple_records ( $multiple_records, $table ) {
    global $wpdb;
    $fail = 0;
    $record_array = explode ( ',', $multiple_records );
    for ( $record_count = 0; $record_count < count ( $record_array ); $record_count++ ) {
        $record[ $record_count ] = ucwords ( trim ( $record_array [ $record_count ] ) );
        
        $new_record = $wpdb->insert ( $table,
                array ( 'name' => stripslashes_deep( $record[ $record_count ] ) ),
                array ( '%s' )
                );
        if ( 1 == $new_record ) {
            echo '<span class="feedback"> ' . $record [ $record_count ] . ' was Inserted Successfully. </span>';
            ?><script>document.getElementById('records').value = '';</script><?php
        } else {
            echo '<span class="error">There was an error, please contact support</span>';
            $fail = 1;
            die( 'There was an error, please contact support' );
        }
    }
    if ( 1 !== $fail ) {

    }   
}
function imc_add_single_record( $single_record, $table ) {
    global $wpdb;
    $record = ucwords ( trim ( $single_record ) );
    $new_record = $wpdb->insert ( $table, array ( 'name' => $record ), array ( '%s' ) );
    if ( 1 == $new_record ) {
        echo '<span class="feedback"> ' . $record  . ' was Inserted Successfully. </span>';
        ?><script>document.getElementById('records').value = '';</script><?php
    } else {
        echo '<span class="error">There was an error, please contact support</span>';
        die ( 'There was an error, please contact support' );
    }
}

function imc_load_scripts() {
    wp_enqueue_script( 'imc-ajax', plugin_dir_url(__FILE__) . 'js/imc-ajax.js', array( 'jquery' ) );
    wp_enqueue_media();
    wp_register_script( 'imc-image-upload', plugin_dir_url( __FILE__ ) . 'js/imc-image-upload.js', array ( 'jquery' ) );
    wp_enqueue_script( 'imc-image-upload' );
    wp_register_script( 'imc-venue-verify', plugin_dir_url( __FILE__ ) . 'js/imc-venue-verify.js', array ( 'jquery' ) );
    wp_enqueue_script( 'imc-venue-verify' );
    wp_enqueue_script( 'imc-autocomplete', plugin_dir_url(__FILE__) . 'js/imc-autocomplete.js', array( 'jquery', 'jquery-form', 'json2' ), false, true );
    wp_enqueue_script( 'imc-happy-hour', plugin_dir_url(__FILE__) . 'js/imc-happy-hour.js', array( 'jquery' ) );
    wp_enqueue_script( 'imc-event-manager', plugin_dir_url(__FILE__) . 'js/imc-event-manager.js', array( 'jquery' ) );
    wp_enqueue_script( 'imc-create-event', plugin_dir_url(__FILE__) . 'js/imc-create-event.js', array( 'jquery' ) );
	wp_register_script( 'imc-upload-filter', plugin_dir_url( __FILE__ ) . 'js/imc-upload-filter.js', array ( 'jquery' ) );
	wp_enqueue_script( 'imc-upload-filter' );
	if( function_exists( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}
}

function imc_load_styles() {
    wp_enqueue_style( 
        'imc-autocomplete-style', 
        plugin_dir_url( __FILE__ ) . 'css/imc-autocomplete-style.css' 
    );    
}

function imc_get_venues_theme() {
    include( 'imc-get-venues.php' );
    imc_display_venues();
}

function imc_get_venue_details( $id ) {
    if( isset( $_POST[ 'id' ] ) ) {
        $id = $_POST[ 'id' ];
    } 
    global $wpdb;
    $sql = 'SELECT wp_post_id, name, address, address2, city, state, phone, website FROM ' . $wpdb->prefix . 'imc_venue WHERE id = ' . sanitize_text_field( $id );
    $result = $wpdb->get_row( $sql );
    $venue_address = get_event_address( $result );
    return $venue_address;    
}

function get_event_address( $venue ) {
    $ret_val =  '<label><span class="input-spacer">Address:</span><input id="address" name="venue-address" type="text" class="wider" value="' . $venue->address . ' ' . $venue->address2 . '" />' .
                '</label><label><span class="input-spacer">City: </span><input id="city" name="venue-city" type="text" class="wider" value="' . $venue->city . '" />' .
                '</label><label><span class="input-spacer">State:</span><input id="state" name="venue-state" type="text" class="wider"  value="' . $venue->state . '" />' .
                '</label><label><span class="input-spacer">Phone:</span><input id="phone" name="venue-phone" type="text" class="wider" value="' . $venue->phone . '" />' .
                '</label><label><span class="input-spacer">Website:</span><input id="website" name="venue-website" type="text" class="wider" value="' . $venue->website . '" /></label>' .
                '<input type="hidden" name="venue-id" id="venue-id" value="' . $venue->wp_post_id . '" />';
    echo $ret_val;
    die();
}
function imc_get_venue_theme( $venue_id ) {
    include( 'imc-get-venue.php' );
    imc_display_venue( $venue_id );
}

function imc_get_venues_pagination( $limit, $record, $venue_sql, $type ) {
    include( 'imc-venue-pagination.php' );
    imc_display_pagination( $limit, $record, $venue_sql, $type );
}
function imc_get_musician_pagination( $limit, $record, $venue_sql, $type ) {
    include( 'imc-musician-pagination.php' );
    imc_display_pagination( $limit, $record, $venue_sql, $type );
}

function imc_get_happy_hour_theme() {
    include( 'imc-happy-hours.php' );
    imc_display_happy_hour_page();
}


function imc_add_icon_styles() {
    
}

function imc_get_venue_options () {
    include ( 'imc-get-venue-options.php' );
    imc_display_sidebar();
}

function imc_filter_venues() {
    imc_get_venues_theme();
}

function imc_filter_bands() {
    imc_get_bands_theme();
}

add_action( 'admin_enqueue_scripts', 'imc_load_scripts' );
add_action( 'admin_menu', 'imc_load_styles' );
add_action( 'wp_ajax_imc_insert_records', 'imc_insert_records' );
add_action( 'wp_ajax_imc_save_venue', 'imc_save_venue' );
add_action( 'wp_ajax_imc_venue_autocomplete', 'imc_venue_autocomplete' );
add_action( 'wp_ajax_nopriv_imc_venue_autocomplete', 'imc_venue_autocomplete' );
add_action( 'wp_ajax_imc_happy_hour_editor', 'imc_happy_hour_editor' );
add_action( 'wp_ajax_imc_delete_venue', 'imc_delete_venue' );
add_action( 'wp_ajax_imc_update_happy_hour', 'imc_update_happy_hour' );
add_action( 'wp_ajax_imc_delete_happy_hour', 'imc_delete_happy_hour' );
add_action( 'wp_ajax_imc_display_hh_data', 'imc_display_hh_data' );
add_action( 'wp_ajax_nopriv_imc_display_hh_data', 'imc_display_hh_data' );
add_action( 'wp_ajax_imc_get_venue_details', 'imc_get_venue_details' );
add_action( 'wp_ajax_nopriv_imc_get_venue_details', 'imc_get_venue_details' );
add_action( 'wp_ajax_imc_create_event', 'imc_create_event' );
add_action( 'wp_ajax_nopriv_imc_create_event', 'imc_create_event' );
add_action( 'wp_ajax_imc_slider_editor', 'imc_slider_editor' );
add_action( 'wp_ajax_nopriv_imc_slider_editor', 'imc_slider_editor' );
add_action( 'wp_ajax_imc_add_slider', 'imc_add_slider' );
add_action( 'wp_ajax_nopriv_imc_add_slider', 'imc_add_slider' );
add_action( 'wp_ajax_imc_add_band', 'imc_add_band' );
add_action( 'wp_ajax_nopriv_imc_add_band', 'imc_add_band' );
add_action( 'wp_ajax_imc_save_band', 'imc_save_band' );
add_action( 'wp_ajax_nopriv_imc_save_band', 'imc_save_band' );
add_action( 'wp_ajax_imc_show_bands', 'imc_show_bands' );
add_action( 'wp_ajax_nopriv_imc_show_bands', 'imc_show_bands' );
add_action( 'wp_ajax_imc_process_textarea', 'imc_process_textarea' );
add_action( 'wp_ajax_imc_verify_user_by_id', 'imc_verify_user_by_id' );
add_action( 'wp_ajax_nopriv_imc_verify_user_by_id', 'imc_verify_user_by_id' );
add_action( 'wp_ajax_imc_venue_search', 'imc_verify_venue_search' );
add_action( 'wp_ajax_imc_edit_band', 'imc_edit_band' );
add_action( 'wp_ajax_nopriv_imc_edit_band', 'imc_edit_band' );
add_action( 'wp_ajax_nopriv_imc_venue_search', 'imc_venue_search' );
add_action( 'wp_ajax_imc_approve_events_ajax', 'imc_approve_events_ajax' );
add_action( 'wp_ajax_nopriv_imc_approve_events_ajax', 'imc_approve_events_ajax' );
add_action( 'wp_ajax_imc_approve_bands_ajax', 'imc_approve_bands_ajax' );
add_action( 'wp_ajax_nopriv_imc_approve_bands_ajax', 'imc_approve_bands_ajax' );
add_action( 'wp_ajax_imc_delete_event', 'imc_delete_event' );
add_action( 'wp_ajax_nopriv_imc_delete_event', 'imc_delete_event' );


function imc_delete_event() {
    $eventid = (int)trim($_POST['eventid']);
    $postid = trim($_POST['postid']);
    global $wpdb;
    $deleted = $wpdb->query($wpdb->prepare("DELETE FROM ".$wpdb->prefix."imc_events WHERE id = %d", $eventid));
    wp_delete_post($postid);
    echo $eventid;
    die();
}

function imc_get_venue_name( $id ) {
    global $wpdb;
    $venue_name_sql = 'SELECT name FROM ' . $wpdb->prefix . 'imc_venue WHERE id = ' . $id;
    $venue_name = $wpdb->get_var( $venue_name_sql, 0, 0 );
    return $venue_name;
    
}
function imc_venue_autocomplete() {
    $venue_temp = $_POST[ 'term' ];
    global $wpdb;
    $venue_sql = 'SELECT id, name FROM ' . $wpdb->prefix . 'imc_venue WHERE is_active = "1" and name LIKE "%' . $venue_temp . '%" ORDER BY name';
    $venues = $wpdb->get_results( $venue_sql );
    $string = '';
    foreach ($venues as $venue) {
        $string .= '<li data-name="' . $venue->name . '" class="venue-name" id="' . $venue->id . '"><span>' . $venue->name . '</span></li>';
    }
    echo $string;
    die();
 }
 
function imc_happy_hour_editor() {
    $id = $_POST[ 'venue_id' ];
    $name = imc_get_venue_name( $id );
    include( 'imc-happy-hour-editor.php' );
    imc_display_happy_hours( $id, $name );
    imc_add_happy_hours( $id, $name );
}

function imc_slider_editor() {
    $id = $_POST[ 'venue_id' ];
    $venue = imc_get_venue_details_event( $id );
    include( 'imc-venue-slider.php' );
    $html = imc_edit_venue_slider( $venue );
    return $html; 
}

function imc_get_price_per_oz( $price, $volume ) {
    if( $price == 0 || $volume == 0 ) {
        return 0;
    }
    $value = intval( $price ) / intval( $volume );
    return $value;
}


function imc_clean_happy_hour_post( $POST ) {
    $venue_id =                 intval( $POST[ 'venue_id' ] );
    $days_clean =               trim( $POST[ 'day' ] );
    $days_array =               explode( ',', $days_clean );
    $happy_hour_input_total =   count( $days_array );
    $time_start =               $_POST[ 'time_start' ];
    $time_end =                 $_POST[ 'time_end' ];
    $type =                     intval( $POST[ 'type' ] );
    $price =                    preg_replace( '/[^0-9\.]/', '', $POST[ 'price' ] );
    $reg_price =                preg_replace( '/[^0-9\.]/', '', $POST[ 'reg_price' ] );
    $ounces =                   preg_replace( '/[^0-9\.]/', '', $POST[ 'ounces'] );
    $summary =                  sanitize_text_field( $POST[ 'summary' ] );
    $updated_by =               intval( $POST[ 'updated_by' ] );
    $price_per_oz = imc_get_price_per_oz( $price, $ounces );
    $happy_hour = array(
        'venue_id'                  => $venue_id,
        'days_array'                => $days_array,
        'happy_hour_input_total'    => $happy_hour_input_total,
        'time_start'                => $time_start,
        'time_end'                  => $time_end,
        'type'                      => $type,
        'price'                     => $price,
        'reg_price'                 => $reg_price,
        'ounces'                    => $ounces,
        'price_per_oz'              => $price_per_oz,
        'summary'                   => $summary,
        'updated_by'                => $updated_by
    );
    return $happy_hour;
}

function imc_delete_happy_hour() { 
    echo 'here bitch';
    global $wpdb;
    $delete_row = $wpdb->delete(
        $wpdb->prefix . 'imc_happy_hour',
        array(
            'id' => $_POST[ 'happy_hour' ]
        )
    );
    echo $wpdb->last_query;
    if( $delete_row ) {
        $ret_val = true;
    } else {
        $ret_val = false;
    }
    echo $ret_val;
    die();
}

function imc_display_hh_data() {
    include_once( 'imc-happy-hours.php' );
    $favorite = get_favorite_hh_type();
    $sort = isset( $_POST[ 'sort' ] ) ? htmlspecialchars( $_POST[ 'sort' ] ) : 'time';
    $hh_sql = imc_get_all_hh_types( $favorite );
    $current_day = date( 'N' ); 
    get_best_happy_hours( $favorite, $hh_sql, $current_day, $sort );
    echo '</div>';
    
    
    
}

function imc_return_hh_string( $single_var ) {
    switch( $single_var ) {
        case 1: //draft beer
            $ret_val = '2,4';
            break;
        case 2:
            $ret_val = '1,3';
            break;
        case 3:
            $ret_val = '5,6';
            break;
        case 4:
            $ret_val = '7,8,9,10';
            break;
        case 0:
        default:
            $ret_val = '0';
    }
    return $ret_val;
}


function imc_get_all_hh_types( $types ) {
    $hh_types_array = explode(',', imc_return_hh_string( $types ) );
    $temp_sql = ' WHERE ( ';
    
    for( $hh_counter = 0; $hh_counter < count( $hh_types_array ); $hh_counter++ ) {
        if( $hh_counter > 0 ) {
            $temp_sql .= ' OR hhs.happy_hour_type = ' . $hh_types_array[ $hh_counter ];
        } else {
            $temp_sql .= 'hhs.happy_hour_type = ' . $hh_types_array[ $hh_counter ];
        }
    }
    if( strlen( $temp_sql ) > 15 ) {
        $temp_sql .= ' ) AND ';
    }
    if( $temp_sql == ' WHERE ( hhs.happy_hour_type = 0 ) AND ' ) {
        $temp_sql = ' WHERE ';
    }
    
    return $temp_sql;
}

function imc_is_selected( $value, $post ) {
    $ret_val = '';
    if( isset( $_POST[ $post ] ) ) {
        if( $_POST[ $post ] == $value ) {
            $ret_val = ' checked="checked" ';
        }
    } else {
        if( $value == 0 ) {
            $ret_val = ' checked="checked" ';
        }
    }
    return $ret_val;
    
}



function imc_update_happy_hour() {
    $after_midnight = 0;
    $happy_hour = imc_clean_happy_hour_post( $_POST );
    $input_amount = $happy_hour[ 'happy_hour_input_total' ] - 1;
    $end_time_fix = $happy_hour[ 'time_end' ];
    if( strtotime( $happy_hour[ 'time_end' ] ) < strtotime( '06:00:00') ) {
        $after_midnight = 1;
        $end_time_fix = '24:00:00';
    }
    global $wpdb;
    $hh_type_array = $happy_hour[ 'days_array' ];
    for( $hh_count = 0; $hh_count <= $input_amount; $hh_count++ ) {
        $wpdb->insert( 
            $wpdb->prefix . 'imc_happy_hour',
                array(
                    'day'                       => intval( $hh_type_array[ $hh_count ] ),
                    'venue_id'                  => $happy_hour[ 'venue_id' ],
                    'time_start'                => $happy_hour[ 'time_start' ],
                    'time_end'                  => $happy_hour[ 'time_end' ],
                    'time_end_fix'              => $end_time_fix,
                    'after_midnight'            => $after_midnight,
                    'happy_hour_type'           => $happy_hour[ 'type' ],
                    'price'                     => $happy_hour[ 'price' ],
                    'reg_price'                 => $happy_hour[ 'reg_price' ],
                    'ounces'                    => $happy_hour[ 'ounces' ],
                    'price_per_oz'              => $happy_hour[ 'price_per_oz' ],
                    'summary'                   => $happy_hour[ 'summary' ],
                    'updated_by'                => $happy_hour[ 'updated_by' ],
                    'updated_date'              => date( "Y-m-d H:i:s" )
                )
        );
    }
    
}
function imc_check_hh_end_time( $time_value ) {
    $time_array = array( '00:00:00', '0:30:00', '1:00:00', '1:30:00', '2:00:00', 
        '2:30:00', '3:00:00', '3:30:00', '4:00:00', '4:30:00', '5:00:00', '5:30:00' );
    $ret_val = '';
    foreach($time_array as $time_value ) {
        if( $time_value == $time_array ) {
            $ret_val = '24:00:00';
        }
    }
    return $ret_val;
}

function imc_get_random_venue_image( $image_array ) {
    $images = array();
    $count = 0;
    foreach($image_array as $image) {
        if($image != '' && $image != 'http://') {
            $count++;
            array_push($images, $image);
        }
    }
    if(count($images) == 0) return '';
    $random_number = rand( 0, $count - 1 );
    return $images[ $random_number ];
}

function imc_get_plugin_image( $image ) {
        $url = plugins_url( 'images/' . $image . '.png' , __FILE__ );
        return $url;
}

function imc_get_day_reverse( $day_value ) {
    $days_of_week = array( 
        0 =>    'Sunday', 
        1 =>    'Monday', 
        2 =>    'Tuesday', 
        3 =>    'Wednesday', 
        4 =>    'Thursday', 
        5 =>    'Friday', 
        6 =>    'Saturday'
    );
    $new_day = array_search( $day_value, $days_of_week );   
    return $new_day;
}

function imc_happy_hour_types( $id ) {
    switch( $id ) {
        case 1:
            $ret_val = 'Domestic Bottle';
            break;
        case 2:
            $ret_val = 'Domestic Draft';
            break;
        case 3:
            $ret_val = 'Import Bottle';
            break;
        case 4:
            $ret_val = 'Import Draft';
            break;
        case 5:
            $ret_val = 'House Wine';
            break;
        case 6:
            $ret_val = 'Special Wine';
            break;
        case 7:
            $ret_val = 'Well Drinks';
            break;
        case 8:
            $ret_val = 'Premium Drinks';
            break;
        case 9:
            $ret_val = 'Top Shelf Drinks';
            break;
        case 10:
            $ret_val = 'Shots';
            break;
        default:
            $ret_val = '';
    }
    return $ret_val;
}

function imc_events_helper() {
    include_once( 'imc-events.php' );
    wp_enqueue_script( 'imc-events', plugin_dir_url(__FILE__) . 'js/imc-events.js', array( 'jquery' ) );
}

function imc_get_venue_details_event( $venue_id ) {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue WHERE id = ' . $venue_id;
    $results = $wpdb->get_row( $sql );
    return $results;
}
function imc_get_venue_event_details_event( $event_id ) {
    $meta = get_post_meta( $event_id );
    $venue = new stdClass();
    $venue->location    = $meta[ '_EventLocation' ][0];
    $venue->name        = $meta[ '_EventLocation' ][0];
    $venue->address     = $meta[ '_EventAddress' ][0];
    $venue->address2    = '';
    $venue->city        = $meta[ '_EventCity' ][0];
    $venue->state       = $meta[ '_EventState' ][0];
    $venue->zip         = '';
    $venue->phone       = $meta[ '_EventPhone' ][0];
    $venue->website     = $meta[ '_EventWebsite' ][0];
    return $venue;
}

function imc_get_event_venue_id( $post_name ) {
    $name_array = explode( '-', $post_name ); 
    $venue_id = get_post_meta( $name_array[0], 'venue_id' );
    return $venue_id[0];
}

function imc_get_entertainers_venue() {
    $venue_id = get_the_ID();
    global $wpdb;
    $current_date = date( 'Y-m-d G:i:s' );
    $week = strtotime( '+7 day' );
    $future_week = date( 'Y-m-d G:i:s', $week );
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_events AS e JOIN ' . $wpdb->prefix . 'posts AS p ON p.ID = e.event_id WHERE e.venue_id = ' . $venue_id . ' AND e.end_date BETWEEN "' . $current_date . '" AND "' . $future_week . '" AND e.approved = 1 ORDER BY e.start_date ASC';
    $event_results = $wpdb->get_results( $sql );
    return imc_get_entertainment_html( $event_results, $venue_id );
}

function imc_get_entertainment_html( $events, $venue_id ) {
    $ret_val = '<div id="event-specials">';
    $previous_start_time = '';
    $previous_end_time = '';
    global $wpdb;
    $venueSql = 'SELECT name from ' . $wpdb->prefix . 'imc_venue where wp_post_id = ' . $venue_id;
    $venueName = $wpdb->get_var($venueSql);
    foreach( $events as $event ) {
        if($event->start_date != $previous_start_time && $event->end_date != $previous_end_time) {
            global $wpdb;
            $urlSql = 'SELECT guid FROM ' . $wpdb->prefix . 'posts WHERE ID = '. $event->event_id; 
            $url = $wpdb->get_var($urlSql);
            $event_date = $event->start_date;
            $day = date( 'l', strtotime( $event_date ) );
            $date = date( 'n-d', strtotime( $event_date ) );
            $time = date( 'g:i a', strtotime( $event_date ) );
            $ret_val .= '<div class="left">' . $day . '</div><div class="' . $day . '-event" class="right">';
            $ret_val .= '<p class="event-container"><a href="' . $url . '" target="_blank">' . explode(' at ', stripcslashes( $event->title ) )[0] . '</a>';
            $ret_val .= ' - ' . $time . ' - ' . $date . '</div>';
            $previous_start_time = $event->start_date;
            $previous_end_time = $event->end_date;
        }
        
    }
    $ret_val .= '</div>';
    return $ret_val;
}

function imc_create_event_theme() {
    wp_enqueue_script( 'jquery-ui-datepicker' );
    //wp_enqueue_script( 'jquery-ui-datepicker' );
    wp_enqueue_style( 'jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css' );
    include( 'imc-add-event.php' );
}

function imc_get_option_value_integers( $start_int, $end_int,  $skip_value = 1) {
    $ret_val = '';
    for( $current_int = $start_int; $current_int <= $end_int; $current_int++ ) {
        if( $current_int % $skip_value == 0 ) {
            $ret_val .= '<option value="' . sprintf( '%02d', $current_int ) . '">' . sprintf( '%02d', $current_int ) . '</option>';
        }
    }
    return $ret_val;
}

function imc_create_event() { 
    $event_array = imc_get_event_post_data(); // [0]tribe event post data, [1]tribe event meta data, [2]imc event data
    $postid = create_tribe_event( $event_array[ 0 ], $event_array[ 1 ] );
    $event_array[ 2 ][ 'event_id' ] = $postid;
    $imc_event_id = create_imc_event( $event_array[ 2 ] );
    if($_POST[ 'duplicate-event' ] == '0') {
        echo 'clear';
    }
    die();
}

function imc_get_event_url( $id ) {
    global $wpdb;
    $sql = 'SELECT guid FROM ' . $wpdb->prefix . 'posts WHERE ID = ' . $id;
    return $wpdb->get_var( $sql );
}

function create_tribe_event( $event_post_array, $event_meta_array ) {
    global $wpdb;
    $postid = wp_insert_post( $event_post_array );
    if(is_wp_error($postid)) {
        echo $return->get_error_message();
    }
    foreach ( $event_meta_array  as $key => $value) {
        add_post_meta( $postid, $key, $value );
    }
    return $postid;
}

function create_imc_event( $event_array ) {
    global $wpdb;
    $table = $wpdb->prefix . 'imc_events';
    $new_record = $wpdb->insert( $table, $event_array );
    return $new_record;
}

function imc_get_attachment_id_from_src ($image_src) {
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_src'";
    $id = $wpdb->get_var($query);
    return $id;
}
        
function imc_get_event_post_data() {
    global $wpdb;
    if(isset($_POST['venue-id']) && $_POST['venue-id'] != '') {
        $venue_id = intval( $_POST[ 'venue-id' ] );
    } else {
        $venue_id = '';
    }
   
    $event_website =                sanitize_text_field( $_POST[ 'event-website' ] );
    $event_name =                   sanitize_text_field( $_POST[ 'event-title' ] );
    $event_description =            sanitize_text_field( $_POST[ 'event-description' ] );
    $event_category =               intval( $_POST[ 'event-category' ]);
    $event_band =                   intval( $_POST[ 'event_band' ] );
    $start_date =                   sanitize_text_field( $_POST[ 'start-date' ] );
    $start_hour =                   sanitize_text_field( $_POST[ 'start-hour' ] );
    $start_minute =                 sanitize_text_field( $_POST[ 'start-minute' ] );
    $start_meridian =               sanitize_text_field( $_POST[ 'start-meridian' ] );
    $end_date =                     sanitize_text_field( $_POST[ 'end-date' ] );
    $end_hour =                     sanitize_text_field( $_POST[ 'end-hour' ] );
    $end_minute =                   sanitize_text_field( $_POST[ 'end-minute' ] );
    $end_meridian =                 sanitize_text_field( $_POST[ 'end-meridian' ] );
    $event_cost =                   sanitize_text_field( $_POST[  'event-cost' ] );
    $event_image =                  sanitize_text_field( $_POST[ 'imc_image_url' ] );
    $venue_name =                   sanitize_text_field( $_POST[ 'term' ] );
    $venue_address =                sanitize_text_field( $_POST[ 'venue-address' ] );
    $venue_city =                   sanitize_text_field( $_POST[ 'venue-city' ] );
    $venue_state =                  sanitize_text_field( $_POST[ 'venue-state' ] ); 
    $venue_phone =                  sanitize_text_field( $_POST[ 'venue-phone' ] );
    $venue_website =                sanitize_text_field( $_POST[ 'venue-website' ] );
    $thumbnail_id =                 imc_get_attachment_id_from_src( $event_image );
    if( $thumbnail_id == null ) { $thumbnail_id = ''; }
    $tribe_event_post_array = array( 
        'post_content'              => $event_description,
        'post_name'                 => trim($venue_id . '-' . str_replace( ' ', '-', $event_name ) . '-' . $start_date),
        'post_title'                => $event_name,
        'post_status'               => 'publish',
        'post_type'                 => 'tribe_events',
        'comment_status'            => 'closed',
        'ping_status'               => 'closed'
    );
    $tribe_event_post_meta_array = array(
        '_EventOrgin'               => 'events-calendar',
        '_thumbnail_id'             => $thumbnail_id,
        '_EventShowMapLink'         => '1',
        '_EventShowMap'             => '1',
        '_EventStartDate'           => $start_date . ' ' . date( "H:i:s", strtotime( $start_hour . ':' . $start_minute . ':00 ' . $start_meridian ) ),
        '_EventEndDate'             => $end_date . ' ' . date( "H:i:s", strtotime( $end_hour . ':' . $end_minute . ':00 ' . $end_meridian ) ),
        '_EventVenueID'             => $venue_id,
        '_EventCurrencySymbol'      => '$',
        '_EventCurrencyPosition'    => 'prefix',
        '_EventCost'                => $event_cost,
        '_EventURL'                 => $event_website,
        '_EventBand'                => $event_band,
        '_EventLocation'            => $venue_name,
        '_EventAddress'             => $venue_address,
        '_EventCity'                => $venue_city,
        '_EventState'               => $venue_state,
        '_EventPhone'               => $venue_phone,
        '_EventWebsite'             => $venue_website
    );
    $imc_event_array = array(
        'title'                     => $event_name,
        'image'                     => $event_image,
        'event_category'            => $event_category,
        'venue_id'                  => $venue_id,
        'event_band'                => $event_band,
        'start_date'                => $start_date . ' ' . date( "H:i:s", strtotime( $start_hour . ':' . $start_minute . ':00 ' . $start_meridian ) ),
        'end_date'                  => $end_date . ' ' . date( "H:i:s", strtotime( $end_hour . ':' . $end_minute . ':00 ' . $end_meridian ) )
    );
    return [ $tribe_event_post_array, $tribe_event_post_meta_array, $imc_event_array ];
}

//include('cache/happy-hour-cache.php');

function imc_display_home_page( $category = 'all' ) {
    //imc_get_slider( $category ); removed in place of ad display
    imc_get_ad_campaign( 4 );
    imc_get_events();
    imc_get_random_venue();
    imc_get_venue_search();
    imc_get_best_happy_hours();
    //imc_get_top_rated_hh();
}

function imc_get_ad_campaign( $campaign_id, $category = 'all' ) {
    if( function_exists( 'display_campaign' ) ) {
        echo '<div id="imc-campaign-' . $campaign_id . '">';
        display_campaign( $campaign_id );
        echo '</div>';
        wp_enqueue_script( 'imc-rotate-ads', plugin_dir_url(__FILE__) . 'js/imc-rotate-ads.js', array( 'jquery' ) );
    } else {
        imc_get_slider( $category );
    }
}

//include('cache/cach_footer.php');

function imc_get_slider( $category ) {
    $venues = imc_get_paid_venues( $category );
    $results = imc_get_slider_html( $venues );
    echo $results;
}
function imc_get_events() {
    $events = imc_get_current_events();
    $event_html = imc_get_events_homepage( $events );
    echo $event_html;
}

function imc_get_current_events() {
    global $wpdb;
    $current_date = date( 'Y-m-d G:i:s' );
    $week = strtotime( '+30 day' );
    $future_week = date( 'Y-m-d G:i:s', $week );
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_events as e JOIN ' . $wpdb->prefix . 'posts AS p ON p.ID = e.event_id WHERE e.end_date BETWEEN "' . $current_date . '" AND "' . $future_week . '" AND approved = 1 ORDER BY e.start_date ASC LIMIT 10';
    // ' AND e.start_date BETWEEN "' . $current_date . '" AND "' . $future_week . '"';
    //$sql = 'SELECT * FROM wp_imc_events AS e JOIN wp_posts AS p ON p.post_name LIKE "%" LIMIT 10';
    $event_results = $wpdb->get_results( $sql );
    return $event_results;
}

function imc_get_events_homepage( $events ) {
    $html = imc_get_event_html( 'header' );
    $eventCounter = 0;
    foreach( $events as $event ) {
        $eventCounter++;
        $eventCounter = $eventCounter == 3 ? 1 : $eventCounter;
        $startdate = new DateTime( $event->start_date );
        $enddate = new DateTime( $event->end_date );
        $start = $startdate->format('m/d h:i a');
        $end = $enddate->format('m/d h:i a');
        /*$img_array = getimagesize( $event->image );
        if( ! is_array( $img_array ) ) {
            $event->image = '';
        } */
        if( $event->image != '' ) {
        $img = '<img class="home-event-image" src="' . $event->image . '" alt="' . $event->title . '" />';
        } else {
            $img = '';
        }
        $description = $event->post_content;
        if( strlen( $description ) > 400 ) {
            $description = substr( $description, 0, strpos( $description, ' ', 400 ) ) . '...';
        } 
        if($eventCounter == 1) {
            $html .= '<div class="eventRow">';
        } 
        $html .= '<div class="eventColumn">'
            . '<a href="' . $event->guid . '"><h3>' . stripslashes( $event->title ) . '</h3>'
            . '<p class="excerpt">' . $img . '<strong>' . $start . '</strong></p></a>' 
            . '</div>';
        if($eventCounter == 2) {
            $html .= '</div>';
        }
    }
    $html .= imc_get_event_html( 'footer' );
    return $html;
}

function imc_get_event_html( $block ) {
    switch( $block ) {
        case 'header':
            $html = '<div id="venue-this-week" class="home-event"><div id="venue-events">'
                . '<img class="icon" src="' . imc_get_plugin_image( 'event-icon' ) . '" />'
                . '<img class="title" src="' . imc_get_plugin_image( 'events-title' ) . '" alt="Events" />'
                . '</div>'
                . imc_get_event_totals()
                . '<div class="events-list">';
            break;
        case 'footer':
            $html = '</div><a class="float-right" href="events">See All Upcoming Events</a></div><div class="clear"></div>';
    }
    return $html;
}

function imc_get_best_happy_hours() {
    $venues = imc_get_best_hh_venues();
    echo $venues;
}

function imc_get_top_rated_hh() {
    include_once('happy_hour_daily.php');
    $venues = display_top_10();
    $html = '<h3>Top 10 Happy Hours Today</h3>';
    $html .= "<div>";
    $html .= "<table class='hh'>";
    $counter = 1;
    
    foreach ($venues as $venue) {
        $link = '<a href="' . $venue->wp_guid . '">' . $venue->name . '</a>';
        $html .= "<tr><td><h3><a href='" . $venue->wp_guid . "'>" . $counter . "</a></h3><td><h3>" . $link . "</h3></td></tr>";
        $counter++;
    }
    $html .= "</table>";
    $html .= "</div>";
    return $html;
}

function get_happy_hour_meta() {
    global $wpdb; 
    $sql = 'SELECT '; //GET THIS STATEMENT
    $table_name = $wpdb->prefix . 'imc_top_rated';
    
}

function imc_get_event_totals() {
    $totals = imc_get_breakdown_object();
    $html = '<div id="event-totals-home">'
          . '<table cellpadding="5"><tr>'
          . '<td>Events this month</td>'
          . '<td>' . $totals->events_month . '</td>'
          . '<td>Music events this month</td>'
          . '<td>' . $totals->music_month . '</td>'
            . '</tr><tr>'
          . '<td>Total events today</td>'
          . '<td>' . $totals->events_today . '</td>'
          . '<td>Music events this week</td>'
          . '<td>' . $totals->music_week . '</td>'
          . '</tr><tr>'
          . '<td>Total restaurants</td>'
          . '<td>' . $totals->venues . '</td>'
          . '<td>Total Bands/Musicians</td>'
          . '<td>' . $totals->bands . '</td>'
          . '</tr></table></div>';
    return $html;
}

function imc_get_breakdown_object() {
    $events_this_month = imc_get_totals( 'events', 'month' );
    $music_this_month = imc_get_totals( 'music', 'month' );
    $music_this_week = imc_get_totals( 'music', 'week' );
    $events_today = imc_get_totals( 'events' );
    $restaurants = imc_get_totals( 'venues' );
    $bands = imc_get_totals( 'bands' );
    $totals = new stdClass();
    $totals->events_month = $events_this_month;
    $totals->music_month = $music_this_month;
    $totals->music_week = $music_this_week;
    $totals->events_today = $events_today;
    $totals->venues = $restaurants;
    $totals->bands = $bands;
    return $totals;
}

function imc_get_totals( $table, $time = '' ) {
    $day = date( 'Y-m-d G:i:s' );
    switch( $time ) {
        case 'month':
            $time = date( 'Y-m-d G:i:s', strtotime( '+30 day' ) );
            break;
        case 'week':
            $time = date( 'Y-m-d G:i:s', strtotime( '+7 day' ) );
            break;
        default:
            $time = '';
    }
    
    switch ( $table ) {
        case 'events':
            $count = imc_events_timeframe( $day, $time );
            break;
        case 'music':
            $count = imc_events_timeframe( $day, $time, ' AND event_category = 1' );
            break;
        case 'venues':
            $count = imc_total_venues();
            break;
        case 'bands':
            $count = imc_total_bands();
            break;
        default:
            $count = '';
    }
    return $count;
}
function imc_total_venues() {
    global $wpdb;
    $sql = 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'imc_venue WHERE is_active = 1 OR is_active IS null';
    $count = $wpdb->get_var( $sql );
    return $count;
}

function imc_total_bands() {
    global $wpdb;
    $sql = 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'imc_bands';
    $count = $wpdb->get_var( $sql );
    return $count;
}

function imc_events_timeframe( $day, $time = '', $music = '' ) {
    if( $time == '' ) {
        if( $music == '' ) {
            $day = trim(mb_substr( $day, 0, -8));
            $sql = ' DATE(start_date) = CURDATE() ';
            //$sql = ' start_date >= NOW() AND start_date < CURRENT_DATE() + INTERVAL 1 DAY';
            
//            $sql = ' start_date = CURDATE() AND start_date < CURDATE() + INTERVAL 1 DAY ';
            

            //$sql =' CURDATE() between start_date and end_date ';
            //$sql =' start_date like "%'.$day.'%"';
            //$sql = ' LIKE "%' . $day . '%" OR end_date LIKE "%' . $day . '%"';
        } else {
            $sql = 'start_date = "' . $day . '" ' . $music;
        }
    } else {
        /* debugging
        echo '<hr>';
        echo 'day: '.$day.'<br>time: '.$time.'<br>';
        var_dump($music); */
        $sql = 'end_date BETWEEN "' . $day . '" AND "' . $time . '" ' . $music;
    }
    global $wpdb;
    $count_sql = 'SELECT COUNT(*) FROM ' . $wpdb->prefix . 'imc_events AS e INNER JOIN ' . $wpdb->prefix . 'posts AS p ON e.event_id = p.ID WHERE  ' . $sql . ' AND approved = 1';
    //var_dump($count_sql);
    if($time == '') {
        
        //die(var_dump($count_sql));
    }
    
    /*
     * $resultsql = 'SELECT * from ' . $wpdb->prefix . 'imc_events WHERE  ' . $sql;
     */
    /* debug
    $results = $wpdb->get_results($resultsql);
    
    var_dump($resultsql);
    var_dump($results);
    echo '<hr>'; */
    
    /*if($time=='' && $music==''){
        echo '<hr>';
        echo $resultsql;
        echo '<hr>';
        var_dump($results);
        echo '<hr>';
    }*/
    
    $count = $wpdb->get_var( $count_sql );
    
    return $count;
}

function imc_get_best_hh_venues() {
    global $wpdb;
    $day = date('N') == 7 ? 0 : date('N');
    $pre = $wpdb->prefix;
    //save for possible future optimization
    //$sql = 'SELECT ' . $pre . 'imc_happy_hour.summary, ' . $pre . 'imc_happy_hour_type.happy_hour_type, min(' . $pre . 'imc_happy_hour.price_per_oz) FROM ' . $pre . 'imc_happy_hour JOIN ' . $pre . 'imc_happy_hour_type ON ' . $pre . 'imc_happy_hour.Happy_hour_type = ' . $pre . 'imc_happy_hour_type.id AND ' . $pre . 'imc_happy_hour.day = ' . $day . ' AND ' . $pre . 'imc_happy_hour.happy_hour_type <> 0 GROUP BY happy_hour_type';
    //$sql = 'SELECT hh.id, hh.happy_hour_type, hh.time_start, hh.time_end, hh.all_day, hh.price, hh.ounces, hh.summary, venue.name, venue.wp_guid FROM ' . $pre . 'imc_happy_hour as hh JOIN ' . $pre . 'imc_venue as venue on hh.venue_id = venue.id WHERE hh.day = ' . $day . ' AND hh.price_per_oz IS NOT NULL AND hh.happy_hour_type IN (1,2,3,4,5,6,7,8,9,10) GROUP BY hh.happy_hour_type ORDER BY hh.price_per_oz ASC';
    $sql = 'SELECT hh.time_start, hh.time_end, (TIME_TO_SEC(hh.time_end) - TIME_TO_SEC(hh.time_start)) as total_hours, hh.venue_id, venue.id, venue.name, venue.wp_guid FROM '. $wpdb->prefix.'imc_happy_hour as hh INNER JOIN '.$wpdb->prefix.'imc_venue as venue on hh.venue_id = venue.id where venue.is_active = 1 and hh.day = ' . $day . ' GROUP BY hh.venue_id ORDER BY total_hours DESC LIMIT 30';
//    if(current_user_can('manage_options')) {
//        var_dump($sql);
//    }
    $venues = $wpdb->get_results( $sql );
    //var_dump($sql);
    $result = imc_get_best_hh_html( $venues );
    return $result;
}

function imc_get_happy_hour_table($sort = 'time') {
    global $wpdb;
    $sort_sql = imc_get_happy_hour_sort($sort);
    $table = $wpdb->prefix . 'imc_happy_hour_type';
    $sql = 'SELECT * FROM ' . $table;
    $hh_array = $wpdb->get_results( $sql, ARRAY_A );
    return $hh_array;
}

function cleanSort($sort) {
    $newSort = '';
    $cleaned = explode('.', $sort);
    if($cleaned[0] = 'hhs') {
      $newSort = ' ORDER BY '. $cleaned[1];  
    } 
    return $newSort;
}

function imc_get_happy_hour_sort($sort = 'time') {
    switch($sort) {
        case 'name':
            $sort_order = ' venue.name';
            break;
        case 'time':
        case 'time_start':
            $sort_order = ' hhs.time_start';
            break;
        case 'price':
            $sort_order = ' hhs.price_per_oz';
            break;
        case 'cheapest':
            $sort_order = ' hhs.price';
            break;
        default:
            $sort_order = ' hhs.time_start';      
    }
    return $sort_order;
}

function imc_get_best_hh_html( $venues ) {
    global $wpdb;
    $hh_type_array = imc_get_happy_hour_table('time');
    $open_div = "<div ";
    $close_tag = ">";
    $close_div = "</div>";
    $float_clear = "<div style='clear:both;'></div>";
    $html = $open_div." class='daily-hh'".$close_tag;
    $html .= $open_div." class='daily-hh-list'".$close_tag; 
    $html .= $open_div . ' class="home-hh-icon"' . $close_tag . '<img src="' . imc_get_plugin_image( 'happy-hour-icon' ) .'" />' . $close_div;
    $html .= '<div class="center"><img class="hh-title-home" src="' . imc_get_plugin_image( 'happy-hour-title' ) . '" /></div>';
    $html .= '<div class="half-hh">';
    $html .= '<h3>Most Happy Hours Today</h3><br>';
    $html .= "<table class='hh'>";
    $html .= "<tr><td></td><td>Start Time</td><td>End Time</td><td>Total Hours</td></tr>";
    $count = 1;
    foreach ($venues as $venue) {
        if($count < 11) {
            $start = date('h:ia', strtotime($venue->time_start));
            $end = date('h:ia', strtotime($venue->time_end));
            $html .= "<tr><td><a href='" . $venue->wp_guid . "'><strong>" . trim($venue->name) . "</strong></td><td>". $start ."</td><td>". $end ."</td><td>". (($venue->total_hours / 60) / 60)   ."</tr>";
            //hh price$html .= "<tr><td>" . $hh_type . "</td><td><a href='" . $value->wp_guid . "'><strong>". trim($value->name) ."</strong></a></td><td><span class='daily-hh-list-price'>$". $price ."</span></i></td></tr>";
        }
        
        $count++;
    }
    $html .= "</table>";
    
    $html .= $close_div;
    $html .= '<div class="half-hh">' . imc_get_top_rated_hh() . $close_div; 
    $html .= $float_clear;
    $html .= $close_div; 
    return $html;
}


function imc_get_paid_venues( $category ) {
    global $wpdb;
    $options = imc_get_slider_sql( $category );
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_slider WHERE is_active = 1' . $options;
    $results = $wpdb->get_results( $sql );
    return $results;
}


function imc_get_slider_sql( $category ) {
    if( $category == 'all' ) {
        $ret_val = '';
    } else {
        $ret_val = 'AND cuisine1 = "' .$category . '"';
    }
    return $ret_val;
}

function imc_get_slider_html( $results ) {
    $html = '<div id="paid-slider-wrapper">';
    $counter = 1;
    foreach( $results as $result ) {
        $result = get_object_vars( $result );
        $html .= imc_get_venue_slider( $result, 'a' );
    }
    $html .= '</div>';
    $html .= imc_get_slider_jquery();
    return $html;
}

function imc_get_cuisine_name( $id ) {
    global $wpdb;
    $sql = 'SELECT name FROM ' . $wpdb->prefix . 'imc_venue_cuisine_list WHERE id = ' . $id;
    $cuisine = $wpdb->get_var( $sql );
    return $cuisine;
}

function imc_get_venue_slider( $venue, $view ) {
    if( $venue[ 'image'] == '' ) {
        $venue[ 'image'] = 'http://aznrestaurant.com/naples/files/2013/03/sushiSlide1.jpg';
    }
    if( $venue[ 'hh_hours' ] == '' ) {
        $venue[ 'hh_hours' ] = 'Join us for happy hour 24 hours a day, 7 days a week!';
    }
    if( $venue[ 'special' ] == '' ) {
        $venue[ 'special' ] = 'Come on out tonight for the best ribs you have ever had!';
    }
    $link = '<' . $view . ' href="' . get_site_url() . '/restaurants' . $venue[ 'link' ] . '">';
    $html = '<div class="paid-slider">'
            . $link
            . '<div class="image-container">'
            . '<img src="' . $venue[ 'image' ] . '" alt="' . $venue[ 'name' ] . '" />'
            . '</div>'
            . '</' . $view . '>'
            . '<div class="paid-slider-content"><img class="food-icon" src="' . imc_get_plugin_image( 'food-icon' ) . '" />' . $link
            . '<h2>' . $venue[ 'name' ] . ' <span class="smaller"> ' . imc_get_cuisine_name( $venue[ 'cuisine' ] ) . '</span></h2>'
            . '<div id="special" class="paid-slider-specials"><h3>' . $venue[ 'special' ] . '</h3></div>'
            . '<script>jQuery("#special h3").html(jQuery("#replace-special").val())</script>'
            . '<div id="hh-hours" class="paid-slider-hh"><h3>' . $venue[ 'hh_hours' ] . '</h3></div>'
            . '<script>jQuery("#hh-hours h3").html(jQuery("#replace-hh-hours").val())</script>'
            . '</' . $view . '></div></div>'; 
    return $html;
}

function imc_random_string() {
    $quotes = array( 
        'Come on out tonight for the best ribs you have ever had!',
        'Join us for happy hour 24 hours a day, 7 days a week!',
        'That\'s what she said',
        'I\'m on a whiskey diet. I\'ve lost three days already.',
        'My wife sent her photograph to the Lonely Hearts Club. They sent it back saying they weren\'t that lonely.',
        'Well, here\'s another nice mess you\'ve gotten me into.',
        'Trying is the first step towards failure.',
        'I shot my first Turkey today. Scared the crap out of everyone in the frozen food section, it was awesome!'
    );
    shuffle( $quotes );
    return $quotes[0];
}

function imc_add_slider() {
    global $wpdb;
    $slider = imc_clean_slider();
    if( $slider[ 'id' ] == 'new' ) {
        $saved_slider = $wpdb->insert( 
            $wpdb->prefix . 'imc_venue_slider', $slider
        );
    } else {
        $saved_slider = $wpdb->update( $wpdb->prefix . 'imc_venue_slider', $slider, array( 'id' => $slider[ 'id' ] ) );
    }
    
}

function imc_clean_slider() {
    $POST = array_map( 'stripslashes_deep', $_POST );
    $slider = array(
        'id'            => $POST[ 'id' ],
        'venue_id'      => $POST[ 'venue_id' ],
        'name'          => $POST[ 'name' ],
        'cuisine1'      => $POST[ 'cuisine1' ],
        'special'       => $POST[ 'special' ],
        'hh_hours'      => $POST[ 'hh_hours' ],
        'link'          => $POST[ 'link' ],
        'image'         => $POST[ 'image' ],
        'is_active'     => '1'
    );
    return $slider;
}

function imc_get_slider_jquery() {
    wp_enqueue_script( 'imc-slider-rotate', plugin_dir_url(__FILE__) . 'js/imc-slider-rotate.js', array( 'jquery' ) );
}

function imc_get_advanced_search_jquery() {
    wp_enqueue_script( 'imc-advanced-search', plugin_dir_url(__FILE__) . 'js/imc-advanced-search.js', array( 'jquery' ) );
}

function imc_get_random_venue($notice = false) {

    global $wpdb;
    $sql = 'SELECT wp_guid FROM ' . $wpdb->prefix . 'imc_venue WHERE is_active = 1 ORDER BY RAND() LIMIT 100';
    $venues = $wpdb->get_results( $sql );
    $count = 0;
    foreach($venues as $venue) {
        $count++;
        echo '<div id="'.$count.'" class="hidden">'.$venue->wp_guid.'</div>';
    }
    $html = '<div class="half-container"><div id="random-venue" class="half-home">'
            . '<h2>Random Restaurant Picker</h2>'
            . '<p class="center" style="color:#fff;">'
            . '<a class="more-options" href="#">'
            . 'Take Me To A Random Restaurant'
            . '</a></div></div>';
    if( $notice == true ) {
        return $html;
    } else {
        echo $html;
    }
    
}

function imc_get_venue_search() {
    include( 'imc-get-venue-options.php' );
    $html = '<div id="advanced-search" class="half-home"><h2>Search for a restaurant</h2>';
    $cuisine = get_options( 'cuisine' );
    $all_cuisine = explode( '</label>', $cuisine );
    array_pop( $all_cuisine );
    $random_cuisine_keys = array_rand( $all_cuisine, 8 );
    foreach( $random_cuisine_keys as $key ) {
        $html .= $all_cuisine[ $key ] . '</label>';
    }
    $html .= '<p class="center"><a class="more-options" href="restaurants/">See More Options</a></p>';
    $html .= '</div></div><div class="clear"></div>';
    echo $html;
    imc_get_advanced_search_jquery();    
}

function musician_scripts() {
    wp_enqueue_script( 'imc-add-musicians', plugin_dir_url(__FILE__) . 'js/imc-add-musicians.js', array( 'jquery' ) );
}

function imc_add_musician_button() {
    $html = '<div id="add-musician" name="add-band-button"><span>Add Musician</span></div>';
    $modal = '<div id="modal"><div id="add-musician-input"><img class="close-modal" src="' . imc_get_plugin_image( 'close' ) . '" />';
    $modal .= '<form id="imc-add-band-form" action="" method="POST">';
    $modal .= wp_nonce_field('create_none_for_wti','wti_like_post_meta_box_nonce');
    $modal .= '<h1>Add A Band or Musician</h1>';
    $modal .= '<div class="bandcol">';
    $modal .= '<label>Band/Musician Name: <input type="text" id="name" name="band_name" /></label>';
    $modal .= '<label>Website: <input type="text" id="website" name="website" /></label>';
    $modal .= '<label>Soundcloud: <input type="text" id="soundcloud" name="soundcloud" /></label>';
    $modal .= '<label>Facebook: <input type="text" id="facebook" name="facebook" /></label>';
    $modal .= '</div><div class="bandcol">';
    $modal .= '<label>YouTube: <input type="text" id="youtube" name="youtube" /></label>';
    $modal .= '<label>Twitter: <input type="text" id="twitter" name="twitter" /></label>';
    $modal .= '<label>Instagram: <input type="text" id="instagram" name="instagram" /></label>';
    $modal .= '<label>Band Picture URL/Link <input type="text" id="image" name="image" /></label>';
    if(current_user_can('manage_options')) {
        $modal .= '<a style="color: #fff;" href="http://napleslivenow.com/wp-admin/media-new.php" target="_blank">Upload an image</a>';
    }
    $modal .= '</div>';
    $modal .= '<div class="genre-container">';
    $modal .=  imc_get_genres_boxes( );
    $modal .= '</div>';
    $modal .= '<label>Bio: <textarea id="bio" name="bio"></textarea>';
    $modal .= '<input type="hidden" name="action" value="imc_add_band" />';
    $modal .= '<input id="imc-add-band" type="submit" class="button-primary" value="Add Musician / Band" />';
    $modal .= '<img src="' . admin_url('/images/wpspin_light.gif') . '" class="waiting" id="imc-loading" style="display:none;" /></form>';
    $modal .= '<div id="imc-band-results"></div>';
    $html .= '<div class="hidden" id="modal-temp">' . $modal . '</div>';
    echo $html;
    return musician_scripts();    
}

function imc_get_genres() {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_music_genres';
    $genres = $wpdb->get_results( $sql );
    $html = '<option value="">Select One</option>';
    
    foreach( $genres as $genre ) {
        $html .= '<option value="' . $genre->id . '">' . $genre->name . '</option>';
    }
    return $html;
}
function imc_get_genres_boxes( $genre_string = null ) {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_music_genres';
    $genres = $wpdb->get_results( $sql );
    $html = '';
    if(is_null($genre_string)) {
        foreach( $genres as $genre ) {
        
            $html .= '<label><input name="genre[]" type="checkbox" value="' . $genre->id . '" /> ' . $genre->name . '</label><br>';
        }
    } else {
        echo $genre_string;
        $genreArray = explode(',', $genre_string);

        foreach( $genres as $genre ) {
            $checked = in_array($genre->id, $genreArray) ? ' checked="checked" ' : ''; 
            $html .= '<label><input name="genre[]" type="checkbox" value="' . $genre->id . '" ' . $checked . ' /> ' . $genre->name . '</label><br>';
        }
    }
    
    return $html;
}

function imc_add_band( $data = 'false' ) {
    $action = $_POST['action'];
    $band = imc_create_band( $action, $data );
    $post_id = imc_create_band_page( $band );
    $success = imc_add_id_to_band( $post_id, $band );
    $meta = imc_create_band_meta( $band, $post_id );
    $url = imc_get_band_url( $post_id );
    $updated = imc_update_band_url( $url, $post_id );
    echo $url;
    return $url;
}

function imc_save_band() {
    global $wpdb;
    if(isset($_POST[ 'deleteband' ]) && $_POST[ 'deleteband' ] == '1') {
        $sql = 'DELETE FROM ' . $wpdb->prefix . 'imc_bands where id = ' . $_POST[ 'saveband' ];
        $delete = $wpdb->query( $sql );
        $saved = $_POST[ 'band_name' ] . ' has been deleted.';
    } else {
        $band = new stdClass();
        foreach( $_POST as $key => $value ) {
            if( $key != 'action' ) {
                if( is_array( $value ) ) {
                    $band->$key = implode( ',', $value );
                } else {
                    $band->$key = sanitize_text_field( $value );
                }

            }
        }
        unset($band->wti_like_post_meta_box_nonce);
        unset($band->_wp_http_referer);

        $bandsave = $wpdb->update( 
            $wpdb->prefix . 'imc_bands', 
            array( 
                'band_name' => $band->band_name,
                'website'   => $band->website,
                'youtube'   => $band->youtube,
                'soundcloud'=> $band->soundcloud,
                'facebook'  => $band->facebook,
                'twitter'   => $band->twitter,
                'instagram' => $band->instagram,
                'image'     => $band->image,
                'genre'     => $band->genre,
                'bio'       => $band->bio
            ), 
            array( 'ID' => $band->saveband )
        );
        $saved = ($bandsave > 0 ) ? $band->band_name . ' has been saved!' : $error = $bandsave === false ? 'There was an error' : 'No changes were made';
    }
    
    echo $saved;
    die();
}

function imc_update_band_url( $url, $post_id ) {
    global $wpdb; 
    $added = $wpdb->update( $wpdb->prefix . 'imc_bands', array( 'url' => $url ), array( 'post_id' => $post_id ) );
    return $added;
}

function imc_create_band( $action, $import ) {
    $band = new stdClass();
    if( $action == 'imc_add_band') {
        foreach( $_POST as $key => $value ) {
            if( $key != 'action' && $key != 'genre') {
                
                $band->$key = sanitize_text_field( $value );
            } 
        }
        if(isset($_POST['genre'])) {
            $band->genre = implode(',', $_POST['genre']);
        }
        
        unset($band->wti_like_post_meta_box_nonce);
        unset($band->_wp_http_referer);
        
    } else {
        $band->band_name = $import[ 'band_name' ];
        $band->genre = $import[ 'genre' ];
    }
    $added = imc_add_band_to_db( $band );
    global $wpdb;
    $band->db_id = $wpdb->insert_id;
    return $band;
}

function imc_add_id_to_band( $id, $band ) {
    $band_id = $band->db_id;
    global $wpdb;
    $added_id = $wpdb->update( $wpdb->prefix . 'imc_bands', array( 'post_id' => $id ), array( 'id' => $band_id ) );
}

function imc_add_band_to_db( $band ) {
    $array = array();
    foreach( $band as $key => $value ) {
        $array[ $key ] = $value;
    }
    global $wpdb;
    $saved_band = $wpdb->insert( 
        $wpdb->prefix . 'imc_bands', $array
    );
    return $saved_band;
}

function imc_create_band_page( $band ) {
    $name = $band->band_name;
    $post = array(
        'post_content'      => $band->bio,
        'post_name'         => $name,
        'post_title'        => $name,
        'post_status'       => 'publish',
        'post_type'         => 'page',
        'post_parent'       => '264',
        'category'          => array( 0 ),
        'comment_status'    => 'closed',
        'ping_status'       => 'closed'
    );
    $post_id = wp_insert_post( $post );
    return $post_id;
}

function imc_create_band_meta( $band, $id ) {
    foreach( $band as $key => $value ) {
        add_post_meta( $id, $key, $value );
    }
}

function imc_get_band_url( $id ) {
    global $wpdb;
    $sql = 'SELECT guid FROM ' . $wpdb->prefix . 'posts WHERE ID = ' . $id;
    $url = $wpdb->get_var( $sql );
    return $url;
}

function imc_get_band( $id ) {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_bands WHERE id = ' . $id;
    $band = $wpdb->get_results( $sql );
    return $band;
}

function display_related_band( $band_id ) {
    if( $band_id != 0 ) {
        echo '<h4 class="event-featuring">Featuring:</h4>';
        $band = imc_get_band( $band_id );
        echo imc_get_band_html( $band );
    }
}

function imc_show_bands() {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_bands WHERE approved = 1 ';
    $limit = 999;
    $record = 1;
    $start = 0;
    $alphafilter = false;
    $filter_sql = '';
    if( isset( $_POST[ 'searchname' ] ) && strlen( $_POST[ 'searchname' ] ) > 0 ) {
        $filter_sql .= ' AND band_name LIKE "%' . $_POST[ 'searchname' ] . '%" ';
        $alphafilter = true;
        $type = 'numerical';
    } elseif( isset( $_POST[ 'letter' ] ) && strlen( $_POST[ 'letter' ] ) > 0 ) {
        $letter = $_POST[ 'letter' ];
        $filter_sql .= ' AND band_name LIKE "' . $letter . '%" ';
        $alphafilter = true;
        $type = 'both';
    } else {
        $type = 'alpha';
    }
    if( isset( $_POST[ 'record' ] ) ) {
        if( $_POST[ 'record' ] > 0 ) {
            $record = $_POST[ 'record' ];
            $start = ( $record - 1 ) * $limit;
            $type = 'numerical';
        } else {
            $record = 1;
        }
    }
    $filter = imc_get_band_genre();
    if( $alphafilter != true ) {
        if( strlen( $filter_sql ) > 1 ) {
            $filter = ' AND ' . $filter;
        } else {
            if( strlen( $filter ) > 0 ) { 
               $filter_sql = ' AND ';
            }
            
        }
    }
    
    
    if( strlen( $filter ) > 1 ) {
        if( $alphafilter == true ) { 
            $filter = ' AND ' . $filter;
        }
    }

    $limit_sql = 'LIMIT ' . $start . ', ' . $limit;
    $sql = $sql . $filter_sql . $filter . ' ORDER BY band_name ASC ' . $limit_sql;
    //var_dump($sql);
    imc_get_musician_pagination( $limit, $record, $sql, $type );
    $bands = $wpdb->get_results( $sql );
    $html = imc_get_band_html( $bands );
    echo $html . '<span class="die">';
}

function imc_get_band_id( $id_type, $id ) {
    global $wpdb;
    $sql = 'SELECT event_band FROM ' . $wpdb->prefix . 'imc_events WHERE ' . $id_type . ' = ' . $id;
    $band_id = $wpdb->get_var( $sql );
    return $band_id;
}

function imc_get_band_genre() {
    $return_value = '';
    if ( isset( $_POST ) ) {
        if( isset( $_POST[ 'genres' ] ) ) {
            $return_value = ' ( ';
            foreach($_POST[ 'genres'] as $key => $genreId) {
                $return_value .= strlen($return_value) > 10 ? ' AND ' : '';
                $return_value .= ' FIND_IN_SET("'.$genreId.'", genre) ';
            }
            $return_value .= ' ) ';
        }
    }
    
    return $return_value;
}

function imc_get_band_html( $bands ) {
    $genres = get_genre_by_id();
    $html = '<div id="band-list">';
    foreach( $bands as $band ) {
        if($band->band_name != '') {
            $html .= '<a href="' . $band->url . '"><div class="band">';
            if( $band->image != '' ) {
                $html .= '<img src="' . $band->image . '" />';
            }
            $html .= '<h2>' . $band->band_name . '</h2>';
            $html .= '<h3>' . imc_get_genre_name( $band->genre, $genres ) . '</h3>';
            $html .= '</div></a>';
        }
    }
    
    $html .= '</div>';
    return $html;
}

function get_genre_by_id() {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_music_genres';
    $genres = $wpdb->get_results( $sql );
    return $genres;
}

function imc_get_genre_name( $band, $all_genres ) {
    $genre_return = '';
    if( strlen( $band ) == 0 ) return '';
    if( strlen( $band ) > 1 ) {
        $genre_array = explode( ',', $band );
        foreach( $genre_array as $key => $id ) {
            $id = $id - 1;
            $genre_return .= $all_genres[$id]->name . ', ';
        }
    } else {
        
        //$genre_return .= $all_genres[$band - 1]->name . ', ';
        $genre_return .= $all_genres[$band - 1]->name . ', ';
    }
    /* Get Rid of comma */
    if( strlen( $genre_return ) > 1 ) {
        $genre_return = mb_substr( $genre_return, 0, -2 );
    }
    return $genre_return;
}

function imc_get_band_page( $custom, $html ) {
    $new_html = substr( $html, 0, strpos( $html, '<div class=\'wti-clear') );
    $allGenres = get_genre_by_id();
    global $wpdb;
    $bandSql = 'SELECT * FROM '.$wpdb->prefix . 'imc_bands where id = '.$custom['db_id'][0]. ' limit 1';
    $band = $wpdb->get_row($bandSql);
    $bio = $band->bio != '' ? '<p>'.$band->bio.'</p>' : '';
    $genreText = '<h3 style="color: #000;">' . imc_get_genre_name( $band->genre, $allGenres) . '</h3>';
    $links = '<div class="band-links">';
    if($band) {
        foreach( $band as $array => $key) {
            switch( $array ) {
                case 'facebook':
                case 'website':
                case 'soundcloud':
                case 'youtube':
                case 'twitter':
                case 'instagram':
                if($band->{$array} != '') {
                    $links .= '<a href="' . $band->{$array} . '" target="_blank"><img src="' . imc_get_plugin_image( $array ) . '" /></a>';
                }
                    break;
                default:

            }
        }
    }
    
    $image = '<div style="text-align: center; padding: 20px;"><img src="'.$band->image.'" /></div>';
    echo $new_html . $genreText . stripslashes($bio) . $links . $image . '</div></article>';
    band_scripts();
}

function band_scripts() {
    wp_enqueue_script( 'imc-bands', plugin_dir_url(__FILE__) . 'js/imc-bands.js', array( 'jquery' ) );
}

function like_scripts() {
    wp_enqueue_script( 'imc-likes', plugin_dir_url(__FILE__) . 'js/imc-likes.js', array( 'jquery' ) );
}


function imc_get_bands() {
    global $wpdb;
    $options = '<option value="" selected="selected">Select A Band or Musician</option>';
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_bands ORDER BY band_name';
    //echo $sql;
    $results = $wpdb->get_results( $sql );
    foreach( $results as $result ) {
        $options .= '<option value="' . $result->id . '">' . $result->band_name . '</option>';
    }
    return $options;
}

function imc_get_band_options() {
    include( 'band-options.php' );
    display_band_sidebar();
}

function imc_edit_band() {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_bands WHERE id = ' . $_POST[ 'band-id' ] . ' LIMIT 1';
    $bands = $wpdb->get_results( $sql );
    foreach($bands as $band ) {
        $modal = '';
        $modal .= '<form id="imc-save-band-form" action="" method="POST">';
        $modal .= wp_nonce_field('create_none_for_wti','wti_like_post_meta_box_nonce');
        $modal .= '<input type="hidden" name="saveband" value="' . $band->id . '" />';
        $modal .= '<h1>Edit A Band or Musician</h1>';
        $modal .= '<div class="bandcol">';
        $modal .= '<label>Band/Musician Name: <input type="text" id="name" name="band_name" value="' . $band->band_name . '" /></label>';
        $modal .= '<label>Website: <input type="text" id="website" name="website"  value="' . $band->website . '" /></label>';
        $modal .= '<label>Soundcloud: <input type="text" id="soundcloud" name="soundcloud" value="' . $band->soundcloud . '" /></label>';
        $modal .= '<label>Facebook: <input type="text" id="facebook" name="facebook" value="' . $band->facebook . '" /></label>';
        $modal .= '</div><div class="bandcol">';
        $modal .= '<label>YouTube: <input type="text" id="youtube" name="youtube"  value="' . $band->youtube . '"/></label>'; 
        $modal .= '<label>Twitter: <input type="text" id="twitter" name="twitter" value="' . $band->twitter . '" /></label>';
        $modal .= '<label>Instagram: <input type="text" id="instagram" name="instagram" value="' . $band->instagram . '" /></label>';
        $modal .= '<label>Band Picture URL/Link <input type="text" id="image" name="image" value="' . $band->image . '" /></label>';
        if(current_user_can('manage_options')) {
            $modal .= '<a style="color:#000;" href="http://napleslivenow.com/wp-admin/media-new.php" target="_blank">Upload an image</a>';
        }
        $modal .= '</div>';
        $modal .= '<div class="band-name-container">';
        $modal .=  imc_get_genres_boxes( $band->genre );
        $modal .= '</div>';
        $modal .= '<label>Bio: <textarea id="bio" name="bio">' . stripslashes($band->bio) . '</textarea><br>';
        $modal .= '<input type="hidden" name="action" value="imc_save_band" />';
        $modal .= '<input id="imc-add-band" type="submit" class="button-primary" value="Save Musician / Band" /> <input id="imc-remove-band" type="submit" class="button-primary" style="background: red;" value="Delete Musician / Band" />';
        $modal .= '<img src="' . admin_url('/images/wpspin_light.gif') . '" class="waiting" id="imc-loading" style="display:none;" /></form>';
        $modal .= '<div id="imc-band-results"></div>';
        $html = $modal;
    }
    echo $html;
    die();
}



function return_meal_types( $values ) {
    $meal_types = '';
    echo 'meal types<br>';
    //var_dump($values);
    foreach( $values as $value => $type ) {
        if( $value != '') {
            if( strlen( $meal_types ) > 0 ) {
                $meal_types .= ',' . $type;
            } else {
                $meal_types .= $type;
            }
        }
    }
    return $meal_types;
}

function imc_lookup_cuisine( $text ) {
    global $wpdb;
    $sql = 'SELECT id FROM ' . $wpdb->prefix . 'imc_venue_cuisine_list WHERE name LIKE "' . $text . '" LIMIT 1';
    $cuisine = $wpdb->get_var( $sql );
    return $cuisine;
}

function imc_lookup_neighborhood( $text ) {
    global $wpdb;
    $sql = 'SELECT id FROM ' . $wpdb->prefix . 'imc_venue_neighborhoods WHERE name LIKE "' . $text . '" LIMIT 1';
    $neighborhood = $wpdb->get_var( $sql );
    return $neighborhood;
}

function imc_lookup_waterfront( $options ) {
    foreach( $options as $key => $value ) {
        if( $value == '1' ) {
            return $key;
        } 
    }
    return '0';
}

function imc_lookup_flags( $flags_array ) {
    $flags = '';
    foreach( $flags_array as $key => $value ) {
        if( $value == 1 ) {
            if( strlen( $flags ) >= 1 ) {
                $flags .= ',' . $key;
            } else {
                $flags .= $key;
            }
        }
    }
    return $flags;
}

function get_text_from_link( $link ) {
    if( strpos( $link, 'href' ) != false )  {
        $start = strpos( $link, '>' ) + 1;
        $end = strpos( $link, '</a>' ) - 1;
        $length = $end - $start;
    }
    return substr( $link, $start, $length );
    
}

function add_comma( $string, $value ) {
    if( strlen( $string ) > 0 ) {
        $value = ",$value";
    }
    return $value;
}

function sendTestMail() {
    wp_mail( 'dev@chooseimpulse.com', 'The subject is not long', 'attachments(string or array) (optional) Files to attach: a single filename, an array of filenames, or a newline-delimited string list of multiple filenames. (advanced)' );
}

function imc_process_textarea() {
    
    $import_type = $_POST[ 'importtype' ];
    $import = $_POST[ 'csv-data' ];
    $import = explode( PHP_EOL, $import );
    //var_dump( $import );
    switch( $import_type ) {
        case 'Venue':
            foreach( $import as $lines ) {
                $fields = explode( ',', $lines );
                $fields = array_map( 'stripslashes_deep', $fields );
                $verfied = trim( $fields[ 0 ] );
                $venue_name = trim( $fields[ 1 ] );
                $venue_address = $fields[ 2 ];
                $venue_address2 = $fields[ 3 ];
                $venue_city = $fields[ 4 ];
                $venue_state = $fields[ 5 ];
                $venue_zip = $fields[ 6 ];
                $venue_phone = $fields[ 7 ];
                $venue_website = $fields[ 8 ];
                $venue_cuisine1 = imc_lookup_cuisine( $fields[ 9 ] );
                $venue_cuisine2 = imc_lookup_cuisine( $fields[ 10 ] );
                $venue_cuisine3 = imc_lookup_cuisine( $fields[ 11 ] );
                $venue_neighborhood  = imc_lookup_neighborhood( $fields[ 12 ] );
                $venue_price_range = strlen( $fields[ 13 ] );
                var_dump($fields[14]);
                var_dump($fields[15]);
                var_dump($fields[16]);
                var_dump($fields[17]);
                $venue_meal_types =  ( $fields[ 14 ] == '' ) ? '' : '1';
                $venue_meal_types .= ( $fields[ 15 ] == '' ) ? '' : add_comma( $venue_meal_types, 2 );
                $venue_meal_types .= ( $fields[ 16 ] == '' ) ? '' : add_comma( $venue_meal_types, 3 );
                $venue_meal_types .= ( $fields[ 17 ] == '' ) ? '' : add_comma( $venue_meal_types, 4 );

                $venue_flags = imc_lookup_flags( array( 
                    1   => $fields[ 18 ],
                    2   => $fields[ 19 ],
                    3   => $fields[ 20 ],
                    4   => $fields[ 21 ],
                    5   => $fields[ 22 ],
                    6   => $fields[ 23 ],
                    8   => $fields[ 24 ],
                    9   => $fields[ 25 ],
                    10  => $fields[ 26 ],
                    12  => $fields[ 27 ],
                    13  => $fields[ 28 ],
                    14  => $fields[ 33 ],
                    15  => $fields[ 34 ]
                ) );
                $outdoor = $fields[ 32 ];
                $waterfront = imc_lookup_waterfront( array( '1' => $fields[ 29 ], '2' => $fields[ 30 ], '3' => $fields[ 31 ] ) );        
                //$venue_flags_string = implode( ',', $venue_flags );
                $import = array( 
                    'name' => $venue_name,
                    'address' => $venue_address,
                    'address2' => $venue_address2,
                    'city' => $venue_city,
                    'state' => $venue_state,
                    'zip' => $venue_zip,
                    'phone' => $venue_phone,
                    'website' => $venue_website,
                    'cuisine1' => $venue_cuisine1,
                    'cuisine2' => $venue_cuisine2,
                    'cuisine3' => $venue_cuisine3,
                    'neighborhood' => $venue_neighborhood,
                    'price_range' => $venue_price_range,
                    'waterfront' => $waterfront,
                    'outdoor' => $outdoor,
                    'meal_types' => $venue_meal_types,
                    'venue_flags' => $venue_flags,
                    'is_active' => 1,
                    'paid_level' => 3,
                    'is_paid' => 1
                );
                $format = array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%s', '%s' );
                $saved_venue = imc_add_venue_to_database( $import, $format );
                if ( $saved_venue ) {
                    $name = $venue_name;
                    $postid = imc_add_venue_page( $name, $saved_venue );
                    imc_add_venue_meta_data( $postid, $saved_venue );
                    imc_add_wp_url_to_venue( $postid );
                    echo $name . ' was saved <br>';
                    var_dump($import);
                }   
            }
            break;
        case 'Musician':
            foreach( $import as $lines ) {
                $fields = explode( ',', $lines );
                $fields = array_map( 'stripslashes_deep', $fields );
                $customer = trim( $fields[ 0 ] );
                $band_name = trim( $fields[ 1 ] );
                $genre_array = array(
                    1   => $fields[ 2 ],
                    2   => $fields[ 3 ],
                    3   => $fields[ 4 ],
                    4   => $fields[ 5 ],
                    5   => $fields[ 6 ],
                    6   => $fields[ 7 ],
                    7   => $fields[ 8 ],
                    8   => $fields[ 9 ],
                    9  => $fields[ 10 ],
                    10  => $fields[ 11 ],
                    11  => $fields[ 12 ],
                    12  => $fields[ 13 ],
                    13  => $fields[ 14 ],
                    14   => $fields[ 15 ],
                    15   => $fields[ 16 ],
                    16   => $fields[ 17 ],
                    17   => $fields[ 18 ],
                    18   => $fields[ 19 ],
                    19   => $fields[ 20 ],
                    20   => $fields[ 21 ],
                    21   => $fields[ 22 ],
                    22  => $fields[ 23 ],
                    23  => $fields[ 24 ],
                    24  => $fields[ 25 ],
                    25  => $fields[ 26 ],
                    26  => $fields[ 27 ],
                    27   => $fields[ 28 ],
                    28   => $fields[ 29 ],
                    29   => $fields[ 30 ],
                    30   => $fields[ 31 ],
                    31   => $fields[ 32 ],
                    32   => $fields[ 33 ],
                    33   => $fields[ 34 ],
                    34   => $fields[ 35 ],
                    35  => $fields[ 36 ],
                    36  => $fields[ 37 ]
                );
                $genre = '';
                foreach($genre_array as $key => $value ) {
                    if( $value == 1) {
                        if( strlen( $genre < 1 ) ) {
                            $genre .= $key;
                        } else {
                            $genre .= ",$key";
                        }
                    }
                }
                $import = array(
                    'band_name' => $band_name,
                    'genre' => $genre
                );
                imc_add_band( $import );
            }
    }
    
}