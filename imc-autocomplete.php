<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function imc_load_autocomplete() {
 
}


function imc_get_venue_names( $action = 'imc_happy_hour_editor', $message = 'happy hour'  ) {
?> 
<div class="wrapper">
    <div id="venue_search">
        <form action="" method="POST" id="imc-venue-autocomplete">
            <span id="hh-autocomplete-spacer">Venue Name:</span><input type="text" name="term" id="venue_name"  autocomplete="off" />
            <input type="hidden" name="action" value="imc_venue_autocomplete" />
        </form>
        <form id="imc-venue-results" action="" method="POST">
                <div id="imc-autocomplete-container">
                    <ul class="imc-autocomplete">
                        
                    </ul>
                </div>
                <input type="hidden" id="venue_id" name="venue_id" value="" />
                
                <input type="hidden" name="action" value="<?php echo $action; ?>" />
            </form>
        <div id="temp">
            <p>Type a venue in above and select from the venues below to edit their <?php echo $message; ?>. </p>
            

        </div>
    </div>
    <div id="venue-results">
        
    </div>
</div>
<?php
}



