<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function imc_display_slider_admin() {

    include( 'imc-autocomplete.php' );
    imc_get_venue_names( 'imc_slider_editor', 'slider' ); 
    wp_enqueue_script( 'imc-edit-slider', plugin_dir_url(__FILE__) . 'js/imc-edit-slider.js', array( 'jquery' ) );
    /* console log awesome updated is loaded  when above js file is loaded */
    
}

function imc_edit_venue_slider( $venue ) {
    $venue_slider = imc_check_venue_slider( $venue->id );
    if( $venue_slider == null ) {
        $venue_slider = imc_get_venue_slider_array( $venue );
    }
    $html = imc_get_venue_slider_edit( $venue_slider ); 
    echo $html;
    die();

}

function imc_get_venue_slider_array( $venue ) {
    $fields = array(
        'id'            => '',
        'venue_id'      => $venue->id,
        'name'          => $venue->name,
        'cuisine'       => $venue->cuisine1,
        'special'       => '',
        'hh_hours'      => '',
        'link'          => $venue->wp_guid,
        'image'         => $venue->image,
        'is_active'     => ''
    );
    return $fields;
}
function imc_check_venue_slider( $id ) {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_slider WHERE venue_id = ' . $id;
    $venue = $wpdb->get_row( $sql );
    return $venue;
} 
    
function imc_get_venue_slider_edit( $venue_slider ) {
    $html = '<div class="slider-row"><form id="edit-slider" action="" method="POST">';
    foreach( $venue_slider as $key => $value ) {
        if( $value == '' ) { $value = 'test'; }
        $html .= imc_get_venue_sider_inputs( $key, $value, $venue_slider );
    }
    $html  .= '<input type="hidden" name="action" value="imc_add_slider" />'
            . '<input type="submit" class="button-primary" id="imc-edit-venue-button" value="Add Venue To Slider" />'
            . '<img src="' . admin_url("/images/wpspin_light.gif") . '" class="waiting" id="imc-loading" style="display:none;" />'
            . '</form></div>';
    return $html;
}

function imc_get_venue_sider_inputs( $key, $value, $venue_slider ) {
    switch( $key ) {
            case 'hh_hours':
            case 'special':
                if( $value == 'test' ) { $value = imc_random_string(); }
                return '<label for "' . $key . '"><span class="label">' . ucwords( str_replace( '_', ' ', $key ) ) . ': </span>'
                    . '<input type="text" name="' . $key . '" id="replace-' . str_replace( '_', '-', $key ) . '" value="' . $value . '" />'
                    . '</label>';
            case 'is_active':
                return '<div id="slider-preview">' . imc_get_venue_slider( $venue_slider, 'span' ) . '</div>'
                    . '<label for "' . $key . '"><span class="label-active">Check this box if you want to add this venue to the slider: </span>'
                    . '<input type="checkbox" name="' . $key . '" id="' . str_replace( '_', '-', $key ) . '" value="' . $value . '" />'
                    . '</label>';
            default:
                if( $value == 'test' ) { $value = 'new'; }
                return '<label for "' . $key . '"><span class="label">' . ucwords( str_replace( '_', ' ', $key ) ) . ': </span>'
                    . '<input type="text" name="' . $key . '" id="' . str_replace( '_', '-', $key ) . '" value="' . $value . '" disabled />'
                    . '</label>';
        }
}

