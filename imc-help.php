<?php
/**
 * Created by PhpStorm.
 * User: nickdecker
 * Date: 10/29/14
 * Time: 9:28 AM
 */



?>

<h1>Help</h1>
<ul>
    <li>Create Venue: Create a new venue</li>
    <li>Venue Importer: Import Venues. This should never need to be used by you, but is more important to setup.</li>
    <li>Venue Slider: Will be removed for live version</li>
    <li>Cuisine Editor:  Add a cuisine to the cuisine list. Cuisines Need to be added before you can import a restaurant with that cuisine, or create a restaurant with the cuisine.</li>
    <li>Neighborhood Editor: Follows the same as above</li>
    <li>Venue Flag Editor: Works the same as cuisine and neighborhood editor</li>
    <li>Happy Hour Editor: Add happy hours to each venue</li>
</ul>

<p>CREATE VENUE:
<p>Fill the form out completely to create a new Restaurant in the system.

<p>VENUE IMPORTER:
<p>Add your spreadsheet pasted in via a CSV file.  SHOULD only be done by Impulse Creative to prevent data loss.

<p>Cuisine, Neighborhood and Venue Flag Editor:
<p>Type in your new properties and submit.  If you have multiple you are adding, use commas in between them.

<p>HAPPY HOUR EDITOR:
<p>Start by typing the name of the restaurant you want to input happy hours for.  The results that match your spelling will show up below.  Keep typing to refine your search, or click the restaurant you are trying to edit their happy hours.

<p>This will display all of the current happy hours you have added for the restaurant.  You can remove current happy hours by clicking Delete Row next to the happy hour row you would like to remove.

<p>You can add Happy Hours below that.  You select the Day or Days of week, start time and end time and the happy hour type.  A Happy hour price and a size in ounces is also needed.  Summary of Happy Hour is for a short listing of maybe the type covered.
<p>EXAMPLES:
<ul>
    <li>1) Two for one Bud and Miller Bottles.</li>
    <li>2) Stella and Heineken on tap</li>
</ul>

<p>Beers should be figured on ounces of Glass / bottle.
<p>Wines should be figured on pour.  Somewhere around 5-6 oz.
<p>Liquors are usually a 2oz pour.

<p>Adpress (The second menu item.  > Available Ads )

<p>This is where you can add ads.
<p>Click Purchase, Upload Image, Upload Files, Select Files.  Then select your file and click Insert into post. Then pick a destination URL.




