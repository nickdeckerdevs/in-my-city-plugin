<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function display_all_venues() {
    global $wpdb;
    $sql = 'SELECT id, name, address, is_active FROM ' . $wpdb->prefix . 'imc_venue ORDER BY name ASC';
    $venues = $wpdb->get_results( $sql );
    display_wrap('top');
    display_venues( $venues );
    display_wrap('bottom');
    
}

function display_wrap( $area ) {
    if( $area == 'top' ) {
        echo '<div class="wrap venues-edit"><ul>';
    } else {
        echo '</ul></div>';
    }
}

function display_venues( $venues ) {
    foreach( $venues as $venue ) {
        $active = $venue->is_active == '0' ? ' style="text-decoration: line-through;" ' : ' ';
        echo '<li><a '. $active .' href="admin.php?page=imc-venue-editor&id=' . $venue->id . ' ">' . $venue->name . ' : ' . $venue->address . '</a></li>';
    }
}