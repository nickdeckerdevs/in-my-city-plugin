<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function imc_get_event_manager() {
    global $wpdb;
    $sql = 'SELECT e.title, e.event_category, e.venue_id, e.event_id, e.start_date, e.end_date, e.event_band, e.approved, p.post_content, p.post_title, p.guid, e.id as Eventid, p.ID as Postid from '.$wpdb->prefix.'imc_events as e INNER JOIN '.$wpdb->prefix.'posts as p on e.event_id = p.ID where e.start_date > DATE_ADD(curdate(), INTERVAL -1 day) ORDER BY e.start_date ASC';
    $events = $wpdb->get_results($sql);
    echo display_events_manager($events);
             
}

function display_events_manager($events) {
//        require_once('kint/Kint.class.php');
    $html = '<style>'
            .'.edit-event, .delete-event { cursor: pointer; }'
            .'</style>';
    $html .= '<div class="wrap"><table><tr><td></td><td>Date</td><td>Title</td>Approved<td>Delete</td></tr>';
    
    foreach($events as $event) {
//        ddd($event);
        $html .= '<tr id="'.$event->Eventid.'">';
        $html .= '<td>'.$event->start_date.'</td>';
        $html .= '<td><a target="_blank" href="'.$event->guid.'">'.$event->title.'<a/></td>';
        $html .= '<td>'.$event->approved = 1 ? 'yes' : '---'.'</td>';
        $html .= '<td><span class="delete-event" data-eventid="'.$event->Eventid.'" data-postid="'.$event->Postid.'">Delete</span></td>';
        $html .= '</tr>';
    }
    $html .= '</table></div>';
    $html .= '<form id="delete_events_form" action="" method="POST">';
    $html .= '<input type="hidden" name="action" value="imc_delete_event" />';
    $html .= '<input type="hidden" id="delete_event" name="eventid" value="" />';
    $html .= '<input type="hidden" id="delete_post" name="postid" value="" />';
    $html .= '<input type="submit" id="deleteevent" name="deleteevent" value="" class="hidden" />';
    $html .= '</form>';

    return $html;
    
    
    
}


