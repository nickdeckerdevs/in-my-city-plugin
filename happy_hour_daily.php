<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function display_top_10() {
    global $wpdb;
    $sql = 'SELECT 
        top.venue_id, top.points, 
        venue.id, venue.name, venue.wp_guid 
            FROM ' . $wpdb->prefix . 'imc_rated_venues as top
            INNER JOIN ' . $wpdb->prefix . 'imc_venue AS venue
                ON top.venue_id = venue.id ORDER BY top.points DESC LIMIT 10';
    $top_ten = $wpdb->get_results( $sql );
    return $top_ten;
}
function fix_pricing( $day ) {
    global $wpdb;
    $ppo_fix_sql = 'SELECT id, venue_id, price, ounces, price_per_oz FROM ' . $wpdb->prefix . 'imc_happy_hour where day = ' . $day;
    $ppo_fix = $wpdb->get_results( $ppo_fix_sql );
    $ppo_fix_final = fix_ppo_numbers( $ppo_fix );
}

function get_name( $id ) {
    global $wpdb;
    $venue_name_sql = 'SELECT name FROM ' . $wpdb->prefix . 'imc_venue WHERE id = ' . $id;
    $venue_name = $wpdb->get_var( $venue_name_sql, 0, 0 );
    return $venue_name; 
}

function calculate_scores() {
    define('WP_USE_THEMES', true);
    
    /* local server */
//    require_once('kint/Kint.class.php');
//    require_once('/Applications/XAMPP/htdocs/napleslivenow/wp-blog-header.php'); 
    
//    /* live server */
    require_once('/home/napleslivenow/public_html/wp-blog-header.php'); 
    update_all_happyhours();
    $day_of_week = date( 'w', mktime() );
//    $day_of_week = 6;
//    ddd($day_of_week);
    fix_pricing( $day_of_week );
    global $wpdb;
    $reset_rating = $wpdb->query( "TRUNCATE TABLE {$wpdb->prefix}imc_rated_venues" );
    $venue_sql = 'SELECT id from ' . $wpdb->prefix . 'imc_venue where is_active = 1';
    $venues = $wpdb->get_results( $venue_sql );
    foreach( $venues as $venue ) {
        $hh_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_happy_hour WHERE happy_hour_type in ("1", "2", "3", "4", "5", "7") AND venue_id = ' . $venue->id . ' AND day = ' . $day_of_week;
        $happy_hours = $wpdb->get_results( $hh_sql );
        $total_hhs = count($happy_hours);
        $total_hours = 0;
        $latest_hour = 0;
        if( !is_null( $total_hhs ) && $total_hhs > 0 ) {
            foreach( $happy_hours as $hh ) {
                $fix = $hh->time_end_fix < '08:00:00' ? true : false;
                if( $fix == true ) {
                    /* This isn't being used at the moment because he said whatever */
//                    $add_time = $hh->time_end_fix < '01:01:00' ? '+1 hours' : '+2 hours'; 
                    $total_hours = $total_hours + ( strtotime( '24:00:00' ) - strtotime( $hh->time_start ) );
                } else {
                    $total_hours = $total_hours + ( strtotime( $hh->time_end_fix ) - strtotime( $hh->time_start ) );
                }
                $ppo[ $hh->happy_hour_type ] = $hh->price_per_oz == 0 ? 100 : $hh->price_per_oz;
                
                    
                
                $latest_hour = $latest_hour < $hh->time_end_fix ? $hh->time_end_fix : $latest_hour;
            }
            $ppo[4] = is_null( $ppo[4] ) ? 100 : $ppo[4];
            $late_night_bonus = set_late_night_bonus( $venue->id, $latest_hour );
            $average_hh_hours = get_hours( $total_hours ) / $total_hhs;
            $saved = save_venue_bonus_points( $hh->venue_id, $average_hh_hours, $ppo );
            if( $saved ) {
                unset( $ppo );
            } else {
                unset( $ppo );
            }
        }
    }
    $types = [ 'd_bottle', 'd_draft', 'i_bottle', 'i_draft', 'wine', 'well' ];
    foreach( $types as $type ) {
        $rating_sql = 'SELECT venue_id, ah, lnb, points, ' . $type . ' FROM ' . $wpdb->prefix . 'imc_rated_venues ORDER BY ' . $type . ' ASC';
        $ratings = $wpdb->get_results( $rating_sql );
        $key_count = 0;
        $points = [ 0, 16, 12, 10, 7, 6, 5, 4, 3, 2, 1 ];
        $previous_price = 0;
        $skipped = 0;
        $place = 0;
        foreach( $ratings as $venue ) {
            /* This was removed because it didn't really do that stuff like I wanted it to do aka skip if there isn't enough in the thing */
//            if($previous_price = 0) continue;
            $price = $venue->{$type};
//            echo $previous_price . ' vs. price now: '.$price."\r\n";
            if( $previous_price == $price) {
                $skipped++;
            } else {
                if($skipped > 0) { 
                    $key_count = $key_count + $skipped + 1;
                    $skipped = 0;
                } else {
                    $key_count++;
                }
            }
//            if($type == 'well') {
//                d($venue);
//                dd();
//                $name = get_name($venue->venue_id);
//                echo $name.': well ['.$venue->well.'] price per oz'."\r\n";
//                echo 'key:'.$key_count . ' skipped:'.$skipped."\r\n";
//                echo 'place: '.$place."\r\n";
//            }
            $place++;
            $venue_points = is_null( $points[ $key_count ] ) ? 0 : $points[ $key_count ];
            save_points_for_venue( $venue->venue_id, $type, $venue_points );
            $previous_price = $price;
        }
    }
    
    $all_ln_updated = update_late_night_bonus();
    
    $rated_venue_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_rated_venues';
    $rated_venues = $wpdb->get_results( $rated_venue_sql );
    
    foreach( $rated_venues as $venue ) {
        $total = ( $venue->d_bottle + $venue->d_draft + $venue->i_bottle + $venue->i_draft + $venue->wine + $venue->well + $venue->lnb ) * $venue->ah;
        update_total_points( $venue->venue_id, $total );
    }
}

function update_late_night_bonus( ) {
    $key_count = 0;
    $points = [ 0, 16, 12, 10, 7, 6, 5, 4, 3, 2, 1 ];
    $previous_time = 0;
    $skipped = 0;
    $place = 0;
    global $wpdb;
    $lnb_venue_sql = 'SELECT venue_id, lnb FROM ' . $wpdb->prefix . 'imc_rated_venues ORDER BY lnb DESC';
    $lnb_venues = $wpdb->get_results( $lnb_venue_sql );
    foreach( $lnb_venues as $venue ) {
        $time = $venue->lnb;
        if( $previous_time == $time) {
            $skipped++;
        } else {
            if( $skipped > 0 ) { 
                $key_count = $key_count + $skipped + 1;
                $skipped = 0;
            } else {
                $key_count++;
            }
        }
        $place++;
        $venue_points = is_null( $points[ $key_count ] ) ? 0 : $points[ $key_count ];
        save_lnb_for_venue( $venue->venue_id, $venue_points );
        $previous_time = $time;
        
    }
}

function save_lnb_for_venue( $venue_id, $points ) {
    global $wpdb;
    $sql = "INSERT INTO {$wpdb->prefix}imc_rated_venues 
        ( venue_id, lnb ) 
            VALUES 
            ( %d, %d ) ON DUPLICATE KEY UPDATE lnb = %d";
    $sql_prepare = $wpdb->prepare( $sql, $venue_id, $points, $points );
    $update = $wpdb->query( $sql_prepare );
    return $update;
}

function set_late_night_bonus( $venue_id, $time ) {
    $adjusted_time = strtotime( $time );
    global $wpdb;
    $sql = "INSERT INTO {$wpdb->prefix}imc_rated_venues 
        ( venue_id, lnb ) 
            VALUES 
            ( %d, %d ) ON DUPLICATE KEY UPDATE lnb = %d";
    $sql_prepare = $wpdb->prepare( $sql, $venue_id, $adjusted_time, $adjusted_time );
    $update = $wpdb->query( $sql_prepare );
    return $update;        
}

function update_total_points($id, $points) {
    global $wpdb;
    
    $updated = $wpdb->update(
        $wpdb->prefix . 'imc_rated_venues',
            array(
                'points' => $points
            ),
            array( 'venue_id' => $id ),
            array( '%d' )
            );
        
//    echo "UPDATE IS ::::".$updated."\r\n";
    return $update;
}

function save_points_for_venue( $venue_id, $column, $points  ) {
    global $wpdb;
    $sql = "INSERT INTO {$wpdb->prefix}imc_rated_venues 
        ( venue_id, $column ) 
            VALUES 
            ( %d, %f ) ON DUPLICATE KEY UPDATE  $column = %f";
    $sql_prepare = $wpdb->prepare( $sql, $venue_id, $points, $points );
    $update = $wpdb->query( $sql_prepare );
    
//    echo $sql."\r\n";
//    echo "UPDATE IS ::::".$update."\r\n";
    return $update;
}

function save_venue_bonus_points( $id, $ah, $ppo ) {
    global $wpdb;
    $total = $ah + $lnb;
    $sql = "INSERT INTO {$wpdb->prefix}imc_rated_venues (
        venue_id, d_bottle, d_draft, i_bottle, i_draft, wine, well, ah, points
    ) VALUES (
        %d, %f, %f, %f, %f, %f, %f, %d, %d
    ) ON DUPLICATE KEY UPDATE 
        d_bottle = %f, d_draft = %f, i_bottle = %f, i_draft = %f, wine = %f, well = %f, ah = %d, points = %d";
    $sql = $wpdb -> prepare( 
        $sql, 
            $id, $ppo[ 1 ], $ppo[ 2 ], $ppo[ 3 ], $ppo[ 4 ], $ppo[ 5 ], $ppo[ 7 ], $ah, $total, 
            $ppo[ 1 ], $ppo[ 2 ], $ppo[ 3 ], $ppo[ 4 ], $ppo[ 5 ], $ppo[ 7 ], $ah, $total
    );
    $update = $wpdb->query( $sql );
    return $update;
}

function update_all_happyhours() {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_happy_hour';
    
    $results = $wpdb->get_results($sql);
    foreach($results as $hh) {
        if( strtotime( $hh->time_end_fix ) < strtotime( '06:00:00') ||  $hh->time_end_fix == '23:59:59' ) {
            $end_time_fix = '24:00:00';
     
            $hh_update = $wpdb->update( 
                    $wpdb->prefix . 'imc_happy_hour',
                    array( 'time_end_fix' => $end_time_fix ),
                    array( 'id' => $hh->id ),
                    array( '%s' )
            );
        }
        
    }
}

function fix_ppo_numbers( $hhs ) {
    foreach($hhs as $hh) {
        $ppo = $hh->price / $hh->ounces;
        global $wpdb;
        $hh_update = $wpdb->update( 
                $wpdb->prefix . 'imc_happy_hour',
                array( 'price_per_oz' => $ppo ),
                array( 'id' => $hh->id ),
                array( '%f' )
        );
    }
    return $hhs;
}

function return_day( $value ) {
    switch( $value ) {
        case 0:
            return 'Sunday';
        case 1:
            return 'Monday';
        case 2:
            return 'Tuesday';
        case 3:
            return 'Wednesday';
        case 4:
            return 'Thursday';
        case 5:
            return 'Friday';
        case 6:
            return 'Saturday';
        case 7: 
            return 'Sunday';
    }
}

function check_for_duplicate_happyhours( $objects ) {
    $csv = '';
    foreach( $objects as $object ) {
        $csv .= $object->venue_id . ', ' . return_day( $object->day ) . ', ' . $object->happy_hour_type . ',<br>';
    }
}



function figure_out_points( $late_night_hhs, $steps ) {
    $late_time = strtotime( '18:00:00' ); 
    $venues = [];
    foreach( $late_night_hhs as $current_hh ) {
//        d($current_hh);
        //check to see if the venue is already an object to be added to, if not, create object and add to array of objects
        $venue = $current_hh->venue_id;
        if( is_object( ${$venue} ) == false ) {
            ${$venue} = new stdClass();
            array_push( $venues, ${$venue} );
        }
        
        $total_time = strtotime( $current_hh->time_end_fix ) - strtotime( $current_hh->time_start );
        ${$venue}->id = $current_hh->venue_id;
        ${$venue}->day[$current_hh->day]->name = return_day( $current_hh->day );
        ${$venue}->day[$current_hh->day]->hh[$current_hh->happy_hour_type] = array(
            'type'      => $current_hh->happy_hour_type,
            'time'      => $total_time,
            'ppo'       => $current_hh->price_per_oz,
            'time_start'=> $current_hh->time_start,
            'time_end'  => $current_hh->time_end_fix
        );
          
        
        $hh_end = strtotime( $current_hh->time_end );
        $points = 0;

    }
    foreach( $venues as $venue ) {
        $id = $venue->id;
        $latest_hour = '00:00:00';
        $time = 0;
        $ppo_array = array( 
            1 => 100,
            2 => 100,
            3 => 100,
            4 => 100,
            5 => 100,
            7 => 100
        );
        foreach( $venue as $property ) {
            if( is_array( $property ) ) {
                //This is the days array by number
                foreach( $property as $object ) {
                    //contains day name and the -> hh object that contains data
                    foreach( $object->hh as $key ) {
                        $ppo_array[ $key[ 'type' ] ] = check_ppo( floatval( $ppo_array[ $key[ 'type' ] ] ), floatval( $key[ 'ppo' ] ) );
                        if( $key[ 'type' ] == 5 ) {
                            var_dump($key);
                            $ppo_array[ $key[ 'type' ] ] = check_ppo( floatval( $ppo_array[ $key[ 'type' ] ] ), floatval( $key[ 'ppo' ] ) );
                            $time = $time + $key[ 'time' ];
                            $latest_hour = check_late_night( $latest_hour, $key[ 'time_end' ] );
                        }
                        
                        
                    } 
                }
            }
        }
        $ah = get_hours( $time );
//        d($ah);
        $lnb = get_late_night_bonus( $latest_hour, $steps );
        $points = get_ppo_points( $ppo_array );
        $updated = submit_points( $id, $ah, $lnb, $points );   
    }
}

function submit_points( $id, $ah, $lnb, $points ) {
    $total = get_total_ranked_points( $ah, $lnb, $points );
    global $wpdb;
    $sql = "INSERT INTO {$wpdb->prefix}imc_rated_venues (
        venue_id, d_bottle, d_draft, i_bottle, i_draft, wine, well, ah, lnb, points
    ) VALUES (
        %d, %d, %d, %d, %d, %d, %d, %d, %d, %d
    ) ON DUPLICATE KEY UPDATE 
        d_bottle = %d, d_draft = %d, i_bottle = %d, i_draft = %d, wine = %d, well = %d, ah = %d, lnb = %d, points = %d";
    $sql = $wpdb -> prepare( 
        $sql, 
            $id, $points[ 1 ], $points[ 2 ], $points[ 3 ], $points[ 4 ], $points[ 5 ], $points[ 7 ], $ah, $lnb, $total, 
            $points[ 1 ], $points[ 2 ], $points[ 3 ], $points[ 4 ], $points[ 5 ], $points[ 7 ], $ah, $lnb, $total
    );
    $update = $wpdb->query( $sql );
}

function get_total_ranked_points( $ah, $lnb, $points ) {
    $total = 0;
    foreach( $points as $point ) {
        $total = $total + $point;
    }
    $total = $total + $lnb;
    $total = $total * $ah;
    return $total;
}

function get_rating_array( $id, $ah, $lnb, $points ) {
    $rating = array();
    $rating[ 'id' ] = $id;
    $rating[ 'ah' ] = $ah;
    $rating[ 'lnb' ] = $lnb;
    foreach( $points as $key=>$value ) {
        $rating[ $key ] = $value;
    }
    return $rating;
}

function get_ppo_points( $ppo_array ) {
    $steps = array(
        1 => array( 0.3750, 0.3547, 0.3344, 0.3141, 0.2938, 0.2734, 0.2531, 0.2328, 0.2125, 0.1922, 0.1719, 0.1516, 0.1313, 0.1109, 0.0906, 0.0703, 0.0500 ),
        2 => array( 0.5000, 0.4746, 0.4492, 0.4238, 0.3984, 0.3730, 0.3477, 0.3223, 0.2969, 0.2715, 0.2461, 0.2207, 0.1953, 0.1699, 0.1445, 0.1191, 0.0938 ),
        3 => array( 0.3750, 0.3568, 0.3385, 0.3203, 0.3021, 0.2839, 0.2656, 0.2474, 0.2292, 0.2109, 0.1927, 0.1745, 0.1563, 0.1380, 0.1198, 0.1016, 0.0833 ),
        4 => array( 0.6667, 0.6302, 0.5938, 0.5573, 0.5208, 0.4844, 0.4479, 0.4115, 0.3750, 0.3385, 0.3021, 0.2656, 0.2292, 0.1927, 0.1563, 0.1198, 0.0833 ),
        5 => array( 1.0000, 0.9625, 0.9250, 0.8875, 0.8500, 0.8125, 0.7750, 0.7375, 0.7000, 0.6625, 0.6250, 0.5875, 0.5500, 0.5125, 0.4750, 0.4375, 0.4000 ),
        7 => array( 3.0000, 2.9063, 2.8125, 2.7188, 2.6250, 2.5313, 2.4375, 2.3438, 2.2500, 2.1563, 2.0625, 1.9688, 1.8750, 1.7813, 1.6875, 1.5938, 1.5000 )
    );
    $points = array(
        1 => get_points( $ppo_array[ 1 ], $steps[ 1 ] ),
        2 => get_points( $ppo_array[ 2 ], $steps[ 2 ] ),
        3 => get_points( $ppo_array[ 3 ], $steps[ 3 ] ),
        4 => get_points( $ppo_array[ 4 ], $steps[ 4 ] ),
        5 => get_points( $ppo_array[ 5 ], $steps[ 5 ] ),
        7 => get_points( $ppo_array[ 7 ], $steps[ 7 ] )
    );
    return $points;
}

function get_points( $ppo, $steps ) {
    $points = 0;
    if( $ppo == 100 ) {
        return 0;
    }
    for( $i = 0; $i <= 16; $i++ ) {
        $stepppo = $steps[ $i ];
        if( $ppo >= $stepppo ) {
            $points = $i;
            break 1;
        } else {
            $points = $i;
        }
    }
    return $points;
}
function check_ppo( $current, $new ) {
    if( $new < $current ) {
        return $new;
    } else {
        return $current;
    }
}


function get_late_night_bonus( $time, $steps ) {
    
    $points = 0;
    $seconds = strtotime( $time );
    for( $i = 0; $i <= 16; $i++ ) {
        $stepseconds = strtotime( $steps[ $i ] );
        if( $seconds <= $stepseconds ) {
            
            $points = $i;
            
            break 1;
        } else {
            $points = $i;
    
        }
    }
        
    return $points;

    
    
}

function check_late_night( $current, $new ) {
    $current_unix = strtotime( $current );
    $new_unix = strtotime( $new );
    if( $current_unix <= $new_unix ) {
        return $new;
    } else {
        return $current;
    }
            
}


function get_hours( $seconds ) {
    $minutes = $seconds / 60;
    $hours = $minutes / 60;
    return $hours;
}

