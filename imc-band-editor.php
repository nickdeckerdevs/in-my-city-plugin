<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function display_bands_admin() { 
    global $wpdb;
    $sql = 'SELECT id, band_name FROM ' . $wpdb->prefix . 'imc_bands ORDER BY band_name ASC';
    $bands = $wpdb->get_results( $sql );
    $band_html = display_bands_admin_html( $bands );
    echo $band_html;
}

function display_bands_admin_html( $bands ) {
    $html = '<div class="wrap band-admin"><div class="edit-band-admin-details"></div><ul class="band-name-container">';
    foreach( $bands as $band ) {
        $html .= '<li class="edit-band" id="' . $band->id . '">' . $band->band_name . '</li>';
    }
    $html .= '</ul></div>';
    $html .= get_band_editor_form();
    return $html;
}

function get_band_editor_form() {
    $form = '<form id="edit-band-form" method="POST" action="">';
    $form .= '<input type="hidden" name="band-id" id="band-id" />';
    $form .= '<input type="hidden" name="action" value="imc_edit_band" />';
    $form .= '</form>';
    return $form;
}

