<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function display_unapproved( $type ) {
    switch( $type ) {
        case 'events':
            $html = get_unapproved_events();
            break;
        case 'bands':
            $html = get_unapproved_bands();
            break;
    }
    return $html;
}

function get_unapproved_events() {
    global $wpdb;
    $sql = 'SELECT m.post_id, m.meta_key, m.meta_value, e.id, e.title, e.event_category, e.start_date, e.end_date, e.event_band, p.post_content, p.guid from ' . $wpdb->prefix . 'imc_events as e INNER JOIN ' . $wpdb->prefix . 'posts as p on e.event_id = p.id INNER JOIN ' . $wpdb->prefix . 'postmeta as m on m.post_id = p.id WHERE e.approved = 0 AND m.meta_key = "_EventPhone"';
    $events = $wpdb->get_results( $sql );
    $unapproved = display_approval_html( $events, 'events', 'imc_approve_events_ajax' );
    return $unapproved;
}
//    require_once('kint/Kint.class.php');
function get_unapproved_bands() {
    global $wpdb;
//    $sql = 'SELECT id, band_name, image, bio from ' . $wpdb->prefix . 'imc_bands WHERE approved = 0';
    $sql = 'SELECT * from ' . $wpdb->prefix . 'imc_bands WHERE approved = 0';
    $bands = $wpdb->get_results( $sql );
    $unapproved = display_approval_html( $bands, 'bands', 'imc_approve_bands_ajax' );
    return $unapproved;
}

function display_approval_html( $results, $type, $action ) {
    $form_start = '<form id="approve-' . $type . '" method="POST" action="" />';
    $table = '<table class="approvals" cellpadding="4" cellspacing="5">';
    $keys = '';
    $data = '';
    $keycount = 0;
    foreach( $results as $object ) {
        $keys .= '<tr class="key">';
        $data .= '<tr id="' . $object->id . '">';
        foreach( $object as $key => $value ) {
            if($key == 'meta_key') continue;
            if($key == 'meta_value') $key = 'contact';
            $keys .= '<td>' . $key . '</td>';
            
            if( $key == 'event_category' ) {
                switch( $value ) {
                    case 3:
                        $value = 'Farmers Markets';
                        break;
                    case 4:
                        $value = 'Festivals & Fairs';
                        break;
                    case 5:
                        $value = 'Fine Arts';
                        break;
                    case 6:
                        $value = 'Fund Raisers';
                        break;
                    case 1:
                        $value = 'Live Music';
                        break;
                    case 0:
                        $value = 'Theatre';
                        break;
                    default:
                        $value = 'Other';
                        break;
                        
                }
            } else {
                if($value == '_EventPhone') continue;
                $value = strpos($value, 'http') > -1 ? '<a href="' . $value . '" target="_blank">Link will open in new window</a>' : $value;
            }
            
            $data .= '<td>' . $value . '</td>';
        }
        $data .= '<td class="approve-item" style="cursor:pointer; text-align:center">+ Approve</td><td class="delete-item" style="cursor:pointer; text-align:center">- DELETE</td></tr>';
        $keys .= '<td>Approve</td><td>Delete</td></tr>ENDTHISKEY';
    }
    
    $key = explode( 'ENDTHISKEY', $keys );
    $end_table = '</table><br><div class="center">';
    $end_table .= '<input type="button" class="button-primary" name="approve" id="approve-all" value="Approve All ' . ucwords( $type ) . '" />';
    $end_table .= '</div>';
    $end_table .= '<input type="hidden" name="action" value="' . $action . '" />';
    $end_table .= '<input type="hidden" name="approve" id="approve-id" value="" /></form>';
    $built_table = $form_start . $table . $key[ 0 ] . $data . $end_table;
    return $built_table;
}