<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $wpdb;
$sql = 'SELECT * FROM wp_imc_venue_cuisine_list';
$cuisines = $wpdb->get_results($sql);
?>
<div class="wrap">
    <h2>Add New Cuisine</h2>
    <form method="post" action="" id="imc-add-cuisine"> <!-- action="admin-post.php" -->
    <label for="cuisine">
        <input type="text" id="cuisine" name="cuisine" placeholder="type cuisines here" />
        <input type="hidden" name="cuisine_hidden" value="Y" />
        <input type="submit" class="button-primary" id="imc-submit-cuisine" name="submit_ajax">
        <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" class="waiting" id="imc-loading" style="display:none;" />
        <p class="infoText">Place a comma between cuisines if adding multiple cuisines.</p>

    </label>
        <div><span id="feedback"></span></div>
    </form>
    
    <h2>Current Cuisine Types</h2>

    <ul class="typeList" id="imc-cuisine-results">
        <?php
        foreach($cuisines as $cuisine) {
            echo '<li id="'.$cuisine->id.'">'.$cuisine->name.'</li>';
        } ?>
    </ul>

    
</div>