<?php
/**
 * Created by PhpStorm.
 * User: nickdecker
 * Date: 10/23/14
 * Time: 10:53 AM
 */



function display_image_upload( $count = 1 ) {
	for( $i = 1; $i <= $count; $i++ ) {
		echo "<tr><th><label><b>Venue Image Upload $i</b></label></th>";
		echo "<td><input id='imc_image_url$i' type='text' name='ad_image$i' class='imc_image_venue_profile' value='http://' size='50' />";
		echo "<input id='imc_image_url_button$i' class='imc_image_url_button button button-primary' type='button' value='Select Image' />";
		echo "<br><img id='imc_image_preview$i' class='image-preview' /></td></tr>";
	}
}

function display_logo_upload() {
	echo "<tr><th><label><b>Logo Image Upload</b></label></th>";
	echo "<td><input name='logo_image' id='logo_image' type='text' class='imc_logo_image' value='http://' size='50' />";
	echo "<input id='imc_logo_button' class='imc_logo_button imc_image_url_button button button-primary' type='button' value='Select Logo' />";
	echo "<br><img id='imc_logo_preview' class='logo-preview' /></td></tr>";
}
function display_cuisine_options( $count = 3 ) {
	global $wpdb;
	$cuisine_sql = "SELECT * FROM " . $wpdb->prefix . "imc_venue_cuisine_list ORDER BY name ASC";
	$cuisines = $wpdb->get_results( $cuisine_sql );
	for( $i = 1; $i <= $count; $i++ ) {
		echo "<tr class='no-main'><th><label>Cuisine Type $i</label></th>";
		echo "<td><select id='cuisine" . $i . "_select'><option>Select A Cuisine Type</option>";
        foreach($cuisines as $cuisine) {
            if ( $cuisine->name !== '' ) {
                echo '<option value="'.$cuisine->id.'">'.$cuisine->name.'</option>';
            }
        }
		echo '</select></td></tr>';
	}
}

function display_price_range() {
	echo "<tr class='no-main'><th><label>Price Range</label></th>";
	echo '<td><select id="price_range_select"><option>Select A Price Range</option>';
	for($x = 1; $x <=4; $x++) {
		switch ( $x ) {
			case 4:
				$price_range = '$$$$';
				break;
			case 3:
				$price_range = '$$$';
				break;
			case 2:
				$price_range = '$$';
				break;
			case 1:
				$price_range = '$';
				break;
			default:
				$price_range = '';
		}
		echo '<option value="' . $x . '">' . $price_range . '</option>';
	}
	echo '</td></tr>';
}

function display_dropdown( $label, $id, $options ) {
	echo "<tr class='no-main'><th><label>$label</label></th>";
	echo "<td><select id='$id'><option>Select One</option>";
	foreach( $options as $key => $value ) {
		echo "<option value='$key'>$value</option>";
	}
	echo '</td></tr>';
}

function display_flags() {
	global $wpdb;
	$table = $wpdb->prefix.'imc_venue_flag_list';
	$flags = $wpdb->get_results( 'SELECT * FROM ' . $table );
	echo '<tr class="no-main"><th><label>Venue Options</label></th>';
	echo '<td>';
	foreach($flags as $flag) {
		if( $flag->id != 11 ) {
			echo '<label><input type="checkbox" class="venue_flags_box" name="venue_flags_select" value="' . $flag->id . '" />' . $flag->name . '</label><br>';
		}
	}
	echo '</td></tr>';
}

function display_checkboxes( $label, $id, $options) {
	echo "<tr class='no-main'><th><label>$label</label></th>";
	echo '<td>';
        //var_dump($options);
	foreach($options as $key => $value) {
		echo '<label><input type="checkbox" class ="' . $id . '" name="' . $id . '" value="'. $key .'" />'. $value .'</label><br>';
	}
	echo '</td></tr>';
}

function imc_display_profile_plus( $images = 1, $cuisines = 3 ) {
	display_logo_upload();
	display_image_upload( $images );
	display_cuisine_options( $cuisines );
	display_price_range();
	display_dropdown( 'Outdoor Seating', 'outdoor_select', array( 0 => 'No', 1 => 'Yes' ) );
	display_dropdown( 'Waterfront', 'waterfront_select', array( 0 => 'None', 1 => 'Beach Front', 2 => 'River Front', 3 => 'Bay Front' ) );
	display_dropdown( 'Active Listing', 'is_active_select', array( 1 => 'Yes', 0 => 'No' ) );
	display_flags();
	display_checkboxes( 'Open For', 'meal_types_box', array( 1 => 'Breakfast', 2 => 'Brunch', 3 => 'Lunch', 4 => 'Dinner' ));
}

function displayAll() {

    if( !current_user_can( 'upload_files' ) ) {
            $user = new WP_USER( $user_id );
            imc_allow_uploads_for_user( $user );
            if( !current_user_can( 'upload_files' ) ) {
                    echo "can't upload like a bitch";
            } else {
                    echo 'can upload now';
            }
            } else {
    ?>

    <div id="profile-extended">
            <table class="form-table">
                    <?php imc_display_profile_plus( 5, 3 ); ?>
            </table>

    </div>

    <?php }
}