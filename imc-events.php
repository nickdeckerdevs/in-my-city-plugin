<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function imc_get_date_image( $day_or_month ) {
    $date_array = explode( ' ', date( 'n j' ) );
    $image_type = $date_array[ 1 ];
    if( $day_or_month == 'month' ) {
        $image_type = $date_array[ 0 ];
    }
    return imc_get_event_date_image( $image_type, $day_or_month );
}

function imc_get_event_date_image( $file_name, $folder ) {
    return imc_get_plugin_image( 'date/' . $folder . '/' . $file_name );
}

function imc_get_featured_event_id() {
    //random or something
    return 107;
}

function imc_get_event_summary() {
?>
<div id="events-summary">
    <div id="events-totals" class="fLeft">
        <div id="events-week">
            
        </div>
        <div id="events-middle">
            <h4 class="orange-text small-title fRight">This Week's Total</h4>
            <?php //imc_get_this_weeks_events_total(); ?>
        </div>
        <div id="events-right">
            <h4 class="small-title">Added this week</h4>
            <?php //imc_get_this_weeks_events_breakdown(); ?>
        </div>
    </div>
</div> <?php
}
function imc_get_featured_event( $event_id ) { 
    $event_id = get_the_ID();
    $skeleton_mode = apply_filters( 'tribe_events_single_event_the_meta_skeleton', false, $event_id ) ;
    $group_venue = apply_filters( 'tribe_events_single_event_the_meta_group_venue', false, $event_id );
    
    $featured_image = tribe_event_featured_image();
?>
    <div id="featured-event">
        
        <div class="column">
            <div id="todays-date">
                <?php  
                
                ?>
                
            </div>
                <?php the_title( '<h2 class="small-title orange-text">', '</h2>' ); ?>
                <?php echo tribe_events_event_schedule_details( $event_id, '<h3>', '</h3>'); ?>
		<?php  if ( tribe_get_cost() ) :  ?>
			<span class="tribe-events-cost">Event Cost: <?php echo tribe_get_cost( null, true ) ?></span>
		<?php endif; ?>
                <?php $current_post = get_post( $event_id ) ?>
                
                
                        
            <p><?php echo $current_post->post_content; ?></p>
            
            <p><?php tribe_get_meta_group( 'tribe_event_details' ); ?></p>
            
        </div>
        <div class="column">
            <?php 
            if( $featured_image != null ) {
                echo $featured_image;
                echo $image_exists = true;
            }
            
            echo tribe_get_meta( 'tribe_event_organizer_name' );
            echo tribe_get_meta( 'tribe_event_venue_name' );
            echo tribe_get_meta( 'tribe_event_venue_phone' );
            $organizer_phone_div = explode( '</label>', tribe_get_meta( 'tribe_event_organizer_phone' ) );
            if( count( $organizer_phone_div ) > 1 ) {
                $phone = substr( $organizer_phone_div[ 1 ], 0, -6 );
            } else {
                $phone = '';
            }
            echo $phone;
            echo tribe_get_meta( 'tribe_event_organizer_website' ); 
            echo tribe_get_meta( 'tribe_event_venue_address' );
            echo tribe_get_meta( 'tribe_venue_map' ); ?>
        </div>
    </div>
<?php
}



function imc_tribe_get_meta_group( $meta_group_id, $is_the_meta = false ){

		do_action('tribe_get_meta_group', $meta_group_id, $is_the_meta );

		$type = 'meta_group';

		// die silently if the requested meta group is not registered
		if( ! Tribe_Meta_Factory::check_exists( $meta_group_id, $type ) )
			return false;

		$meta_group = Tribe_Meta_Factory::get_args( $meta_group_id, $type );
		$meta_ids = Tribe_Meta_Factory::get_order( $meta_group_id );
		$group_html = '';

		// internal check for hiding items in the meta
		if( ! $meta_group['show_on_meta'] ){
			return false;
		}
		$meta_pos_int = 0;
		$total_meta_items = tribe_count_hierarchical( $meta_ids );
		foreach( $meta_ids as $meta_id_group ) {
			foreach( $meta_id_group as $meta_id ){
				$meta_pos_int++;

				// if we should have a meta divider let's add it!
				if( !empty($group_html) && $meta_pos_int <= $total_meta_items ) {
					$group_html .= $meta_group['wrap']['meta_separator'];
				}
                                $group_html .= '<br>id: ' . $meta_id . '  meta: -' . $is_the_meta . '-<br>';
				//if( $meta_id ==)
                                    $group_html .= tribe_get_meta( $meta_id, $is_the_meta );

			}
		}

		$params = array( $meta_group_id );

		if( !empty($meta['filter_callback']) ){
			return call_user_func_array($meta['filter_callback'], $params);
		}

		if( !empty($meta['callback']) ){
			$value = call_user_func_array($meta['callback'], $params);
		}

		$value = empty($value) ? $group_html : $value;

		$html = !empty($group_html) ? Tribe_Meta_Factory::template( $meta_group['label'], $value, $meta_group_id, 'meta_group' ) : '';

		return apply_filters('tribe_get_meta_group', $html, $meta_group_id );
	}