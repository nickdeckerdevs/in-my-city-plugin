<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $wpdb;

function imc_get_query_piece ( $imc_sort_key, $imc_sort_value ) {
    switch ( $imc_sort_key ) {
        case 'cuisine':
            $imc_search_string = ' cuisine = ' . $imc_sort_value;
            break;
        case 'neighborhood':
            $imc_search_string = ' neighborhood = ' . $imc_sort_value;
            break;
        case 'price':
            $imc_search_string = ' price_range = ' . $imc_sort_value;
            break;
        case 'flags':
            $imc_search_string = imc_get_multiple_and_search_string ( $imc_sort_value, 'venue_flags', 'AND' );
            break;
        case 'outdoor':
            $imc_search_string = ' outdoor = ' . $imc_sort_value;
            break;
        case 'waterfront':
            $imc_search_string = ' waterfront = ' . $imc_sort_value;
            break;
        case 'city':
            $imc_search_string = ' city LIKE %' . $imc_sort_value . '%';
            break;
        case 'meal':
            $imc_search_string = imc_get_multiple_search_string ( $imc_sort_value, 'meal_types', 'OR' );
            break;
    }
    return $imc_search_string;
}

function imc_get_multiple_search_string ( $string, $column, $conjunction ) {
    if ( strpos ( $string, '+' ) ) {
        $imc_values = explode( '+', $string );
        $imc_search_string = ' ' . $column . ' LIKE ';
        $imc_counter = 0;
        foreach ( $imc_values as $imc_value ) {
            if ( $imc_counter != 0 ) {
                $imc_search_string .= ' ' . $conjunction . ' %' . $imc_value . '%';
            } else {
                $imc_search_string .= '%' . $imc_value . '%';
                $imc_counter++;
            }
        }
    } else {
        $imc_search_string = ' ' . $column . ' LIKE %' . $string . '%';
    }
    return $imc_search_string;
}

function str_lreplace($search, $replace, $subject)
{
    $pos = strrpos($subject, $search);

    if($pos !== false)
    {
        $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
}

function imc_get_venues() {
    $limit_sql = '';
    $record = 1;
    $start = 0;
    $limit = 15;

    global $wpdb; 
    $venue_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue where is_active = "1" AND ';
    $filter_sql = '';
    $filter_count = 0;
    //var_dump($_POST);
    
    
    if( isset( $_POST[ 'searchname' ] ) && strlen( $_POST[ 'searchname' ] ) > 0 ) {
        $filter_sql .= ' name LIKE "%' . $_POST[ 'searchname' ] . '%" AND ';
        $type = 'numerical';
        
    } elseif( isset( $_POST[ 'letter' ] ) && strlen( $_POST[ 'letter' ] ) > 0 ) {
        $letter = $_POST[ 'letter' ];
        $filter_sql .= ' name LIKE "' . $letter . '%" AND ';
        $type = 'all';
    } else {
        $type = 'alpha';
    }
    
        
    if( isset( $_POST[ 'neighborhood' ] ) ) {
        $filter_sql .= ' ( ';
        $filter_sql .= ' neighborhood = ';
        $filter_sql .= implode( ' OR neighborhood = ', $_POST[ 'neighborhood' ] );
        $filter_sql .= ' ) ';
        $filter_count++;
        $type = 'numerical';
    }
    if( isset( $_POST[ 'meal' ] ) ) {
        $filter_sql .= ' ( ';
        $filter_sql .= ' meal_types LIKE \'%';
        $filter_sql .= implode( '%\' AND meal_types LIKE \'%', $_POST[ 'meal' ] );
        $filter_sql .= '%\'';
        $filter_sql .= ' )';
        $type = 'numerical';
        $filter_count++;
    }
    
    if( isset( $_POST[ 'cuisine' ] ) ) {
        if ( $filter_count > 0 ) {
            $filter_sql .= ' AND ( ';
        } else {
            $filter_sql .= ' ( ';
        }
        $filter_sql .= ' cuisine1 = ';
        $filter_sql .= implode( ' OR cuisine1 = ', $_POST[ 'cuisine' ] );
        $filter_sql .= ' OR cuisine2 = ';
        $filter_sql .= implode( ' OR cuisine2 = ', $_POST[ 'cuisine' ] );
        $filter_sql .= ' OR cuisine3 = ';
        $filter_sql .= implode( ' OR cuisine3 = ', $_POST[ 'cuisine' ] );
        $filter_sql .= ' ) ';
        $filter_count++;
        $type = 'numerical';
    }
    if( isset( $_POST[ 'filters' ] ) ) {
        if ( $filter_count > 0 ) {
            $filter_sql .= ' AND (';
        } else {
            $filter_sql .= ' ( ';
        }
        
        
        $filter_sql .= ' venue_flags LIKE \'%';
        
        $filter_sql .= implode( '%\' AND venue_flags LIKE \'%', $_POST[ 'filters' ] );
        $filter_sql .= '%\'';
        $filter_sql .= ' ) AND ( ';
        $filterCount = 0;
        foreach($_POST[ 'filters' ] as $key => $value) {
            $filterCount++;
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'0%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'1%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'2%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'3%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'4%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'5%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'6%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'7%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'8%\' AND';
            $filter_sql .= ' venue_flags NOT LIKE \'%'.$value.'9%\' )';
            if($filterCount != count($_POST['filters'])) $filter_sql .= ' AND ( ';
        }
        
        $filter_count++;
        $type = 'numerical';
    }
    if ( strlen($filter_sql) > 1 ) {
        $venue_sql .= $filter_sql;
    } 
    if( isset( $_POST[ 'record' ] ) ) {
        if( $_POST[ 'record' ] > 0 ) {
            $record = $_POST[ 'record' ];
            $start = ( $record - 1 ) * $limit;
            $type = 'numerical';
        } else {
            $record = 1;
        }
    }

    if($type != 'all') {
        $limit_sql = ' LIMIT ' . $start . ', ' . $limit;
    }
    /* remove the last and if there is no filter */
    if($filter_count == 0) {
        $venue_sql = str_lreplace('AND', '', $venue_sql);
    }
    $venue_sql .= ' ORDER BY name ASC ';
    $venue_sql .= $limit_sql;
    
    $venues = $wpdb->get_results ( $venue_sql );
    imc_get_venues_pagination( $limit, $record, $venue_sql, $type );
    return $venues;
}

function imc_return_price_range( $number ) {
    switch ( $number ) {
        case 1:
            $range = '$';
            break;
        case 2:
            $range = "$$";
            break;
        case 3:
            $range = '$$$';
            break;
        case 4:
            $range = '$$$$';
            break;
        default:
            $range = '';
            break;
    }
    return $range;
}

function imc_return_records ( $record_type, $value ) {
    global $wpdb;
    $record_value = '';

    switch ( $record_type ) {
        case 'cuisine':
            $record_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_cuisine_list WHERE id = ' . $value;
            break;
        case 'venue_flags':
            if( strlen( $value ) == 0 ) {
                $value = 11;
            }
            $record_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_flag_list WHERE ';
            if ( strpos ( $value, ',' ) ) {
                $venue_flag_array = explode( ',', $value );
                $venue_flag_counter= 0;
                foreach ( $venue_flag_array as $venue_flag ) {
                    if ( $venue_flag_counter != 0 ) {
                        $record_sql .= ' OR id = ' . trim ( $venue_flag );
                    } else {
                        $record_sql .= ' id = ' . trim ( $venue_flag );
                        $venue_flag_counter++;
                    }
                }
            } else {
                $record_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_flag_list WHERE id = ' . $value;
            }
            break;
        case 'neighborhood':
            $record_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_neighborhoods WHERE id = ' . $value;
            break;
        default:
            $record_value = 'Unknown';
    }
    $record_results = $wpdb->get_results ( $record_sql );
    $record_counter = 0;
    if ( strlen ( $value ) > 2 ) {
        foreach ( $record_results as $record_result ) {
            if ( $record_counter > 0 ) {
                $record_value .= ', ' . $record_result->name; 
            } else {
                $record_value .= $record_result->name;
                $record_counter++;
            }
        }
    } else {
        foreach ( $record_results as $record_result ) {
            if ( $record_result->name != 'Unsure' ) {
                $record_value = $record_result->name;
            }
        }
    }
    return $record_value;
}

function imc_get_random_image() {
   $random = rand( 1, 3 );
   
   switch ( $random ) {
       case 1:
           $image = 'http://dev.chooseimpulse.com/test/wp-content/uploads/2014/05/default1.jpg';
           break;
       case 2:
           $image = 'http://dev.chooseimpulse.com/test/wp-content/uploads/2014/05/default_2.jpg';
           break;
       case 3:
           $image = 'http://dev.chooseimpulse.com/test/wp-content/uploads/2014/05/default_3.jpg';
           break;
       default:
           $image = 'http://dev.chooseimpulse.com/test/wp-content/uploads/2014/05/default_3.jpg';
   }
   return $image;
}

function explode_list ( $list ) {
    if ( strpos ( $list, ',' ) ) {
        $list_array = array_map ( 'trim', explode( ',', $list ) ) ;
    } else {
        $list_array = $list;
    }
    return $list_array;
}
 
function imc_get_venue_image ( $url ) {
    echo $url;
    if ( $url === 'undefined' || is_null($url) || $url === '' ) {
        $image_url = imc_get_random_image();
    } else {
        $image_url = $url;
    }
    return $image_url;
}

function imc_get_hours( $options ) { 
    $open_times = '';
    if ( strpos ( $options, ',' ) ) {
        $hours_array = explode_list( $options );
        $meal_type_counter = 0;
        foreach ( $hours_array as $hour ) {
            if ( $meal_type_counter > 0 ) {
                $open_times .= ' ' . imc_get_meal_type ( $hour );
            } elseif( $meal_type_counter == 0) {
                $open_types = '';
            } else {
                $open_times .= imc_get_meal_type ( $hour );
                $meal_type_counter++;
            }
            
        }
    } else {
        $open_times = imc_get_meal_type( $options );
        if ( $open_times == 'Unsure' ) {
            $open_times = ' ';
        }
    }
    return $open_times;
}

function imc_get_meal_type ( $time ) {
    switch ( $time ) {
        case 1:
            $meal_type = 'Breakfast';
            break;
        case 2:
            $meal_type = 'Brunch';
            break;
        case 3:
            $meal_type = 'Lunch';
            break;
        case 4:
            $meal_type = 'Dinner';
            break;
        default:
            $meal_type = '';
    }
    if($meal_type != '') {
        return '<span class="mealType meal-type">' . $meal_type . '</span>';
    }
    return '';
    
}

function imc_get_paid_table( $id ) {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_paid WHERE venue_id = ' . $id;
    return $wpdb->get_row( $sql );
}

function display_cusines( $cuisine_array ) {
    $cuisines = '';
    foreach( $cuisine_array as $cuisine ) {
        if(imc_return_records( 'cuisine', $cuisine ) != 0 || imc_return_records( 'cuisine', $cuisine ) != '' ) {
            if( strlen( $cuisines ) > 1 ) {
                $cuisines .= ', ' . imc_return_records( 'cuisine', $cuisine );
            } else {
                $cuisines .= '<p><span class="title">Cuisine:</span> ' . imc_return_records( 'cuisine', $cuisine );
            }
        }
    }
    return $cuisines;
}

function imc_display_venues () {
    global $wpdb;
    $venues = imc_get_venues();
    foreach ( $venues as $venue ) { 
        $paid_level = $venue->paid_level;
        if( $venue->paid_level > 0 ) {
            $paid = imc_get_paid_table( $venue->id );
        }
        
        $venue_id = $venue->id;
        $pageid = imc_get_venue_page_id( $venue_id );
        $url = imc_get_venue_guid( $pageid );
        $full_address = $venue->address . ' ' . $venue->address2 . ', ' . $venue->city . ', ' . $venue->state;
        ?>
        <div class="venue">
            <?php if( $paid_level > 2 ) {
                if( $venue->image != '' ) {
                ?>
                <div class="venueImage">
                    <a href="<?php echo $url ?>">
                        <img src="<?php echo imc_get_venue_image( $venue->image ); ?>" alt="<?php echo $venue->name . ' ' . $venue->city . ', ' .$venue->state; ?>" />
                    </a>
                </div>
                <?php } ?>
            
            
            <?php
             } ?>
            <div class="logo">
                <?php
                if($venue->logo != null  && $venue->logo != ' ' && trim($venue->logo) != 'http://' && strlen($venue->logo) > 10) {
                    ?>
                <img src="<?php echo $venue->logo; ?>" alt="<?php echo $venue->name . $venue->city . ', ' .$venue->state; ?>" />
            <?php } ?>
                <div class="hours">
                    <?php echo imc_get_hours( $venue->meal_types ); ?>
                </div>
            </div>
            
            <div class="venueDetails">
                <h2><a target="_blank" href="<?php echo $venue->wp_guid; ?>"><?php echo $venue->name; ?></a></h2>
                <h3>Address: <a href="http://maps.google.com/?q=<?php echo $venue->name . ' ' . $full_address; ?>" target="_blank"><?php echo $full_address; ?></a></h3>
                <?php if( strlen( $venue->phone ) > 9 ) { ?> 
                <h4>Phone: <a href="tel:<?php echo $venue->phone; ?>"><?php echo preg_replace("/^(\d{3})(\d{3})(\d{4})$/", "$1-$2-$3", $venue->phone ); ?></a></h4>
                <?php } ?>
                 <?php
                    echo display_cusines( array( $venue->cuisine1, $venue->cuisine2, $venue->cuisine3 ) );?> <span class="priceRange">
                        <?php echo imc_return_price_range ( $venue->price_range ); ?></span></p>
                <?php 
                if( $paid_level > 0 ) {
                    if($venue->website != '') {
                        if( strpos( $venue->website, 'http' ) === false) {
                            $venue->website = 'http://' . $venue->website;
                        } 
                        
                            
                    ?>
                    <h5 class="website"><a href="<?php echo $venue->website; ?>" target="_blank">Visit <?php echo $venue->name . '\'s website'; ?></a></h5>
                <?php }
                    }
                if( $paid_level > 1 ) {
                    ?>
                    <div class="options">
                        <p><span class="title">Neighborhood:</span> <?php echo imc_return_records ( 'neighborhood', $venue->neighborhood ); ?></p>
                        
                    </div>
                <?php  } 
                if( $paid_level > 0 ) {
                    ?> 
                    <div class="flags"><?php echo imc_return_records ( 'venue_flags', $venue->venue_flags ); ?></div>
                <?php }
?>
                
                
                
                
                
            </div>    
        </div>    
    <?php     
    }
    ?><span class="die"><?php
} 
