<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include( 'imc-get-venues.php' );

function imc_display_venue( $venue_id ) {
    wp_enqueue_script( 'imc-venue-display', plugin_dir_url(__FILE__) . 'js/imc-venue-display.js', array( 'jquery' ) );
    $social = array( 'twitter' => imc_get_plugin_image( 'twitter' ), 'yelp' => imc_get_plugin_image( 'yelp' ), 'googleplus' => imc_get_plugin_image( 'googleplus' ), 'foursquare' => imc_get_plugin_image( 'foursquare' ), 
        'facebook' => imc_get_plugin_image( 'facebook' ), 'urbanspoon' => imc_get_plugin_image( 'urbanspoon' ), 'instagram' => imc_get_plugin_image( 'instagram' ) );
    $venue = imc_get_venue( $venue_id );
    $paid = imc_get_paid_values( $venue_id );
    if( $paid == null ) {
        $display_paid = false;
    } else {
        $display_paid = true;
    }
    
    if( $display_paid == true ) {
        $image_array = explode( ',', $paid->photos );
        $tempImage = imc_get_random_venue_image( $image_array );
        if($tempImage != null  && $tempImage != ' ' && trim($tempImage) != 'http://' && strlen($tempImage) > 10) {
            $random_image = '<img src="' . $tempImage . '" />';
        } else {
            ?> <style>#venue-main-bottom { border-radius: 15px!important; } </style> <?php
            $random_image = '';
        }
    } else {
        $image_array = array( ' ', ' ' );
        $random_image = '';
        ?> <style>#venue-main-bottom { border-radius: 15px!important; } </style> <?php
    }
    $price_range = imc_return_price_range( $venue->price_range );
    $neighborhood = imc_return_records( 'neighborhood', $venue->neighborhood );
    $cuisine = imc_return_records( 'cuisine', $venue->cuisine1 );
    $flags = imc_return_records( 'venue_flags', $venue->venue_flags );
    $address1 = $venue->address;
    $address2 = imc_check_address( $venue->address2 );
    $address3 = $venue->city . ', ' . $venue->state . '  ' . $venue->zip;
    $address_google = str_replace( ' ', '+', $address1 ) . $address2 . '+' . str_replace( ' ', '+', $address3 );
    $name = $venue->name;
    $meal_types = explode( ',', $venue->meal_types );
    $meals = '';
    
    foreach($meal_types as $key => $value) {
        switch( $value ) {
            case 1:
                $meals .= '<span class="meal-type">Breakfast</span>';
                break;
            case 2:
                $meals .= '<span class="meal-type">Brunch</span>';
                break;
            case 3:
                $meals .= '<span class="meal-type">Lunch</span>';
                break;
            case 4:
                $meals .= '<span class="meal-type">Dinner</span>';
                break;
        }
    }
    $hh_array = imc_get_venue_hh( $venue_id, date( 'N' ) );
    $target_array = array();
    ?>

<div id="venue-main">
    
    <?php if($random_image != '') { ?>
        <div id="venue-image">
        <?php echo $random_image; ?>
    </div>
    <?php } ?>
    
    <div id="venue-main-bottom">
        <?php if($venue->logo != '') { ?>
            <div class="logo" style="float: right; max-width: 75px; overflow: hidden;">
                <img src="<?php echo $venue->logo; ?>" />
            </div>
        <?php } ?>
        
        <div class="venue-main-details">
            <h1><?php echo $name; ?></h1>
            <span class="venue-cuisine"><?php echo $cuisine . '  ' . $price_range; ?></span>
            <p><?php echo $neighborhood; ?>
                <span> <?php echo $flags; ?></span></p>
            
            <p><?php if( $display_paid ) {
                echo $paid->details;
            }?></p>
        </div>
        <div class="venue-image-details">
            <div class="additional-images">
                <?php echo imc_get_venue_images( $image_array ); ?>
            </div>
            <div class="social">
                <?php echo imc_get_plugin_images( $social, $display_paid, $paid ); ?>
            </div>
            <div class="extras">
                <?php if( $display_paid ) {
                    echo imc_get_extras( array( $paid->menu, $paid->beer_on_taps, $paid->craft_beers, $display_paid ) ); 
                }?>
                <?php echo $phone = $venue->phone != '' ? '<p style="padding: 15px 0 10px 0;"><a style="color:#000;" href="tel:'.$venue->phone.'">'.$venue->phone.'</a></p>' : ''; ?>
                
            </div>
        </div>
        <div><h2 style="margin: 10px 0 0 0;">Open For: </h2>
            <p><?php echo $meals; ?></p>
        </div>
    </div>
    
    
</div>
<?php
    if ( is_user_logged_in() ) {
        $link_url = add_query_arg( 'claim', str_replace( ' ', '_', $name ),  get_site_url() . '/contact/' );
        $link_title = 'Claim this listing';
    } else {
        $link_url = wp_login_url() . '?e=claim&id=' . $venue_id;
        $link_title = 'Log in / Register to Claim your listing';
    }
    echo '<a class="claim-link" href="' . $link_url . '" target="_blank">' . $link_title . '</a>';
?>
<div id="venue-second">
    <div class="map-details">
        <a href="http://maps.google.com/?q=<?php echo $name . ' ' . $address_google; ?>" target="_blank">
            <img class="map-image" src="http://maps.googleapis.com/maps/api/staticmap?center=<?php echo $address_google; ?>&zoom=15&size=450x250&maptype=roadmap&markers=color:red%7Clabel<?php echo $venue->name; ?>%7C<?php echo $address_google; ?>" />
        </a>
    </div>
    <div class="venue-excitement">
        <h3>Today's Excitement</h3>
        <p class="title">Happy Hour Specials<br>
            <div class="hh-insert">
                <div id="hh-live">No specials today</div>
                <?php echo imc_get_all_happy_hours_venue( $hh_array, 'hh-live' ); array_push( $target_array, 'hh-live' ); ?>
            </div>
            
        </p>
        <p class="title">Live Entertainment</p>
        <div class="entertainment-insert">
            <?php echo imc_get_todays_entertainment( $venue->wp_post_id ); ?>
        </div>
        
    </div>
    
    
    
    <div id="venue-map-details">
        <img src="<?php echo imc_get_plugin_image( 'marker_small' ); ?>" />
        <h2><?php echo $name; ?></h2>
        <p>
            <span id="address1"><?php echo $address1; ?></span><br>
            <?php if( strlen( $address2 ) > 0 ) {
                ?><span id="address2"><?php echo $address2; ?></span><br>
            <?php } ?>
            <span id="address3"><?php echo $address3; ?></span>
        </p>
    </div>   
    
   
</div>
<div id="venue-this-week">
    <div id="venue-events" class="half-venue">
        <img class="icon" src="<?php echo imc_get_plugin_image( 'event-icon' ) ?>" />
        <img class="title" src="<?php echo imc_get_plugin_image( 'events-title' ) ?>" alt="Events" /> 
        <h4 class="orange-text">Entertainers This Week</h4>
        <?php echo imc_get_entertainers_venue(); ?>
        
    </div>
    <div id="venue-hh" class="half-venue">
        <img class="icon" src="<?php echo imc_get_plugin_image( 'happy-hour-icon' ) ?>" />
        <img class="title" src="<?php echo imc_get_plugin_image( 'happy-hour-title' ) ?>" alt="Happy Hours" /> 
        <h4 class="blue-text">Drink Specials This Week</h4>
        <?php if( imc_check_for_happyhours( $venue_id ) != NULL ) { ?>
            <div id="hh-specials">
                <?php $days = array( 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' );
                foreach( $days as $day ) {
                    echo '<div class="left">' . ucfirst( $day ) . '</div>';
                    echo '<div id="' . $day . '-hh" class="right">';
                    echo imc_get_hh_array( $day . '-hh', $venue_id ); array_push( $target_array, $day . '-hh' );
                    echo '</div>';
                } ?>
            </div> <?php
            
            
        } else { echo 'There are no current specials registered'; } ?>
            
    </div>
</div>
<div>
    
</div>
<div id="hhs"><?php echo implode( ',', $target_array ); ?></div>


<script>
jQuery(document).ready(function($) {
    $('ul.uk-navbar-nav.uk-hidden-small>li:eq(3)').addClass('uk-active');
    
}); </script>

 <?php
}


function imc_check_for_happyhours( $venue_id ) {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_happy_hour WHERE venue_id = ' . $venue_id;
    $has_hh = $wpdb->get_results( $sql );
    return $has_hh;
}

function imc_get_extras( $extras = array() ) {
    $paid = $extras[3];
    if( $paid == 1 ) {
        array_pop( $extras );
        $ret_val = '';
        foreach( $extras as $index=>$key ) {
            $ret_val .= imc_get_paid_extras( $index, $key );
        }
    } 
    return $ret_val;
}

function imc_get_paid_extras( $index, $key ) {
    switch( $index ) {
        case 0:
            $ret_val = imc_check_extras( $key, '<p class="menu"><a href="' . $key . '" target="_blank">View Menu</a></p>');
            break;
        case 1:
            $ret_val = imc_check_extras( $key, '<p class="beers">' . $key . ' beers on tap</p>');
            break;
        case 2:
            $ret_val = imc_check_extras( $key, '<p class="craft">Additional Info From Venue:<br>' . $key . '</p>');
            break;
    }
    return $ret_val;
}

function imc_check_extras( $value, $return ) {
    if( isset( $value ) && trim( $value ) != '' ) {
        $ret_val = $return;
    } else {
        $ret_val = '';
    }
    return $ret_val;    
}


function imc_get_hh_array( $target, $id ) {
    $day = explode( '-', $target );
    $new_day = imc_get_day_reverse( ucfirst( $day[0] ) );
    $hh_array = imc_get_venue_hh( $id, $new_day );
    $ret_val = imc_get_all_happy_hours_venue( $hh_array, $target );
    return $ret_val;
    
        
}

function imc_get_todays_entertainment( $venue_id ) {
    $dt = new DateTime();
    $todays_date = $dt->format( 'Y-m-d' );
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_events WHERE DATE( start_date ) = "' . $todays_date . '" AND venue_id = ' . $venue_id. ' and approved = 1 ORDER BY start_date ASC';
    $results = $wpdb->get_results( $sql );
    if( $results ) { 
        return imc_get_todays_entertainers( $results, $venue_id );
    } else {
        return '<span class="entertainment">No Live Entertainment today</span>';
    }
}

function imc_get_todays_entertainers( $events, $venue_id ) {
    $event_ids = array();
    foreach( $events as $event ) {
        $event_ids[] = $event->event_id;
    }
    return imc_get_event_details( $event_ids, $venue_id );
}

function imc_get_event_details( $event_ids, $venue_id ) {
    global $wpdb;
    $sql = 'SELECT * FROM ' . $wpdb->prefix . 'posts WHERE ID = ' . implode( ' OR ID = ', $event_ids ); 
    $results = $wpdb->get_results( $sql );
    return imc_get_today_event_venue( $results, $venue_id );
}

function imc_get_today_event_venue( $posts, $venue_id) {
    global $wpdb;
    $venueSql = 'SELECT name from ' . $wpdb->prefix . 'imc_venue where wp_post_id = ' . $venue_id;
    $venueName = $wpdb->get_var($venueSql);
    foreach( $posts as $post ) {
        global $wpdb;
        $sql = 'SELECT * from '.$wpdb->prefix.'imc_events where event_id = '.$post->ID;
        $event = $wpdb->get_row($sql);
        $allEvents[date('H', strtotime($event->start_date))] = '<a class="entertainment" href="' . $post->guid . '" target="_blank">' . date('h:ia', strtotime($event->start_date)) . ' - ' . explode(' at ', $post->post_title )[0] . '</a><br>';
    }
    $newHtml = '';
    for($i = 0; $i < 23; $i++) {
        if(array_key_exists($i, $allEvents)) {
            $newHtml .= $allEvents[$i];
        }
    }
    return $newHtml;
}

function imc_get_all_happy_hours_venue( $hh_array, $target ) {
    $ret_val ='<p class="hh-container">';
    foreach( $hh_array as $hh ) {
        $ret_val .= imc_get_excitement( $hh, $target );
    }
    return $ret_val;
}

function imc_get_excitement( $hh, $target ) {
    $ret_val = '<span class="' . $target . '">';
    $ret_val .= imc_get_price_excitement( $hh->price, $hh->reg_price, imc_happy_hour_types( $hh->happy_hour_type ) );
    $ret_val .= imc_get_time_excitement( $hh->time_start, $hh->time_end );
    $ret_val .= $hh->summary . '</span>';
    
    return $ret_val;
}

function imc_get_price_excitement( $price, $reg_price, $hh_type ) {
    $ret_val = '<span class="old-price">';
    $ret_val .= '$' . number_format( $reg_price, 2, '.', '' ) . '</span> <span class="new-price">$' . number_format( $price, 2, '.', '' ) . ' ' . $hh_type . '</span>';
    return $ret_val;
}

function imc_get_time_excitement( $start, $end ) {
    $ret_val = '<span class="time-span">';
    if( $start != $end ) {
        $ret_val .= ' ' . ltrim( date( 'h:ia', strtotime( $start ) ), '0');
        $ret_val .= ' - ' . ltrim( date( 'h:ia', strtotime( $end ) ), '0');
    } else {
        $ret_val .= ' All Day';
    }
    $ret_val .= '</span>';
    return $ret_val;
}

function imc_check_address( $address2 ) {
    $address2 = trim($address2, ' ');
    if( $address2 != '' ) {
        $ret_val = '+' . str_replace( ' ', '+', $address2 );
    } else {
        $ret_val = '';
    }
    return $ret_val;
}


function imc_get_paid_values( $id ) {
    global $wpdb;
    $paid_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue_paid WHERE venue_id = ' . $id;
    $paid = $wpdb->get_row( $paid_sql );
    return $paid;
}

function imc_get_venue( $id ) {
    global $wpdb;
    $venue_sql = 'SELECT * FROM ' . $wpdb->prefix . 'imc_venue WHERE id=' . $id;
    $venue = $wpdb->get_row( $venue_sql );
    return $venue;
}

function imc_get_venue_images( $image_array ) {
    $image_counter = 0;
    foreach( $image_array as $image ) {
        if( $image_counter < 7 ) {
            $image_counter++;
            if(trim($image) != '' && $image != ' ' && trim($image) != 'http://' && strlen($image) > 10) {
                echo '<div class="venue-thumbnail-container">';
                echo '<img class="venue-thumbnail" src="' . $image . '" data-large-image="' . $image . '" />';
                echo '</div>';
            }
        }   
    }    
}

function imc_get_plugin_images( $social, $display_paid, $paid ) {
    if( $display_paid != false ) {
        foreach( $social as $key => $val ) {
            if( $paid->$key != '' ) {
                echo '<a href="' . $paid->$key . '" target="_blank"><img src="' . $val . '" /></a> ';
            }
        }
    }
}

function imc_get_venue_hh( $id, $day ) {
    global $wpdb;
    $sql = 'SELECT day, time_start, time_end, happy_hour_type, price, reg_price, ounces, summary ';
    $sql .= 'FROM ' . $wpdb->prefix . 'imc_happy_hour WHERE venue_id = ' . $id . ' AND day = "' . $day . '"';
    $hh_array = $wpdb->get_results( $sql );
    return $hh_array;
}
function imc_get_todays_hh( $id ) {
    
    
    
    return $hh_detail_array;
}

