<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Venue {
    private $id; 
    private $name;
    private $address;
    private $address2;
    private $city;
    private $state;
    private $zip;
    private $phone;
    private $contact_id;
    private $cuisine;
    private $neighborhood;
    private $price_range;
    private $logo;
    private $waterfront;
    private $outdoor;
    private $is_claimed;
    private $meal_types;
    private $venue_flags;
    private $last_updated;
    private $is_active;
    
    private $created_by;
    private $created_date;
    private $updated_by;
    private $updated_date;
         
    
    public function __construct(  ) {
        $now = new DateTime();
        $timestamp_type = 'new';
        $this->set_timestamp( $now, $timestamp_type );
        $this->set_user( get_current_user_id(), 'new' );
    }
    
    public function set_timestamp( DateTime $stamp, $timestamp_type ) {
        if ( $timestamp_type === 'new' ) {
            $this->created_date = $stamp;
        } else {
            $this->updated_date = $stamp;
        }
        
    }
    
    public function set_user( $user_id, $user_type ) {
        if ( $user_type === 'new' ) {
            $this->created_by = $user_id;
        } else {
            $this->updated_by = $user_id;
        }
        
    }
    
    public function get_venue($id) {
        
    }
    
    public function set_venue() {
        
    }
    
    public function remove_venue() {
        
    }
    
    
}