<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
imc_load_styles();

imc_load_scripts();

?>
<h1>Add an Event</h1>
<div id="tribe-events-content" class="tribe-events-single featured tribe-events-page-template">
    <div id="imc-event-form">
        
        <form id="imc-create-event" action="" method="POST">
            <?php wp_nonce_field( 'create_none_for_wti','wti_like_post_meta_box_nonce' ); ?>
            <div class="event-category">
                <span  class="input-spacer">Event Category: </span>
                <label>
                    <input type="radio" name="event-category" value="3" />
                     Farmers Markets
                </label>

                     <br>
                    <span  class="input-spacer"></span>
                <label>
                    <input type="radio" name="event-category" value="4" />
                     Festivals & Fairs
                </label>
                     <br>
                    <span  class="input-spacer"></span>
                <label>
                    <input type="radio" name="event-category" value="5" />
                     Fine Arts
                </label>
                     <br>
                    <span  class="input-spacer"></span>
                <label>
                    <input type="radio" name="event-category" value="6" />
                     Fund Raisers
                </label>
                     <br>
                     <span  class="input-spacer"></span>
                <label>
                     <input type="radio" name="event-category" value="1" />
                     Live Music
                </label>
                     <br>
                    <span  class="input-spacer"></span>
                <label>
                    <input type="radio" name="event-category" value="0" />
                     Theatre
                </label>
                    <br>
                    <span  class="input-spacer"></span>
                <label>
                    <input type="radio" name="event-category" value="7" checked="checked" />
                     Enter My Own Search Tags
                </label>
                     <br><br>

            </div>
            <label>
                <span class="input-spacer">Event Title: </span>
                <input id="title" type="text" name="event-title" class="wider e-required" />
            </label>
            <label>
                <span class="input-spacer">Event Description: </span>
                <textarea id="description" name="event-description" class="wider e-required"></textarea>
            </label>
            <label id="band-input" style="display: none;">
                <span class="input-spacer">Band or Musician: </span>
                <select name="event_band" id="event_band">
                    <?php echo imc_get_bands(); ?>
                </select>
                <span class="extra-text">Click in the select box and type the first letter of the band or musician for quicker results. If you don't find the band,
                    <a href="<?php echo get_site_url(); ?>/local-musicians/#add-musician">create one first</a>!</span>
            </label>
            <label>
                <span class="input-spacer">Event Website: </span>
                <textarea id="website" name="event-website" class="wider"></textarea>
            </label>
            <div class="logo">
                <span  class="input-spacer">Event Image: </span>
                <input id="imc_image_url" type="text" size="36" name="imc_image_url" value="" class="wider"/> 
                <input id="imc_image_url_button" class="button create-event-button" type="button" value="Select Image"  />
                <span class="extra-text">Enter a URL for an image or click upload to upload an image from your computer</span>
                <div class="labelOptions">
                    <img id="imc_image_preview" />
                </div>
            </div>
            <label></label>
            <label class="timepicker">
                <span  class="input-spacer">Start Date & Time:</span>
                <input id="start-date" type="text" name="start-date" class="date-picker e-required" placeholder="click here for date" /> 
            </label>
            <label class="timepicker">
                @ 
                <select id="start-hour" name="start-hour">
                    <?php echo imc_get_option_value_integers( 1, 12 ); ?>
                </select>
            </label>
            <label class="timepicker">
                <select id="start-minute" name="start-minute">
                    <?php echo imc_get_option_value_integers( 00, 55, 5 ); ?>
                </select>
            </label>
            <label class="timepicker">
                <select id="start-meridian" name="start-meridian">
                    <option value="am">am</option>
                    <option value="pm">pm</option>
                </select>
            </label>
            <label></label>
            <label class="timepicker">
                <span  class="input-spacer">End Date & Time</span>
                    
                <input id="end-date" type="text" name="end-date" class="date-picker e-required" placeholder="click here for date" />
            </label>
            <label class="timepicker">
                @ 
                <select id="end-hour" name="end-hour">
                    <?php echo imc_get_option_value_integers( 1, 12 ); ?>
                </select>
            </label>
            <label class="timepicker">
                <select id="end-minute" name="end-minute">
                    <?php echo imc_get_option_value_integers( 00, 55, 5 ); ?>
                </select>
            </label>
            <label class="timepicker">
                <select id="end-meridian" name="end-meridian">
                    <option value="am">am</option>
                    <option value="pm">pm</option>
                </select>
            </label>
            <div class="event-cost">
                <span  class="input-spacer">Event Cost: </span>
                <label>
                    <span  class="input-spacer"></span>
                    <input type="radio" id="event-cost" name="event-cost" value="Paid"  />
                         Paid
                </label>
                <label>
                    <span  class="input-spacer"></span>
                    <input type="radio" id="event-cost" name="event-cost" value="Free" checked="checked" />
                         Free
                </label>
                <span class="extra-text">Type in all the pricing details inside the event description above (Under Event Title).</span>
            </div>
            <div class="location-autocomplete">
                <span  class="input-spacer">Location Type: </span>
                <label>
                    <span  class="input-spacer"></span>
                    <input type="radio" id="location-autocomplete" name="location-autocomplete" value="0" checked="checked" />
                         Manually Type in Address
                </label>
                <label>
                    <span  class="input-spacer"></span>
                    <input type="radio" id="location-autocomplete" name="location-autocomplete" value="1" />
                    Select a venue/location from the database (<span style="font-style:italic;">Use this option if the venue is a restaurant or bar in Naples</a>)
                </label>
            </div>
            <label id="event-location">
                <span id="hh-autocomplete-spacer" class="input-spacer">Location Name</span>
                <input id="venue-name" type="text" name="term" autocomplete="on" class="wider e-required"/>
                <div id="imc-autocomplete-container">
                    <ul class="imc-autocomplete">

                    </ul>
                </div>
                <span class="extra-text">Start typing in the name of the venue and we will try to load up the venue address for you!</span>
            </label>
            <div id="venue-address">
                <label>
                    <span  class="input-spacer">Address:</span>
                    <input id="address" name="venue-address" type="text" class="wider e-required" />
                </label>
                <label>
                    <span  class="input-spacer">City: </span>
                    <input id="city" name="venue-city" type="text" class="wider e-required" value="Naples" />
                </label>
                <label>
                    <span  class="input-spacer">State:</span>
                    <input id="state" name="venue-state" type="text" class="wider e-required" value="FL"  />
                </label>
                
                <label>
                    <span  class="input-spacer">Website:</span>
                    <input id="website" name="venue-website" type="text" class="wider"  />
                </label>
                
                <label>
                    <span  class="input-spacer">Your Contact Information:</span>
                    <input id="phone" name="venue-phone" type="text" class="wider e-required"  />
                </label>
                <span class="extra-text">Please enter your contact information (name & phone number) in case we have any questions</span>
            </div>
            <div class="duplicate-event">
                <span  class="input-spacer">Duplicate Event: </span>
                <label>
                    <span  class="input-spacer"></span>
                    <input type="radio" id="duplicate-event" name="duplicate-event" value="1" />
                         Duplicate this event after I create it.
                </label>
                <label>
                    <span  class="input-spacer"></span>
                    <input type="radio" id="duplicate-event" name="duplicate-event" value="0" checked="checked" />
                         This is a single event, start me with a blank event.
                </label>
            </div>
            </div>
            <input type="hidden" name="action" value="imc_create_event" />
            <input id="imc-process-event" type="submit" class="create-event-button" value="Create Event" />
            <img src="<?php echo admin_url('/images/wpspin_light.gif'); ?>" class="waiting" id="imc-loading" style="display:none;" />
        </form>
        <div id="imc-event-result">
            <!--result area -->
        </div>
    </div>
</div>    