<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function imc_display_happy_hour_page() {
    wp_enqueue_script( 'imc-hh', plugin_dir_url(__FILE__) . 'js/imc-hh.js', array( 'jquery' ) );
    display_favorite_table();
    imc_display_hh_data();
}



function get_favorite_hh_type() {
    if( isset( $_POST[ 'favorite' ] ) ) {
        $favorite = $_POST[ 'favorite' ];
    } else {
        $favorite = 0;
    }
    return $favorite;
}

function display_favorite_table() {
    ?>
<div id="favorite-hh">
    <img id="hh-icon" src="<?php echo imc_get_plugin_image( 'happy-hour-icon' ); ?>" />
    <h2>What type of happy hour are you looking for?</h2>
    
    <form id="favorite-hh-form" action="" method="POST">
        <img id="select-your-favorite" src="<?php echo imc_get_plugin_image( 'right-arrow' ); ?>" />
        <label for="fav_draft"><input id="fav_draft" type="radio" name="favorite" value="1" <?php echo imc_is_selected( '1', 'favorite' ); ?>/>Draft Beer</label>
        <label for="fav_bottle"><input id="fav_bottle" type="radio" name="favorite" value="2" <?php echo imc_is_selected( '2', 'favorite' ); ?>/>Bottle Beer</label>
        <label for="fav_wine"><input id="fav_wine" type="radio" name="favorite" value="3" <?php echo imc_is_selected( '3', 'favorite' ); ?>/>Wine</label>
        <label for="fav_liquor"><input id="fav_liquor" type="radio" name="favorite" value="4" <?php echo imc_is_selected( '4', 'favorite' ); ?>/>Liquor</label>
        <label for="fav_all"><input id="fav_all" type="radio" name="favorite" value="0" <?php echo imc_is_selected( '0', 'favorite' ); ?>/>No preference</label>
        <label class="hidden"><input id="sort" type="text" name="sort" value="<?php echo isset($_POST['sort']) ? htmlspecialchars($_POST['sort']) : 'time'; ?>" /></label>
        <input type="hidden" name="action" value="imc_display_hh_data" />
    </form>
</div>
<?php
}

function imc_get_time_span() {
    $span_sql = ' ( ';
    $time_count = 0;
    if( time() < strtotime( '05:00:00' ) ) {
        if( $time_count == 0 ) {
            $span_sql = 'hhs.timespan = 3';
            $time_count++;
        } else {
            $span_sql .= ' OR hhs.time_span = 3';
        }
    }
    if( time() < strtotime( '12:00:00' ) ) {
        if( time() > strtotime( '05:00:00') ) {
            if( $time_count == 0 ) {
                $span_sql .= 'hhs.time_span = 1';
                $time_count++;
            } else {
                $span_sql .= 'OR hhs.time_span = 1';
            }
        }
    }
    if( time() < strtotime( '21:30:00' ) ) {
        if( time() > strtotime( '11:00:00') ) {
            if( $time_count == 0 ) {
                $span_sql .= 'hhs.time_span = 2';
                $time_count++;
            } else {
                $span_sql .= ' OR hhs.time_span = 2';
            }
        }
    }
    if( time() > strtotime( '18:00:00' ) ) {
        if( $time_count == 0 ) {
            $span_sql .= 'hhs.time_span = 3';
            $time_count++;
        } else {
            $span_sql .= ' OR hhs.time_span = 3';
        }
    }
    if( $time_count == 0 ) {
        $span_sql .= ' hhs.all_day = 1 ) ';
    } else {
        $span_sql .= ' OR hhs.all_day = 1 ) ';
    }
    return $span_sql;
}

function get_best_happy_hours( $type, $hh_sql, $current_day, $sort) {
    global $wpdb;
    $order_by = imc_get_happy_hour_sort($sort);
    $span_sql = imc_get_time_span();
    $additional_sql = $hh_sql . $span_sql;
    $sql = 'SELECT hhs.*, venue.name FROM ' . $wpdb->prefix . 'imc_happy_hour AS hhs';
    $sql .= ' INNER JOIN ' . $wpdb->prefix . 'imc_venue AS venue ON hhs.venue_id = venue.id';
    $sql .= $hh_sql;
    $sql .= ' hhs.day = ' . $current_day;
    $sql .= ' AND venue.is_active = 1 ';
    $sql .= ' ORDER BY '.$order_by;
    $happy_hour_results = $wpdb->get_results( $sql );
    
    //$sort = isset($_GET['sort']) ? trim(htmlspecialchars($_GET['sort'])) : 'time';
    include( 'imc-best-happy-hour-display.php');
}

function imc_get_hh_type_text( $type ) {
    switch( $type ) {
        case 1:
            $ret_val = 'Draft Beer';
            break;
        case 2:
            $ret_val = 'Bottle Beer';
            break;
        case 3:
            $ret_val = 'Wine';
            break;
        case 4:
            $ret_val = 'Liquor Drinks';
            break;
        default:
            $ret_val = 'All Specials';
    }
    return $ret_val;
}