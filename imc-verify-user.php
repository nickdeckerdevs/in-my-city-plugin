<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



   



function imc_verify_user() {
    wp_enqueue_script( 'imc-verify-user', plugin_dir_url(__FILE__) . 'js/imc-verify-user.js', array( 'jquery' ) );
    $user_query = new WP_User_Query( array( 'meta_key' => 'updated_listing', 'meta_value' => 'Awaiting Verification' ) );
    verify_user_html( 'top' );
    if( !empty( $user_query->results ) ) {
        foreach( $user_query->results as $user ) {
            $id = $user->ID;
            $venue_name = get_user_meta( $id, 'name', true );
            echo "<li>$user->display_name: $user->user_email - Approve Owner [$venue_name] <span>$id</span></li>";
        }
    }
    verify_user_html( 'bottom' );
    
    
    
    
}



function verify_user_html( $section ) {
    if( $section == 'top' ) {
        echo '<div class="wrap verify-user">';
        echo '<h2>Verify Users</h2>';
        echo '<p><i>Recent Users who have updated their listing profiles</i></p>';
        echo '<form method="post" action="" id="imc-verify-user">';
        echo '<input type="hidden" name="action" value="imc_verify_user_by_id" />';
        echo '<input type="hidden" name="user_id" id="user-id"/>';
        echo "<ul>";
    } else {
        echo "</ul>";
        echo '</form>';
        echo '<div id="feedback"></div>';
        echo '<p>Click on a name above to verify there account, and their listing will be updated</p>';
    }
}
        
        